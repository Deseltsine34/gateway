/**
 * @format
 * @flow
 */

import React from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import AnimatedThemeView from './AnimatedThemeView';
import {ActionCreators} from '../actions';

const BottomCard = ({
  children,
  style,
  deviceDimensions,
  animatedY,
  ...props
}: props) => {
  const width = deviceDimensions.get('width');

  const customStyle = style || {};
  // TODO H - Limit bottom card height and make scrollable when necessary
  return (
    <AnimatedThemeView
      style={[
        styles.card,
        customStyle,
        {
          width,
          transform: [
            {
              translateY: animatedY,
            },
          ],
        },
      ]}
      shadowColor="rgba(0,0,0,0.3)"
      shadowOffset={{width: 0, height: 0}}
      shadowOpacity={1}
      shadowRadius={10}
      {...props}>
      <SafeAreaView style={[styles.safeArea, {width}]}>{children}</SafeAreaView>
    </AnimatedThemeView>
  );
};

const styles = StyleSheet.create({
  card: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  safeArea: {
    backgroundColor: 'transparent',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BottomCard);
