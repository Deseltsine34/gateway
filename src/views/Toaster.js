/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {Platform} from 'react-native';
import Toast from 'react-native-root-toast';
import {connect} from 'react-redux';

const Toaster = ({toast, theme, navigationMeasurements}: props) => {
  const [toastMessage, setToastMessage] = useState('');
  const [lastToastRequest, setToastRequest] = useState(0);
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    if (toast.get('request') !== lastToastRequest) {
      setToastRequest(toast.get('request'));
      setToastMessage(toast.get('message'));
      setVisible(true);
      setTimeout(() => {
        setVisible(false);
      }, 5000);
    }
  }, [lastToastRequest, toast]);
  const toastPosition =
    Platform.OS === 'android'
      ? Toast.positions.BOTTOM
      : -navigationMeasurements.get('bottomBarHeight') - 20;
  const backgroundColor = theme.get('textColor');
  const textColor = theme.get('backgroundColor');
  return (
    <Toast
      visible={visible}
      shadow={false}
      animation={true}
      backgroundColor={backgroundColor}
      textColor={textColor}
      position={toastPosition}
      hideOnPress={true}
      onHidden={() => {
        if (visible) {
          setVisible(false);
        }
      }}>
      {toastMessage}
    </Toast>
  );
};

function mapStateToProps(state) {
  return {
    navigationMeasurements: state.navigationMeasurements,
    theme: state.theme,
    toast: state.toast,
  };
}

export default connect(mapStateToProps)(Toaster);
