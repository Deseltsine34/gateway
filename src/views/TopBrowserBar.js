/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Platform, StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeView from './ThemeView';
import NavigationButton from './NavigationButton';
import NavigationInput from './NavigationInput';
import OptionsButton from './OptionsButton';
import OmniButton from './OmniButton';
import TabsButton from './TabsButton';
import ReloadButton from './ReloadButton';
import CloseKeyboardButton from './CloseKeyboardButton';
import AndroidBackHandler from './AndroidBackHandler';
import {ActionCreators} from '../actions';

class TopBrowserBar extends Component {
  constructor() {
    super();
    this.state = {
      inputSelected: false,
    };
  }

  onLayout = ({
    nativeEvent: {
      layout: {height},
    },
  }) => {
    const {
      props: {setTopBarHeight},
    } = this;
    setTopBarHeight(height);
  };

  render() {
    const {
      onLayout,
      state: {inputSelected},
      props: {tabIndex, deviceDimensions},
    } = this;
    const orientation = deviceDimensions.get('orientation');
    const inputJustifyContent = inputSelected ? 'flex-start' : 'space-between';
    const containerRightMargin = inputSelected ? 0 : 5;
    const containerLeftMargin = inputSelected ? 5 : 0;
    const paddingBottom = 6;
    return (
      <ThemeView style={[styles.bar, {paddingBottom}]} onLayout={onLayout}>
        {Platform.OS === 'android' ? (
          <AndroidBackHandler tabIndex={tabIndex} />
        ) : null}
        <SafeAreaView style={styles.bar}>
          {Platform.OS === 'ios' &&
          orientation === 'landscape' &&
          !inputSelected ? (
            <View style={styles.bar}>
              {Platform.OS === 'ios' ? (
                <NavigationButton tabIndex={tabIndex} isBack />
              ) : null}
              <NavigationButton tabIndex={tabIndex} isBack={false} />
              <ReloadButton tabIndex={tabIndex} />
            </View>
          ) : null}
          {Platform.OS === 'android' && !inputSelected ? (
            <View style={styles.bar}>
              <OmniButton tabIndex={tabIndex} />
            </View>
          ) : null}
          <NavigationInput
            tabIndex={tabIndex}
            style={[
              styles.inputContainer,
              {
                justifyContent: inputJustifyContent,
                marginRight: containerRightMargin,
                marginLeft: containerLeftMargin,
              },
            ]}
            inputSelected={inputSelected}
            toggleInputSelected={bool => {
              this.setState({inputSelected: bool});
            }}
          />
          {(Platform.OS === 'android' || orientation === 'landscape') &&
          !inputSelected ? (
            <View style={styles.bar}>
              {/*<TabsButton />*/}
              {Platform.OS === 'ios' ? (
                <OmniButton tabIndex={tabIndex} />
              ) : null}
              <OptionsButton tabIndex={tabIndex} />
            </View>
          ) : null}
          {inputSelected ? <CloseKeyboardButton /> : null}
        </SafeAreaView>
      </ThemeView>
    );
  }
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  inputContainer: {
    flex: 1,
    flexBasis: 36,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    overflow: 'hidden',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
    tabHistory: state.tabHistory,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TopBrowserBar);
