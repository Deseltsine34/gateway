/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';
import ViewShot from 'react-native-view-shot';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
var RNFS = require('react-native-fs');
const {Map} = require('immutable');
const basicParseURL = require('url-parse');
import nodejs from 'nodejs-mobile-react-native';

import {ActionCreators} from '../actions';
import {findStringOverlap} from '../lib/common';
import {makeThemeInjection} from '../lib/webview-injection/theme';

// TODO L - OCCASIONAL BUG: Later page of hyper:// site replace an older page of the same site
// TODO M - Long Press Actions
//        - Images: Share..., Save Image, Copy Image, and Open Image
//        - Links: Share..., Copy Link URL, and Open Link

class CustomWebView extends Component {
  constructor() {
    super();
    this.webView = null;
    this.viewShot = null;
    this.startingUri = null;
    this.lastInterceptedUrl;
    this.listeners = [];
    this.state = {
      // HACK - Workaround for Android: https://github.com/react-native-community/react-native-webview/issues/656#issuecomment-551312436
      renderedOnce: false,
      // HACK - Hard reset to prevent customElements conflicts
      hardReset: false,
      // TEST - For checking screenshots
      screenshot: undefined,
    };
  }

  componentDidMount() {
    const {maybeStartWithNodeProtocol, props} = this;
    maybeStartWithNodeProtocol(props);
  }

  maybeStartWithNodeProtocol(props) {
    console.log('[WEBVIEW] Maybe start with node protocol...');
    const {tabIndex, tabHistory, pushHistory, setup} = props;
    const browserReady = setup.get('browser');
    const {currentIndex, history} = tabHistory.get(tabIndex);
    const currentPage = history.get(currentIndex);
    console.log('[WEBVIEW] browserReady?', browserReady);
    if (
      !browserReady &&
      (!currentPage || !currentPage.inputUrl) &&
      this &&
      this.setState
    ) {
      this.setState({renderedOnce: true});
    }
    if (browserReady && currentPage && currentPage.inputUrl) {
      console.log('[WEBVIEW] Submit Dat 1');
      pushHistory(tabIndex, currentPage, true);
    }
  }

  maybeHardReset(lastSource, nextSource) {
    if (
      lastSource.inputUrl &&
      nextSource.inputUrl &&
      lastSource.uri !== nextSource.uri &&
      lastSource.inputUrl !== nextSource.inputUrl
    ) {
      this.listeners.map(id => {
        nodejs.channel.removeListener(id);
      });
      this.listeners = [];
      // this.setState({hardReset: true});
      // console.log('[WEBVIEW] HARD RESET TRUE!');
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!this.props.setup.equals(nextProps.setup)) {
      this.maybeStartWithNodeProtocol(nextProps);
    }
    if (this.state.hardReset !== nextState.hardReset) {
      if (nextState.hardReset) {
        this.setState({hardReset: false});
        console.log('[WEBVIEW] HARD RESET FALSE!');
      }
      return true;
    }
    if (!this.props.webViewInjection.equals(nextProps.webViewInjection)) {
      console.log('[WEBVIEW] UPDATE! 0');
      return true;
    }
    if (
      !this.props.navigationMeasurements.equals(
        nextProps.navigationMeasurements,
      )
    ) {
      console.log('[WEBVIEW] UPDATE! 1');
      return true;
    }
    const tabIndex = nextProps.tabIndex;
    const lastCommand = this.props.browserCommand.get(tabIndex);
    const nextCommand = nextProps.browserCommand.get(tabIndex);
    if (!lastCommand || !nextCommand) {
      console.log('[WEBVIEW] DO NOT UPDATE! 1');
      return false;
    }
    if (lastCommand.key !== nextCommand.key) {
      if (nextCommand.action !== '') {
        // NOTE - Needs to be an interval instead of for-loop as a workaround
        let i = 0;
        setInterval(() => {
          if (i < nextCommand.iterations) {
            if (this.webView !== null) {
              this.webView[nextCommand.action]();
            }
          }
          i += 1;
        }, 1);
      }
      console.log('[WEBVIEW] DO NOT UPDATE! 2');
      return false;
    }

    const lastTabError = this.props.tabError.get(tabIndex);
    const nextTabError = nextProps.tabError.get(tabIndex);
    if (!lastTabError || !nextTabError) {
      return false;
    }
    const lastErrorMap = Map(lastTabError);
    const nextErrorMap = Map(nextTabError);
    if (!lastErrorMap.equals(nextErrorMap)) {
      return true;
    }

    const lastTabHistory = this.props.tabHistory.get(tabIndex);
    const nextTabHistory = nextProps.tabHistory.get(tabIndex);
    if (!lastTabHistory || !nextTabHistory) {
      console.log('[WEBVIEW] DO NOT UPDATE! 3');
      return false;
    }
    const {currentIndex: lastIndex, history: lastHistory} = lastTabHistory;
    const {currentIndex: nextIndex, history: nextHistory} = nextTabHistory;
    const lastSource = lastHistory.get(lastIndex);
    const nextSource = nextHistory.get(nextIndex);
    if (
      lastSource &&
      nextSource &&
      (!lastSource.context || lastSource.context === 'site') &&
      nextSource.context &&
      nextSource.context !== 'site' &&
      nextState.renderedOnce
    ) {
      console.log('[WEBVIEW] renderedOnce set false');
      this.setState({renderedOnce: false});
    }
    if (nextSource && nextSource.request) {
      return false;
    }
    // If you navBack or navForward in tabHistory
    if (lastIndex !== nextIndex) {
      console.log('[WEBVIEW] UPDATE! 2');
      this.maybeHardReset(lastSource, nextSource);
      return true;
    }
    if (this.startingUri === null) {
      const nextSourceUrl = nextSource.inputUrl || nextSource.uri;
      this.startingUri = nextSourceUrl;
      console.log('[WEBVIEW] UPDATE! 2.5');
      return true;
    }
    if (!lastHistory.equals(nextHistory)) {
      console.log('[WEBVIEW] nextSource', nextSource);
      if (
        !lastSource ||
        !nextSource ||
        lastSource.context !== nextSource.context
      ) {
        console.log('[WEBVIEW] UPDATE! 3');
        return true;
      }
      const lastSourceUrl = lastSource.inputUrl || lastSource.uri;
      const nextSourceUrl = nextSource.inputUrl || nextSource.uri;
      const lastDisplayUrl = lastSourceUrl.replace(/m\.|www\./gi, '');
      const nextDisplayUrl = nextSourceUrl.replace(/m\.|www\./gi, '');
      if (lastDisplayUrl === nextDisplayUrl) {
        console.log('[WEBVIEW] DO NOT UPDATE! 6');
        return false;
      } else if (lastSource.uri === nextSource.uri) {
        const parsedNextUrl = basicParseURL(nextDisplayUrl);
        const nextBaseFsUri = nextSource.uri.substr(
          0,
          nextSource.uri.lastIndexOf('/'),
        );
        // const nextBaseFsUri = nextSource.uri.replace(
        //   new RegExp(parsedNextUrl.pathname + '$'),
        //   '',
        // );
        const updateLocation = `window.baseFsUri = '${nextBaseFsUri}';
        window.fakeLocation = new Proxy({
          href: '${parsedNextUrl.href}',
          protocol: '${parsedNextUrl.protocol}',
          host: '${parsedNextUrl.host}',
          hostname: '${parsedNextUrl.hostname}',
          port: '${parsedNextUrl.port}',
          pathname: '${parsedNextUrl.pathname}',
          search: '${parsedNextUrl.search}',
          hash: '${parsedNextUrl.hash}',
          origin: '${parsedNextUrl.origin}',
          reload: () => {window.location.reload()},
          assign: (url) => {window.location.assign(url)},
          replace: (url) => {window.location.replace(url)},
          toString: () => {return '${parsedNextUrl}'},
         },
         {
           set: function(target, key, value) {
             window.location[key] = value;
             return Reflect.set(target, key, value);
           },
         });
         true;`;
        if (this.webView) {
          console.log('[WEBVIEW] Injecting new fakeLocation:', updateLocation);
          this.webView.injectJavaScript(updateLocation);
        }
        console.log('[WEBVIEW] DO NOT UPDATE! 7');
        return false;
      }
      console.log('[WEBVIEW] UPDATE! 4');
      this.maybeHardReset(lastSource, nextSource);
      return true;
    }
    if (!this.state.renderedOnce && nextState.renderedOnce) {
      console.log('[WEBVIEW] UPDATE! 5');
      return true;
    }
    console.log('[WEBVIEW] DO NOT UPDATE! 8');
    return false;
  }

  getSiteTheme = async () => {
    try {
      // HACK - RNViewShot sometimes uses the previous snapshot, so this fires it twice
      const throwawayUri = await this.viewShot.capture();
      setTimeout(async () => {
        let dataUri = await this.viewShot.capture();
        let fixedDataUri = dataUri.replace(/\\/g, '\\\\').replace(/`/g, '\\`');
        const injection = makeThemeInjection(fixedDataUri);
        if (this.webView) {
          console.log('[WEBVIEW] Injecting theme retrieval...');
          this.webView.injectJavaScript(injection);
        }
      }, 500);
    } catch (error) {
      console.log('Palette Error:', error);
    }
  };

  onNavigationStateChange = ({
    canGoBack,
    canGoForward,
    loading,
    navigationType,
    target,
    title,
    url,
  }) => {
    const {
      props: {tabIndex, tabHistory},
    } = this;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    let currentPage = history.get(currentIndex);
    if (
      !url ||
      url === 'about:blank' ||
      url === currentPage.uri ||
      url.indexOf('file:///') === 0
    ) {
      if (url === 'about:blank' && !this.state.renderedOnce) {
        console.log('[WEBVIEW] renderedOnce set true');
        this.setState({renderedOnce: true});
      }
      // TODO H - Support back and forward navigation with # URLs
      //        - Push them to tabHistory but don't re-render unless navBack or navFwd
      return;
    }
    // one way to handle errors is via query string
    if (url.includes('?errors=true') && this.webView) {
      this.webView.stopLoading();
    }
    if (navigationType === undefined || navigationType === 'other') {
      this.props.pushHistory(tabIndex, {uri: url});
    } else if (navigationType === 'backforward') {
      this.props.parseBackForward(tabIndex, {uri: url});
    }
    if (
      navigationType === undefined ||
      navigationType === 'other' ||
      navigationType === 'backforward'
    ) {
      // NOTE - You may need to remove or modify this if it causes you problems
      this.props.pushHistory(tabIndex, {uri: url});
    }
    this.props.setCanGoBack(tabIndex, canGoBack);
    this.props.setCanGoForward(tabIndex, canGoForward);
  };

  onUrlIntercept = ({
    title,
    url,
    navigationType,
    target,
    lockIdentifier,
  }: request) => {
    console.log(
      '[WEBVIEW] URL Intercepted!',
      title,
      url,
      navigationType,
      target,
      lockIdentifier,
    );
    this.lastInterceptedUrl = url;
    return true;
  };

  addNodeListener = listenerId => {
    this.listeners.push(listenerId);
    const requestingTabIndex = this.props.tabIndex;
    const {
      currentIndex: startingIndex,
      history: startingHistory,
    } = this.props.tabHistory.get(requestingTabIndex);
    const requestingPage = startingHistory.get(startingIndex);
    const requestingUrl = requestingPage.inputUrl || requestingPage.uri;
    nodejs.channel.addListener(listenerId, results => {
      const {currentIndex, history} = this.props.tabHistory.get(
        this.props.tabIndex,
      );
      const currentPage = history.get(currentIndex);
      const currentUrl = currentPage.inputUrl || currentPage.uri;
      if (
        requestingTabIndex === this.props.tabIndex &&
        startingIndex === currentIndex &&
        requestingUrl === currentUrl
      ) {
        console.log(
          '[WEBVIEW] Got result:',
          results,
          'for listenerId:',
          listenerId,
        );
        if (results !== undefined && results !== null && results.bodyId) {
          this.addNodeListener(results.bodyId);
        }
        let run = `
          if (!window.bridgeCallbacks || typeof window.bridgeCallbacks !== 'object') {
            window.bridgeCallbacks = {};
          }
          window.bridgeCallbacks["${listenerId}"] = JSON.parse(\`${JSON.stringify(
          results,
        )
          .replace(/\\/g, '\\\\')
          .replace(/`/g, '\\`')}\`);
          true;
        `;
        if (
          results !== undefined &&
          results !== null &&
          ((results.iteration !== undefined &&
            results.iteration !== null &&
            results.result) ||
            (results.end !== undefined && results.end !== null))
        ) {
          run = `
            if (!window.iteratingBridgeCallbacks || typeof window.iteratingBridgeCallbacks !== 'object') {
              window.iteratingBridgeCallbacks = {};
            }
            var iterationResult = JSON.parse(\`${JSON.stringify(results)
              .replace(/\\/g, '\\\\')
              .replace(/`/g, '\\`')}\`);
            if (!window.iteratingBridgeCallbacks["${listenerId}"]) {
              window.iteratingBridgeCallbacks["${listenerId}"] = [iterationResult];
            } else {
              window.iteratingBridgeCallbacks["${listenerId}"].push(iterationResult);
            }
            true;
          `;
        }
        if (results !== undefined && results !== null && results.eventName) {
          run = `
            if (!window.bridgeEmitters || typeof window.bridgeEmitters !== 'object') {
              window.bridgeEmitters = {};
            }
            if (window.bridgeEmitters["${results.bridgeId}"]) {
              window.bridgeEmitters["${
                results.bridgeId
              }"].dispatchEvent(\`${results.eventName
            .replace(/\\/g, '\\\\')
            .replace(
              /`/g,
              '\\`',
            )}\`, ...(unBridgify(JSON.parse(\`${JSON.stringify(
            results.callbackArgs,
          )
            .replace(/\\/g, '\\\\')
            .replace(/`/g, '\\`')}\`))));
            }
            true;
          `;
        }
        // TODO M? - Request maybe should get rid of bridgeCallbacks after the result is returned, for security?
        console.log('[WEBVIEW] Injecting JS...', run);
        if (this.webView) {
          this.webView.injectJavaScript(run);
        }
      }
    });
  };

  sendNodeRequest = data => {
    const {
      props: {tabIndex, tabHistory, siteInfo},
    } = this;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    let currentPage = history.get(currentIndex);

    // HACK - MD Files
    data = data.replace(/-md\.html/g, '.md');

    let jsData = JSON.parse(data);

    if (jsData.args && jsData.args.apiName) {
      jsData.args.currentUrl = currentPage.inputUrl || currentPage.uri;
      jsData.args.fsPath = currentPage.inputUrl ? currentPage.uri : undefined;
    }
    if (
      jsData.args &&
      jsData.args.argumentsList &&
      Array.isArray(jsData.args.argumentsList)
    ) {
      jsData.args.argumentsList = jsData.args.argumentsList.map(request => {
        if (
          Array.isArray(request) &&
          request[1] &&
          typeof request[1] === 'object' &&
          request[1].headers &&
          request[1].headers['Site-Info']
        ) {
          request[1].headers['Site-Info'] = siteInfo.get(
            request[1].headers['Site-Info'],
          );
        } else if (
          Array.isArray(request) &&
          request[0] &&
          typeof request[0] === 'object' &&
          request[0].headers &&
          request[0].headers['Site-Info']
        ) {
          request[0].headers['Site-Info'] = siteInfo.get(
            request[0].headers['Site-Info'],
          );
        } else if (
          typeof request === 'object' &&
          request.headers &&
          request.headers['Site-Info']
        ) {
          request.headers['Site-Info'] = siteInfo.get(
            request.headers['Site-Info'],
          );
        }
        return request;
      });
    }
    if (currentPage.inputUrl) {
      // TODO H - Go through all string values and remove any cases of absolute fs path
    }
    console.log('[WEBVIEW] jsData pre-node', jsData);
    this.addNodeListener(jsData.requestId);
    nodejs.channel.post(jsData.channelId, jsData.args);
  };

  getWindowInjectionString = () => {
    const {
      props: {tabIndex, tabHistory, webViewInjection, apis},
    } = this;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    let currentPage = history.get(currentIndex);
    let injectionRoot = webViewInjection.get('root');
    if (!injectionRoot) {
      return '(function(){})();';
    }
    let windowInjectionString = injectionRoot;
    // Set fakeLocation
    const fullUrl = currentPage.inputUrl || currentPage.uri;
    const parsedUrl = basicParseURL(fullUrl);
    windowInjectionString = windowInjectionString.replace(
      '__FAKE_LOCATION__',
      `new Proxy({
        href: '${parsedUrl.href}',
        protocol: '${parsedUrl.protocol}',
        host: '${parsedUrl.host}',
        hostname: '${parsedUrl.hostname}',
        port: '${parsedUrl.port}',
        pathname: '${parsedUrl.pathname}',
        search: '${parsedUrl.search}',
        hash: '${parsedUrl.hash}',
        origin: '${parsedUrl.origin}',
        reload: () => {window.location.reload()},
        assign: (url) => {window.location.assign(url)},
        replace: (url) => {window.location.replace(url)},
        toString: () => {return '${fullUrl}'},
       },
       {
         set: function(target, key, value) {
           window.location[key] = value;
           return Reflect.set(target, key, value);
         },
       })`,
    );
    // Set Base FS URI
    const originalUri = currentPage.uri || '/';
    const baseFsUri = originalUri.substr(0, originalUri.lastIndexOf('/'));
    // const baseFsUri = currentPage.uri.replace(
    //   new RegExp(parsedUrl.pathname + '$'),
    //   '',
    // );
    windowInjectionString = windowInjectionString.replace(
      '__BASE_FS_URI__',
      baseFsUri,
    );
    // Add APIs to window
    const windowifyObject = (obj, parentName) => {
      let string = '';
      for (const key in obj) {
        const val = obj[key];
        const keyWithParent = parentName + '.' + key;
        if (typeof val === 'object') {
          string =
            string +
            '\n' +
            `window.${keyWithParent} = {};` +
            windowifyObject(val, keyWithParent);
        }
        if (typeof val === 'string') {
          const isAsync = val.indexOf('async ') === 0 ? 'true' : 'false';
          string =
            string +
            '\n' +
            `window.${keyWithParent} = makeApiFunction("${keyWithParent}", ${isAsync});`;
        }
      }
      return string;
    };
    apis.mapEntries(([key, value]) => {
      if (typeof value === 'object') {
        windowInjectionString =
          windowInjectionString +
          '\n' +
          `window.${key} = {};` +
          windowifyObject(value, key);
      }
      if (typeof value === 'string') {
        const isAsync = value.indexOf('async ') === 0 ? 'true' : 'false';
        windowInjectionString =
          windowInjectionString +
          '\n' +
          `window.${key} = makeApiFunction("${key}", ${isAsync});`;
      }
    });
    return '(function(){\n' + windowInjectionString + '\n})();';
  };

  onContentProcessDidTerminate = syntheticEvent => {
    const {nativeEvent} = syntheticEvent;
    console.log('[WEBVIEW] Content process terminated, reloading', nativeEvent);
    if (this.webView && this.webView.reload) {
      this.webView.reload();
    }
  };

  render() {
    const {
      onContentProcessDidTerminate,
      getWindowInjectionString,
      onNavigationStateChange,
      addNodeListener,
      sendNodeRequest,
      onUrlIntercept,
      getSiteTheme,
      props: {
        tabError,
        tabIndex,
        tabHistory,
        pushHistory,
        setTabError,
        setThemeChameleon,
        setNavigationProgress,
        navigationMeasurements,
      },
      state: {renderedOnce, hardReset},
    } = this;
    const {currentIndex, history} = tabHistory.get(tabIndex);
    const currentPage = history.get(currentIndex);
    let source = currentPage;
    if (source.context && source.context !== 'site') {
      return null;
    }
    const currentError = tabError.get(tabIndex);
    if (
      currentError &&
      currentError.source &&
      source.uri === currentError.source.uri &&
      source.inputUrl === currentError.source.inputUrl &&
      source.context === currentError.source.context
    ) {
      return null;
    }
    if (hardReset || (source.inputUrl && this.startingUri === null)) {
      console.log('[WEBVIEW] Rendering placeholder...');
      return <View style={[styles.placeholder, {marginTop}]} />;
    }
    if (source.inputUrl) {
      source = {uri: `file://${source.uri}`};
    }
    const readAccessURL =
      !source || source.uri.indexOf(RNFS.DocumentDirectoryPath) !== -1
        ? `file://${RNFS.DocumentDirectoryPath}`
        : `file://${RNFS.TemporaryDirectoryPath}`;
    const windowInjectionString = getWindowInjectionString();
    const marginTop = navigationMeasurements.get('topBarHeight');
    console.log('[WEBVIEW] Source:', source);
    return (
      <ViewShot
        style={[styles.webView, {marginTop}]}
        ref={ref => (this.viewShot = ref)}
        options={{
          format: 'jpg',
          quality: 0.5,
          result: 'data-uri',
        }}>
        <View style={styles.webView} collapsable={false}>
          <WebView
            ref={ref => (this.webView = ref)}
            source={renderedOnce ? source : null}
            style={styles.webView}
            javaScriptEnabled
            domStorageEnabled
            sharedCookiesEnabled={false}
            thirdPartyCookiesEnabled={false}
            allowsFullscreenVideo
            allowsInlineMediaPlayback
            allowsBackForwardNavigationGestures={false}
            mediaPlaybackRequiresUserAction={false}
            allowFileAccessFromFileURLs
            allowUniversalAccessFromFileURLs
            allowFileAccess
            allowingReadAccessToURL={readAccessURL}
            geolocationEnabled
            decelerationRate={0.998}
            dataDetectorTypes="all"
            originWhitelist={['*']}
            mixedContentMode="always"
            onNavigationStateChange={onNavigationStateChange}
            onShouldStartLoadWithRequest={onUrlIntercept}
            onLoadProgress={({nativeEvent}) => {
              if (currentPage.uri) {
                setNavigationProgress({
                  tabIndex,
                  url: currentPage.uri,
                  progress: nativeEvent.progress,
                });
              }
            }}
            renderLoading={() => null}
            renderError={errorName => null}
            onError={syntheticEvent => {
              // NOTE - nativeEvent: {description, domain, code, [statusCode], didFailProvisionalNavigation}
              const {nativeEvent} = syntheticEvent;
              console.log('[WEBVIEW] Error: ', nativeEvent);
              if (
                nativeEvent.code === -1002 ||
                nativeEvent.description === 'unsupported URL'
              ) {
                console.log(
                  '[WEBVIEW] Push History from WebView Error:',
                  this.lastInterceptedUrl,
                );
                // const parsedNextUrl = basicParseURL(this.lastInterceptedUrl);
                // // const nextBaseFsUri = nextSource.uri.substr(
                // //   0,
                // //   nextSource.uri.lastIndexOf('/'),
                // // );
                // // window.baseFsUri = '${nextBaseFsUri}';
                // const updateLocation = `window.fakeLocation = new Proxy({
                //   href: '${parsedNextUrl.href}',
                //   protocol: '${parsedNextUrl.protocol}',
                //   host: '${parsedNextUrl.host}',
                //   hostname: '${parsedNextUrl.hostname}',
                //   port: '${parsedNextUrl.port}',
                //   pathname: '${parsedNextUrl.pathname}',
                //   search: '${parsedNextUrl.search}',
                //   hash: '${parsedNextUrl.hash}',
                //   origin: '${parsedNextUrl.origin}',
                //   reload: () => {window.location.reload()},
                //   assign: (url) => {window.location.assign(url)},
                //   replace: (url) => {window.location.replace(url)},
                //   toString: () => {return '${parsedNextUrl}'},
                //  },
                //  {
                //    set: function(target, key, value) {
                //      window.location[key] = value;
                //      return Reflect.set(target, key, value);
                //    },
                //  });
                //  true;`;
                // if (this.webView) {
                //   console.log(
                //     '[WEBVIEW] Injecting new fakeLocation:',
                //     updateLocation,
                //   );
                //   this.webView.injectJavaScript(updateLocation);
                // }
                pushHistory(tabIndex, {
                  inputUrl: this.lastInterceptedUrl,
                  request: true,
                });
              } else {
                setTabError({
                  tabIndex,
                  error: {
                    domain: currentPage.inputUrl || currentPage.uri,
                    statusCode: nativeEvent.statusCode,
                    code: nativeEvent.code,
                    category: 'WebView Error',
                  },
                });
              }
            }}
            onHttpError={syntheticEvent => {
              const {nativeEvent} = syntheticEvent;
              console.log('[WEBVIEW] Error HTTP: ', nativeEvent);
              setTabError({
                tabIndex,
                error: {
                  domain: currentPage.inputUrl || currentPage.uri,
                  statusCode: nativeEvent.statusCode,
                  code: nativeEvent.code,
                  category: 'HTTP Error',
                },
              });
            }}
            onContentProcessDidTerminate={onContentProcessDidTerminate}
            injectedJavaScriptForMainFrameOnly={false}
            injectedJavaScriptBeforeContentLoadedForMainFrameOnly={false}
            injectedJavaScript={windowInjectionString}
            injectedJavaScriptBeforeContentLoaded={windowInjectionString}
            onMessage={({nativeEvent: {data}}) => {
              console.log('[WEBVIEW] ON MESSAGE:', data);
              if (
                data.indexOf('"requestId":') !== -1 &&
                data.indexOf('"channelId":') !== -1
              ) {
                sendNodeRequest(data);
              }
              if (data.indexOf('"addListener":') !== -1) {
                let jsData = JSON.parse(data);
                addNodeListener(jsData.addListener);
              }
              if (data === 'Requesting Theme!') {
                getSiteTheme();
              }
              if (data.indexOf('"theme":') !== -1) {
                const theme = JSON.parse(data).theme;
                setThemeChameleon(theme);
              }
            }}
          />
        </View>
      </ViewShot>
    );
  }
}

const styles = StyleSheet.create({
  placeholder: {
    flex: 1,
    backgroundColor: 'white',
  },
  webView: {
    flex: 1,
  },
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps(state) {
  return {
    navigationMeasurements: state.navigationMeasurements,
    webViewInjection: state.webViewInjection,
    browserCommand: state.browserCommand,
    tabHistory: state.tabHistory,
    siteInfo: state.siteInfo,
    tabError: state.tabError,
    setup: state.setup,
    apis: state.apis,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomWebView);
