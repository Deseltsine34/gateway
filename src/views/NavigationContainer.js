// TODO H - Place header on top of all Gateway files
/* Gateway Browser
 * Copyright (C) 2020 Gateway Browser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/**
 * @format
 * @flow
 */

// React Libraries
import React, {memo, useState, useEffect} from 'react';
import {Linking, Platform} from 'react-native';
import {ActivityIndicator, Dimensions, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
var RNFS = require('react-native-fs');

// Redux Libraries
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// React Navigation Libraries
import {NavigationNativeContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Node.JS Libraries
import nodejs from 'nodejs-mobile-react-native';
const basicParseURL = require('url-parse');

import {webViewInjectionRoot} from '../lib/webview-injection/node-web-bridge';
import TabHistoryPopupScreen from '../screens/TabHistoryPopupScreen';
import OptionsPopupScreen from '../screens/OptionsPopupScreen';
import ProtocolInfoScreen from '../screens/ProtocolInfoScreen';
import BrowserTabScreen from '../screens/BrowserTabScreen';
import NavigationService from '../lib/NavigationService';
import DashboardScreen from '../screens/DashboardScreen';
import QRScanScreen from '../screens/QRScanScreen';
import ShareScreen from '../screens/ShareScreen';
import {ActionCreators} from '../actions';
import {fetchHandler} from '../lib/proxy';
import {capitalize} from '../lib/common';

const Stack = createStackNavigator();
const PERSISTENCE_KEY = 'NAVIGATION_STATE';

const setStrongProp = (target, prop, value) => {
  if (typeof value === 'object') {
    value = Object.freeze(value);
  }
  Object.defineProperty(target, prop, {
    value,
    writable: false,
    enumerable: false,
    configurable: false,
  });
};

const randomInteger = () => {
  return Math.floor(Math.random() * Math.floor(100000));
};

const NavigationContainer = memo(
  ({
    setDimensions,
    pushHistory,
    setWebViewInjection,
    setNavigationProgress,
    startBrowser,
    setTabError,
    setSiteInfo,
    protocols,
    apis,
  }: props) => {
    const [isReady, setIsReady] = useState(__DEV__ ? false : true);
    const [initialState, setInitialState] = useState();

    const getDimensions = () => {
      const {height, width} = Dimensions.get('window');
      const orientation = height >= width ? 'portrait' : 'landscape';
      return {orientation, height, width};
    };

    const openUrl = (url, initial) => {
      if (initial) {
        console.log('[Linking] Initial url is:', url);
      } else {
        console.log('[Linking] URL from listener:', url);
      }
      // TODO M - Open link in new tab when tabs become implemented
      const tabIndex = 0;
      if (url.search(/file:\/\/|https?:\/\//i) !== 0) {
        pushHistory(tabIndex, {inputUrl: url, request: 'site'});
      } else {
        pushHistory(tabIndex, {uri: url});
      }
    };

    const setupLinking = () => {
      Linking.getInitialURL()
        .then(url => {
          if (url) {
            openUrl(url, true);
          }
        })
        .catch(err =>
          console.error('[Linking] Initial linking error occurred', err),
        );
      Linking.addEventListener('url', ({url}) => {
        openUrl(url, false);
      });
    };

    const setup = () => {
      SplashScreen.hide();
      setupLinking();
      startBrowser();
    };

    const setupNode = () => {
      // NOTE - For security, node.js listeners are generated on startup so nothing running in node.js can call them directly
      // Can safely ignore: 'decode-qr'
      const navigationProgressListener = `${randomInteger()}${randomInteger()}`;
      const setupCallbackListener = `${randomInteger()}${randomInteger()}`;
      const setSiteInfoListener = `${randomInteger()}${randomInteger()}`;
      const nodeRequestListener = `${randomInteger()}${randomInteger()}`;
      const apiRequestListener = `${randomInteger()}${randomInteger()}`;
      const nodeErrorListener = `${randomInteger()}${randomInteger()}`;
      const parseURLListener = `${randomInteger()}${randomInteger()}`;
      const fetchListener = `${randomInteger()}${randomInteger()}`;

      const webViewInjectionWithListener = webViewInjectionRoot
        .replace(/__NODE_REQUEST_LISTENER_ID__/g, nodeRequestListener)
        .replace(/__API_REQUEST_LISTENER_ID__/g, apiRequestListener)
        .replace(/__PARSE_URL_LISTENER_ID__/g, parseURLListener)
        .replace(/__FETCH_LISTENER_ID__/g, fetchListener)
        .replace(
          /__CACHE_URI__/g,
          RNFS.TemporaryDirectoryPath.replace('file://', ''),
        );
      setWebViewInjection(webViewInjectionWithListener);

      if (Platform.OS === 'ios') {
        nodejs.start('main.js');
      }

      // Global adaptable parseURL
      function parseURL() {
        return new Promise((resolve, reject) => {
          const url = arguments[0];
          if (!url) {
            return reject('No URL to parse.');
          }
          const urlString = typeof url === 'string' ? url : url.href;
          if (
            urlString.search(/https?:\/\//i) === -1 &&
            urlString.search(/file:\/\//i) === -1
          ) {
            const randomInt = Math.floor(Math.random() * Math.floor(100000));
            const requestId =
              'parseURL*' + new Date().getTime() + '*' + randomInt;
            nodejs.channel.addListener(requestId, nodeParsed => {
              if (nodeParsed.error) {
                reject(nodeParsed);
              } else {
                resolve(nodeParsed);
              }
            });
            nodejs.channel.post(parseURLListener, {
              requestId,
              url,
            });
          } else {
            let parsed = basicParseURL(url);
            if (!parsed.pathname || parsed.pathname === '') {
              parsed.pathname = '/';
            }
            resolve(parsed);
          }
        });
      }
      global.parseURL = parseURL;

      // Modify fetch functionality
      if (!global.basicFetch) {
        global.basicFetch = fetch;
        global.fetch = new Proxy(fetch, fetchHandler(fetchListener));
        // The query and write functions provide simpler, possibly more familiar, syntax
        // for common fetching cases (GET and PUT, respectively).
        const optionsToHeader = options => {
          let newOptions = {};
          for (const key in options) {
            let newKey = key.replace(/([a-z])([A-Z])/g, '$1-$2');
            newKey = capitalize(newKey);
            newOptions[newKey] = options[key];
          }
          return newOptions;
        };
        const queryOrWrite = async (method, ...params) => {
          let parameters = [...params];
          if (parameters.length === 0) {
            return;
          }
          let fetchMethod;
          if (parameters.length === 1) {
            const param = parameters[0];
            if (typeof param === 'string') {
              // Single Request
              fetchMethod = await fetch(param, {method});
            }
            if (typeof param === 'object') {
              // Single Request
              const newOptions = optionsToHeader(param.options);
              fetchMethod = await fetch(param.url, {...newOptions, method});
            } else {
              // Multiple Requests
              let newParam = param.map(p => {
                if (typeof p === 'object') {
                  return optionsToHeader(p.options);
                } else {
                  return optionsToHeader(p[1]);
                }
              });
              fetchMethod = await fetch(newParam);
            }
          } else {
            // Single Request
            const newOptions = optionsToHeader(parameters[1]);
            fetchMethod = await fetch(parameters[0], {...newOptions, method});
          }
          if (Array.isArray(fetchMethod)) {
            const fetchBodies = fetchMethod.map(res => res.body);
            return fetchBodies;
          }
          return fetchMethod.body;
        };
        global.query = (...params) => queryOrWrite('GET', ...params);
        global.write = (...params) => queryOrWrite('PUT', ...params);
      }

      nodejs.channel.addListener('message', msg => {
        console.log('[NODE] MESSAGE:', msg);
      });
      nodejs.channel.addListener('exception', msg => {
        console.log('[NODE] EXCEPTION:', msg);
      });
      nodejs.channel.addListener(setupCallbackListener, msg => {
        console.log('[NODE]', msg);
        if (msg === 'NODE IS SET UP!') {
          setup();
        }
      });
      nodejs.channel.addListener(nodeErrorListener, msg => {
        setTabError(msg);
      });
      nodejs.channel.addListener(navigationProgressListener, msg => {
        setNavigationProgress(msg);
      });
      nodejs.channel.addListener(setSiteInfoListener, msg => {
        console.log('[BROWSER] set-site-info', msg);
        setSiteInfo(msg);
      });
      nodejs.channel.post('setup-node', {
        apis,
        protocols,
        settings: {},
        listeners: {
          'navigation-progress': navigationProgressListener,
          'setup-callback': setupCallbackListener,
          'set-site-info': setSiteInfoListener,
          'node-request': nodeRequestListener,
          'api-request': apiRequestListener,
          'node-error': nodeErrorListener,
          parseURL: parseURLListener,
          fetch: fetchListener,
        },
      });
    };

    useEffect(() => {
      // Event Listener for orientation changes
      Dimensions.addEventListener('change', () => {
        const dim = getDimensions();
        setDimensions(dim);
      });

      return () => {
        // TODO H - Stop listeners when app unmounts
        // TODO H - Make sure node layer is reopened if stopped (properly shut it down if app quits?)
        console.log('componentWillMount?');
      };
    }, []);

    useEffect(() => {
      const restoreState = async () => {
        try {
          const savedStateString = await AsyncStorage.getItem(PERSISTENCE_KEY);
          const state = JSON.parse(savedStateString);

          setInitialState(state);
        } finally {
          setIsReady(true);
        }
      };

      if (!isReady) {
        restoreState();
      } else {
        setupNode();
      }
    }, [isReady]);

    if (!isReady) {
      return <ActivityIndicator size="large" />;
    }

    const modalCardOptions = {
      // stackPresentation:
      //   Platform.OS === 'android' ? 'transparentModal' : undefined,
      // contentStyle: Platform.OS === 'android' ? styles.cardStyle : undefined,
      // cardStyle: Platform.OS === 'ios' ? styles.cardStyle : undefined,
      stackPresentation: 'transparentModal',
      contentStyle: styles.cardStyle,
      cardStyle: styles.cardStyle,
    };

    return (
      <NavigationNativeContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
        initialState={initialState}
        onStateChange={state =>
          AsyncStorage.setItem(PERSISTENCE_KEY, JSON.stringify(state))
        }>
        <Stack.Navigator
          mode="modal"
          initialRouteName="Tab"
          keyboardHandlingEnabled={true}
          screenOptions={{
            cardStyle: {backgroundColor: 'transparent'},
            gestureEnabled: false,
            animationEnabled: false,
            cardStyleInterpolator: ({current: {progress}}) => ({
              cardStyle: {
                opacity: progress.interpolate({
                  inputRange: [0, 0.5, 0.9, 1],
                  outputRange: [0, 0.25, 0.7, 1],
                }),
              },
              overlayStyle: {
                opacity: progress.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 0.5],
                  extrapolate: 'clamp',
                }),
              },
            }),
            cardOverlayEnabled: false,
          }}
          headerMode="none">
          <Stack.Screen
            name="Tab"
            params={{index: 0}}
            component={BrowserTabScreen}
          />
          <Stack.Screen
            name="TabHistoryPopup"
            options={modalCardOptions}
            component={TabHistoryPopupScreen}
          />
          <Stack.Screen
            name="OptionsPopup"
            options={modalCardOptions}
            component={OptionsPopupScreen}
          />
          <Stack.Screen
            name="ProtocolInfo"
            options={modalCardOptions}
            component={ProtocolInfoScreen}
          />
          <Stack.Screen
            name="Dashboard"
            options={modalCardOptions}
            component={DashboardScreen}
          />
          <Stack.Screen
            name="Share"
            options={modalCardOptions}
            component={ShareScreen}
          />
          <Stack.Screen
            name="QRScanner"
            options={{animationEnabled: true}}
            component={QRScanScreen}
          />
        </Stack.Navigator>
      </NavigationNativeContainer>
    );
  },
  function(prevProps, nextProps) {
    return true;
  },
);

const styles = StyleSheet.create({
  cardStyle: {backgroundColor: 'transparent'},
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps(state) {
  return {
    protocols: state.protocols,
    apis: state.apis,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationContainer);
