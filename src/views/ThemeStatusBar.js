/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {StatusBar, Keyboard} from 'react-native';
import {connect} from 'react-redux';

const ThemeStatusBar = ({theme}: props) => {
  const [mounted, setMounted] = useState(false);
  const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
  const backgroundColor = theme.get(subState).get('backgroundColor');
  const textColor = theme.get(subState).get('textColor');
  const rgb = textColor
    .replace('rgba(', '')
    .replace(',1.0)', '')
    .split(',');
  const barStyle =
    (rgb[0] * 299 + rgb[1] * 587 + rgb[2] * 114) / 1000 < 150
      ? 'dark-content'
      : 'light-content';
  useEffect(() => {
    const onKeyboardShow = () => {
      StatusBar.setBarStyle(barStyle);
    };
    if (mounted) {
      Keyboard.removeListener('keyboardWillShow', onKeyboardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyboardShow);
    } else {
      setMounted(true);
    }
    Keyboard.addListener('keyboardWillShow', onKeyboardShow);
    Keyboard.addListener('keyboardDidHide', onKeyboardShow);
    StatusBar.setBarStyle(barStyle);
    return () => {
      Keyboard.removeListener('keyboardWillShow', onKeyboardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyboardShow);
      StatusBar.setBarStyle(barStyle);
    };
  }, [barStyle, mounted]);
  return <StatusBar barStyle={barStyle} backgroundColor={backgroundColor} />;
};

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(mapStateToProps)(ThemeStatusBar);
