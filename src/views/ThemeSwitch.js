/**
 * @format
 * @flow
 */

import React from 'react';
import {Switch} from 'react-native';
import {connect} from 'react-redux';

const ThemeSwitch = ({theme, value, style, ...props}: props) => {
  const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
  const highlightColor = theme.get(subState).get('highlightColor');
  const semiHighlightColor = highlightColor.replace('1.0', '0.5');
  const textColor = theme.get(subState).get('textColor');
  const semiTextColor = textColor.replace('1.0', '0.7');
  return (
    <Switch
      {...props}
      trackColor={{false: semiTextColor, true: semiHighlightColor}}
      thumbColor={value ? highlightColor : textColor}
      ios_backgroundColor={semiTextColor}
      value={value}
      style={style}
    />
  );
};

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(mapStateToProps)(ThemeSwitch);
