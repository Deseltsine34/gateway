/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Animated} from 'react-native';
import {connect} from 'react-redux';

class ThemeView extends Component {
  constructor() {
    super();
    this.animating = false;
    this.pendingColorQueue = [];
    this.state = {
      animation: new Animated.Value(0),
      outputRange: [],
    };
  }
  getColor({type, theme, opacity}) {
    const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
    const themeType = type || 'backgroundColor';
    const color = theme.get(subState).get(themeType);
    const currentColor = opacity
      ? color.replace('1.0', opacity.toString())
      : color;
    return currentColor;
  }
  animateColor() {
    this.animating = true;
    this.state.animation.setValue(0);
    Animated.timing(this.state.animation, {
      toValue: this.state.outputRange.length - 1,
      duration: 250,
      useNativeDriver: true,
    }).start(() => {
      if (this.pendingColorQueue.length > 0) {
        let lastColor = this.state.outputRange[
          this.state.outputRange.length - 1
        ];
        let newRange = this.pendingColorQueue.slice();
        newRange.unshift(lastColor);
        this.pendingColorQueue = [];
        this.setState({outputRange: newRange});
      } else {
        this.animating = false;
        // this.setState({outputRange: []});
      }
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    const thisColor = this.getColor(this.props);
    const nextColor = this.getColor(nextProps);
    if (thisColor !== nextColor) {
      if (this.animating) {
        this.pendingColorQueue.push(nextColor);
        return false;
      } else {
        this.setState({outputRange: [thisColor, nextColor]});
      }
    }
    return true;
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const prevRangePrime =
      prevState.outputRange.length === 0 ? undefined : prevState.outputRange[0];
    if (
      this.state.outputRange.length >= 2 &&
      this.state.outputRange[0] !== prevRangePrime
    ) {
      this.animateColor();
    }
  }
  render() {
    const {
      props: {type, theme, children, style, opacity, ...props},
      state: {outputRange},
    } = this;
    let backgroundColor;
    if (outputRange.length < 2) {
      const currentColor = this.getColor(this.props);
      backgroundColor = currentColor;
    } else {
      let inputRange = Array(outputRange.length)
        .fill()
        .map((x, i) => i);
      backgroundColor = this.state.animation.interpolate({
        inputRange,
        outputRange,
      });
    }
    return (
      <Animated.View {...props} style={[style, {backgroundColor}]}>
        {children}
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(mapStateToProps)(ThemeView);
