/**
 * @format
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity, FlatList, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {systemWeights} from 'react-native-typography';
const basicParseURL = require('url-parse');

import {ActionCreators} from '../actions';
import {formatSizeString, getInfoKey} from '../lib/common';
import ThemeText from './ThemeText';

const DirectoryPathList = ({
  tabIndex,
  contents,
  stat,
  pushHistory,
  onHeight,
  tabHistory,
  siteInfo,
}: props) => {
  const onLayout = ({
    nativeEvent: {
      layout: {height},
    },
  }) => {
    if (onHeight) {
      onHeight(height);
    }
  };
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const source = history.get(currentIndex);
  const url = source.inputUrl || source.uri;
  const infoKey = getInfoKey(url);
  let info =
    infoKey && siteInfo.get(infoKey) ? siteInfo.get(infoKey).info : undefined;
  if (!info) {
    info = {};
  }
  const {protocol, hostname, pathname} = basicParseURL(url);
  let title = info.title || hostname;
  if (title.length > 30) {
    title = title.substr(0, 6) + '..' + title.substr(title.length - 2);
  }
  const preArrayPath = pathname.endsWith('/')
    ? pathname.substring(0, pathname.length - 1)
    : pathname;
  const pathArray = preArrayPath.split(/(\/)/);
  let totalFileSize;

  if (contents && contents.length > 0 && typeof contents[0] === 'object') {
    totalFileSize = contents.reduce((a, b) => a + (b.stat.size || 0), 0);
  } else if (stat) {
    totalFileSize = stat.size;
  }
  let totalSizeString = formatSizeString(totalFileSize);
  totalSizeString = totalSizeString === '' ? '0 KB' : totalSizeString || '0 KB';
  const data = [title]
    .concat(pathArray, [totalSizeString])
    .filter(obj => obj !== '');
  return (
    <FlatList
      data={data}
      horizontal={true}
      style={styles.container}
      onLayout={onLayout}
      renderItem={({item, index}) => {
        if (item === '/' || index === data.length - 1) {
          const marginRight = index === data.length - 1 ? 15 : 0;
          return (
            <View style={[styles.itemContainer, {marginRight}]}>
              <ThemeText
                allowFontScaling={false}
                style={styles.thinText}
                opacity={0.5}>
                {item}
              </ThemeText>
            </View>
          );
        }
        const isCurrent = pathname === '/' || index === data.length - 2;
        const marginLeft = index === 0 ? 15 : 0;
        const navigateToDirectory = () => {
          const newBaseUrl = protocol + '//' + hostname;
          let newPath;
          if (index === 0) {
            newPath = '/';
          } else {
            newPath = data.slice(1, index + 1).join('');
          }
          const newUrl = newBaseUrl + newPath;
          pushHistory(tabIndex, {inputUrl: newUrl, request: 'files'});
        };
        return (
          <TouchableOpacity
            disabled={isCurrent}
            style={styles.itemContainer}
            onPress={navigateToDirectory}>
            <ThemeText
              allowFontScaling={false}
              type={isCurrent ? 'textColor' : 'highlightColor'}
              style={[styles.text, {marginLeft}]}>
              {item}
            </ThemeText>
          </TouchableOpacity>
        );
      }}
      keyExtractor={(item, index) => {
        return item + index;
      }}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  itemContainer: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  text: {
    ...systemWeights.semibold,
    fontSize: 16,
  },
  thinText: {
    ...systemWeights.light,
    marginHorizontal: 10,
    fontSize: 16,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    tabHistory: state.tabHistory,
    siteInfo: state.siteInfo,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DirectoryPathList);
