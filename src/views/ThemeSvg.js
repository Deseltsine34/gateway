/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Animated, StyleSheet} from 'react-native';
import SvgRenderer from 'react-native-svg-renderer';
import {connect} from 'react-redux';

const AnimatedSvg = Animated.createAnimatedComponent(SvgRenderer);

class ThemeSvg extends Component {
  constructor() {
    super();
    this.animating = false;
    this.pendingColorQueue = [];
    this.state = {
      animation: new Animated.Value(0),
      outputRange: [],
    };
  }
  getColor({type, theme}) {
    const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
    const themeType = type || 'textColor';
    const color = theme.get(subState).get(themeType);
    return color;
  }
  animateColor() {
    this.animating = true;
    this.state.animation.setValue(0);
    Animated.timing(this.state.animation, {
      toValue: this.state.outputRange.length - 1,
      duration: 250,
      useNativeDriver: true,
    }).start(() => {
      if (this.pendingColorQueue.length > 0) {
        let lastColor = this.state.outputRange[
          this.state.outputRange.length - 1
        ];
        let newRange = this.pendingColorQueue.slice();
        newRange.unshift(lastColor);
        this.pendingColorQueue = [];
        this.setState({outputRange: newRange});
      } else {
        this.animating = false;
        // this.setState({outputRange: []});
      }
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    const thisColor = this.getColor(this.props);
    const nextColor = this.getColor(nextProps);
    if (thisColor !== nextColor) {
      if (this.animating) {
        this.pendingColorQueue.push(nextColor);
        return false;
      } else {
        this.setState({outputRange: [thisColor, nextColor]});
      }
    }
    return true;
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const prevRangePrime =
      prevState.outputRange.length === 0 ? undefined : prevState.outputRange[0];
    if (
      this.state.outputRange.length >= 2 &&
      this.state.outputRange[0] !== prevRangePrime
    ) {
      this.animateColor();
    }
  }
  render() {
    const {
      props: {type, theme, children, style, opacity, width, height, ...props},
      state: {outputRange},
    } = this;
    let color;
    if (outputRange.length < 2) {
      const currentColor = this.getColor(this.props);
      color = currentColor;
    } else {
      let inputRange = Array(outputRange.length)
        .fill()
        .map((x, i) => i);
      color = this.state.animation.interpolate({
        inputRange,
        outputRange,
      });
    }
    const wrapperOpacity = opacity || 1;
    const customStyle = style || {};
    return (
      <View
        style={[
          styles.wrapper,
          {
            opacity: wrapperOpacity,
            height: Number(height),
            width: Number(width),
          },
          customStyle,
        ]}>
        <AnimatedSvg {...props} height={height} width={width} fill={color} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
});

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(mapStateToProps)(ThemeSvg);
