/**
 * @format
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {systemWeights} from 'react-native-typography';

import ThemeSvg from './ThemeSvg';
import ThemeText from './ThemeText';
import ThemeView from './ThemeView';

const PopupModalListItem = ({
  index,
  maxIndex,
  seperator,
  icon,
  iconStyle,
  iconType,
  iconSize,
  iconWidth,
  iconHeight,
  title,
  titleType,
  numberOfLines,
  ellipsizeMode,
  opacity,
  bodyOpacity,
  bodyType,
  ...props
}: props) => {
  const marginTop = index === 0 ? 5 : 0;
  const marginBottom = index === maxIndex ? 5 : 0;
  if (seperator) {
    return (
      <ThemeView type="textColor" opacity={0.3} style={styles.seperator} />
    );
  }
  return (
    <TouchableOpacity {...props}>
      <ThemeView
        type={bodyType || 'backgroundColor'}
        opacity={bodyOpacity || 1}
        style={[styles.item, {marginTop, marginBottom}]}>
        {icon ? (
          <View style={styles.iconContainer}>
            <ThemeSvg
              type={iconType || 'highlightColor'}
              height={iconHeight || iconSize || '20'}
              width={iconWidth || iconSize || '20'}
              opacity={opacity || 1}
              style={iconStyle}
              svgXmlData={icon}
            />
          </View>
        ) : null}
        <ThemeText
          style={styles.titleText}
          numberOfLines={numberOfLines}
          ellipsizeMode={ellipsizeMode}
          opacity={opacity || 1}
          type={titleType}>
          {title}
        </ThemeText>
      </ThemeView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'transparent',
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleText: {
    ...systemWeights.regular,
    marginLeft: 10,
    fontSize: 18,
  },
  iconContainer: {
    width: 26,
    height: 26,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  seperator: {
    height: 0.25,
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 5,
  },
});

export default PopupModalListItem;
