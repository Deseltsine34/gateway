/**
 * @format
 * @flow
 */

import React from 'react';
import {TextInput, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {ActionCreators} from '../actions';

const ContentTextView = ({content, height}: props) => {
  return (
    <TextInput
      style={[
        styles.input,
        {
          height,
        },
      ]}
      multiline
      editable={false}
      underlineColorAndroid="transparent"
      value={content}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    color: 'black',
    backgroundColor: 'white',
    padding: 15,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    theme: state.theme,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContentTextView);
