/**
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet} from 'react-native';
import {systemWeights} from 'react-native-typography';

import ThemeSvg from './ThemeSvg';
import ThemeText from './ThemeText';
import {gatewaySvg} from '../svg';

const GatewayLogo = props => {
  return (
    <View style={styles.container}>
      <ThemeSvg height="26" width="26" svgXmlData={gatewaySvg} />
      <ThemeText style={styles.font} allowFontScaling={false}>
        Gateway
      </ThemeText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
  },
  font: {
    ...systemWeights.bold,
    marginLeft: 10,
    fontSize: 26,
  },
});

export default GatewayLogo;
