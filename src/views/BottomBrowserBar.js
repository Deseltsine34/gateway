/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeView from './ThemeView';
import NavigationButton from './NavigationButton';
import OptionsButton from './OptionsButton';
import OmniButton from './OmniButton';
import TabsButton from './TabsButton';
import ReloadButton from './ReloadButton';
import {ActionCreators} from '../actions';

class BottomBrowserBar extends Component {
  constructor() {
    super();
  }

  onLayout = ({
    nativeEvent: {
      layout: {height},
    },
  }) => {
    const {
      props: {setBottomBarHeight},
    } = this;
    setBottomBarHeight(height);
  };

  render() {
    const {
      onLayout,
      props: {tabIndex, deviceDimensions},
    } = this;
    const orientation = deviceDimensions.get('orientation');
    if (orientation === 'landscape') {
      return null;
    }
    return (
      <ThemeView onLayout={onLayout}>
        <SafeAreaView style={styles.bar}>
          <NavigationButton tabIndex={tabIndex} isBack />
          <NavigationButton tabIndex={tabIndex} isBack={false} />
          <OmniButton tabIndex={tabIndex} />
          <ReloadButton tabIndex={tabIndex} />
          {/*<TabsButton />*/}
          <OptionsButton tabIndex={tabIndex} />
        </SafeAreaView>
      </ThemeView>
    );
  }
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingVertical: 5,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BottomBrowserBar);
