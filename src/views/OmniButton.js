/**
 * @format
 * @flow
 */

import React, {useRef} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeSvg from './ThemeSvg';
import {hexagonSvg, gatewaySvg} from '../svg';
import {ActionCreators} from '../actions';
import NavigationService from '../lib/NavigationService';

// TODO H - Add notification display/state (only needed for first viewing in release)

const OmniButton = ({tabIndex, setOmniButtonPosition}: props) => {
  const button = useRef(null);
  const options = async () => {
    NavigationService.navigate({
      name: 'Dashboard',
      params: {
        tabIndex,
      },
    });
  };
  const onLayout = () => {
    button.current.measure((x, y, width, height, pageX, pageY) => {
      setOmniButtonPosition(pageX, pageY);
    });
  };
  return (
    <TouchableOpacity
      ref={button}
      style={styles.touchable}
      onPress={options}
      onLongPress={options}
      onLayout={onLayout}>
      <ThemeSvg height="35" width="30" svgXmlData={hexagonSvg} />
      <View style={styles.iconContainer}>
        <ThemeSvg
          type="backgroundColor"
          height="18"
          width="18"
          svgXmlData={gatewaySvg}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touchable: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OmniButton);
