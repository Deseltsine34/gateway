/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity, StyleSheet, Keyboard} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeSvg from './ThemeSvg';
import {closeKeyboardSvg} from '../svg';
import {ActionCreators} from '../actions';

const CloseKeyboardButton = () => {
  const dismiss = async () => {
    Keyboard.dismiss();
  };
  return (
    <TouchableOpacity
      style={styles.touchable}
      onPress={dismiss}
      onLongPress={dismiss}>
      <ThemeSvg width="10.6" height="16" svgXmlData={closeKeyboardSvg} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touchable: {
    height: 40,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CloseKeyboardButton);
