/**
 * @format
 * @flow
 */

import React from 'react';
import {Platform, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {systemWeights} from 'react-native-typography';

import ThemeSvg from './ThemeSvg';
import ThemeView from './ThemeView';
import ThemeText from './ThemeText';
import {lockSvg, hexagonSvg, cautionSvg, scaledWedgeSvg} from '../svg';
import {ActionCreators} from '../actions';
const basicParseURL = require('url-parse');
import NavigationService from '../lib/NavigationService';
import {getProtocolSchema, parseSchemaIcon, getInfoKey} from '../lib/common';

const NavigationHostButton = ({
  tabIndex,
  tabHistory,
  deviceDimensions,
  protocols,
  height,
}: props) => {
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const currentPage = history.get(currentIndex);
  const fullUrl = currentPage.inputUrl || currentPage.uri;
  const {protocol} = basicParseURL(fullUrl, true);
  let scopeInfo = currentPage.scopeInfo || [];
  const hostUrl = getInfoKey(fullUrl);
  // TODO M? - If dat/hyper, check for title. If title, use that.
  const showProtocolInfo = async () => {
    NavigationService.navigate({
      name: 'ProtocolInfo',
      params: {
        tabIndex,
      },
    });
  };
  let hostButtonThemeType = 'textColor';
  let icon;
  let iconHeight = '16';
  let iconWidth = '16';
  if (protocol === 'http:') {
    icon = cautionSvg;
    iconHeight = '14';
    iconWidth = '15.5';
  } else if (protocol === 'https:') {
    icon = lockSvg;
    iconHeight = '14';
    iconWidth = '12';
  } else {
    hostButtonThemeType = 'highlightColor';
    const schema = getProtocolSchema(hostUrl, protocols);
    const {asset, ratio} = parseSchemaIcon(schema.icon);
    icon = asset;
    if (ratio) {
      iconWidth = `${ratio * Number(iconHeight)}`;
    }
    if (!icon) {
      icon = hexagonSvg;
      iconHeight = '16';
      iconWidth = '14.5';
    }
  }
  const roundingStyle = {
    height,
    borderRadius: height / 2,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  };
  const {wedgeSvg, height: wedgeHeight, width: wedgeWidth} = scaledWedgeSvg(
    height,
  );
  const screenWidth = deviceDimensions.get('width');
  const maxHostnameWidth =
    protocol.indexOf('http') === 0
      ? Platform.OS === 'android'
        ? screenWidth / 2
        : (screenWidth * 3) / 4
      : Platform.OS === 'android'
      ? screenWidth / 4
      : screenWidth / 3;
  return (
    <TouchableOpacity
      style={[styles.touchable, {height}]}
      onPress={showProtocolInfo}
      onLongPress={showProtocolInfo}
      activeOpacity={0.6}>
      <ThemeView
        style={[styles.container, roundingStyle]}
        type={hostButtonThemeType}
        opacity={0.2}>
        <ThemeSvg
          width={iconWidth}
          height={iconHeight}
          svgXmlData={icon}
          style={styles.iconStyle}
          type={hostButtonThemeType}
        />
        <ThemeText
          style={[styles.text, {maxWidth: maxHostnameWidth}]}
          type={hostButtonThemeType}
          numberOfLines={1}
          ellipsizeMode="middle">
          {hostUrl}
        </ThemeText>
        {scopeInfo.map((element, index) => {
          let text = element;
          if (typeof element === 'object') {
            text = element.value;
            if (element.alias) {
              if (typeof element.alias === 'string') {
                text = element.alias.replace('{__VALUE__}', `${element.value}`);
                if (text.search(/\(s\)/i) !== -1) {
                  if (element.value === 1) {
                    text = text.replace(/\(s\)/gi, '');
                  } else {
                    text = text.replace(/\(s\)/g, 's').replace(/\(S\)/g, 'S');
                  }
                }
              } else if (
                element.alias[element.value] !== undefined &&
                element.alias[element.value] !== null
              ) {
                text =
                  element.alias[element.value].label ||
                  element.alias[element.value];
              }
            }
          }
          if (text === null || text === undefined) {
            return null;
          }
          return (
            <ThemeView
              key={text + index}
              style={styles.subContainer}
              type={hostButtonThemeType}
              opacity={0.9}>
              <ThemeText type="backgroundColor">{text}</ThemeText>
            </ThemeView>
          );
        })}
      </ThemeView>
      <ThemeSvg
        opacity={0.2}
        width={wedgeWidth}
        height={wedgeHeight}
        svgXmlData={wedgeSvg}
        type={hostButtonThemeType}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingRight: 3,
  },
  subContainer: {
    marginLeft: 5,
    borderRadius: 5,
    paddingHorizontal: 5,
  },
  touchable: {
    flexDirection: 'row',
    overflow: 'hidden',
    marginLeft: 5,
  },
  iconStyle: {
    marginRight: 8,
  },
  text: {
    ...systemWeights.regular,
    fontSize: 18,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    protocols: state.protocols,
    tabHistory: state.tabHistory,
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavigationHostButton);
