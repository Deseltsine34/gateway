/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThemeSvg from './ThemeSvg';
import {threeDotsSvg} from '../svg';
import {ActionCreators} from '../actions';
import NavigationService from '../lib/NavigationService';

class OptionsButton extends Component {
  constructor() {
    super();
    this.button = null;
  }
  options = () => {
    const {
      props: {tabIndex},
    } = this;
    NavigationService.navigate({
      name: 'OptionsPopup',
      params: {
        tabIndex,
      },
    });
  };
  onLayout = () => {
    const {
      props: {setOptButtonPosition},
    } = this;
    this.button.measure((x, y, width, height, pageX, pageY) => {
      setOptButtonPosition(pageX, pageY);
    });
  };
  render() {
    const {options, onLayout} = this;
    return (
      <TouchableOpacity
        ref={ref => (this.button = ref)}
        style={styles.touchable}
        onPress={options}
        onLongPress={options}
        onLayout={onLayout}>
        <ThemeSvg width="20" height="5" svgXmlData={threeDotsSvg} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  touchable: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OptionsButton);
