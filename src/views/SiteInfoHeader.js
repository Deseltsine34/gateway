/**
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {systemWeights} from 'react-native-typography';

import {getInfoKey} from '../lib/common';
import {ActionCreators} from '../actions';
import ThemeView from './ThemeView';
import ThemeText from './ThemeText';

const SiteInfoHeader = ({onHeight, tabIndex, tabHistory, siteInfo}: props) => {
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const source = history.get(currentIndex);
  const url = source.inputUrl || source.uri;
  const infoKey = getInfoKey(url);
  let info =
    infoKey && siteInfo.get(infoKey) ? siteInfo.get(infoKey).info : undefined;
  if (!info) {
    info = {};
  }
  console.log('[BROWSER] SiteInfoHeader info:', info);
  console.log('[BROWSER] SiteInfoHeader scopeInfo:', scopeInfo);
  let displayInfo = info.display || [];
  const scopeInfo = source.scopeInfo || [];
  displayInfo = displayInfo.filter(el => {
    const elKey = typeof el === 'object' ? el.key : el;
    const testArray = scopeInfo.filter(testEl => {
      const testElKey = typeof testEl === 'object' ? testEl.key : testEl;
      if (testElKey === elKey) {
        return true;
      }
      return false;
    });
    if (testArray.length > 0) {
      return false;
    }
    return true;
  });
  const onLayout = ({
    nativeEvent: {
      layout: {height},
    },
  }) => {
    if (onHeight) {
      onHeight(height);
    }
  };
  const title = info.title || url;
  const description = info.description;
  // TODO M - Icon support for display and scope info
  return (
    <View style={[styles.container, styles.transparent]} onLayout={onLayout}>
      <ThemeText style={styles.title}>{title}</ThemeText>
      {description ? (
        <ThemeText style={styles.description}>{description}</ThemeText>
      ) : null}
      <View style={[styles.rowContainer, styles.transparent]}>
        {displayInfo.map((element, index) => {
          let value =
            typeof element === 'object' && element.key
              ? element.key.indexOf('.') === -1
                ? info[element.key]
                : info
              : info[element];
          if (
            typeof element === 'object' &&
            element.key &&
            element.key.indexOf('.') !== -1
          ) {
            const splitKey = element.key.split('.');
            for (const key of splitKey) {
              if (value === undefined || value === null) {
                break;
              }
              value = value[key];
            }
          }
          let text = value;
          if (element.alias) {
            if (typeof element.alias === 'string') {
              text = element.alias.replace('{__VALUE__}', `${value}`);
              if (text.search(/\(s\)/i) !== -1) {
                if (value === 1) {
                  text = text.replace(/\(s\)/gi, '');
                } else {
                  text = text.replace(/\(s\)/g, 's').replace(/\(S\)/g, 'S');
                }
              }
            } else if (
              element.alias[value] !== undefined &&
              element.alias[value] !== null
            ) {
              text = element.alias[value].label || element.alias[value];
            }
          }
          if (text === null || text === undefined) {
            return null;
          }
          if (typeof text !== 'string') {
            text = `${text}`;
          }
          return (
            <ThemeView
              key={text + index}
              type="textColor"
              opacity={0.1}
              style={styles.bubble}>
              <ThemeText
                allowFontScaling={false}
                numberOfLines={3}
                style={styles.bubbleText}>
                {text}
              </ThemeText>
            </ThemeView>
          );
        })}
        {scopeInfo.map((element, index) => {
          let text = element;
          if (typeof element === 'object') {
            text = element.value;
            if (element.alias) {
              if (typeof element.alias === 'string') {
                text = element.alias.replace('{__VALUE__}', `${element.value}`);
                if (text.search(/\(s\)/i) !== -1) {
                  if (element.value === 1) {
                    text = text.replace(/\(s\)/gi, '');
                  } else {
                    text = text.replace(/\(s\)/g, 's').replace(/\(S\)/g, 'S');
                  }
                }
              } else if (
                element.alias[element.value] !== undefined &&
                element.alias[element.value] !== null
              ) {
                text =
                  element.alias[element.value].label ||
                  element.alias[element.value];
              }
            }
          }
          if (text === null || text === undefined) {
            return null;
          }
          return (
            <ThemeView
              key={text + index}
              type="textColor"
              opacity={0.1}
              style={styles.bubble}>
              <ThemeText
                allowFontScaling={false}
                numberOfLines={3}
                style={styles.bubbleText}>
                {text}
              </ThemeText>
            </ThemeView>
          );
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  transparent: {
    backgroundColor: 'transparent',
  },
  rowContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
  },
  title: {
    ...systemWeights.bold,
    fontSize: 21,
  },
  description: {
    ...systemWeights.semibold,
    marginTop: 5,
    fontSize: 18,
  },
  bubble: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 7.5,
    marginRight: 5,
    marginTop: 5,
    padding: 5,
  },
  bubbleText: {
    ...systemWeights.light,
    fontSize: 14,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    tabHistory: state.tabHistory,
    siteInfo: state.siteInfo,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SiteInfoHeader);
