import {makeHtml2Canvas} from './modules/html2canvas.js';
import {makeColorThief} from './modules/color-thief.js';
import {Vibrant} from './modules/vibrant.js';

// TODO H - BUG: Hangs after "Html Image Complete! Finishing..." message. Likely some error with 'both' scenario's colorThief or Vibrant (log to find out)

// TODO M - BUG: CloseKeyboardButton keeps default color scheme despite chameleon mode?
// TODO M - BUG: Seemingly increasing amount of theme injection calls
// TODO M - Also get html background color after text, in a manner similar to native-only?

export const makeThemeInjection = function(nativeScreenshotUri) {
  return `${Vibrant}

  function isValidHighlight(rgb, backgroundRgb) {
    const r = rgb[0];
    const g = rgb[1];
    const b = rgb[2];
    if (Math.abs(r - g) <= 10 && Math.abs(r - b) <= 10 && Math.abs(g - b) <= 10) {
      return false;
    }
    if (!backgroundRgb) {
      return true;
    }
    const bgDelta = Vibrant.Util.rgbDiff(
      rgb,
      backgroundRgb,
    );
    if (bgDelta <= 20) {
      return false;
    }
    return true;
  }

  function rgbTextToVector(string) {
    const numString = string.replace(/rgba?\\(/, '').replace(/\\)/, '').replace(/[\\n \\s]*/g, '');
    const split = numString.split(',');
    return [Number(split[0]), Number(split[1]), Number(split[2])];
  }

  function getMeta(metaName) {
    const metas = document.getElementsByTagName('meta');
    for (let i = 0; i < metas.length; i++) {
      if (metas[i].getAttribute('name') === metaName) {
        return metas[i].getAttribute('content');
      }
    }
    return undefined;
  }

  (function() {
    ${makeHtml2Canvas}

    ${makeColorThief}

    async function finish({both, htmlImage, htmlImageData}) {
      var bgColor;

      var themeColor = getMeta('theme-color');
      if (themeColor && typeof themeColor === 'string') {
        if (themeColor.startsWith('#')) {
          bgColor = Vibrant.Util.hexToRgb(themeColor);
        } else if (themeColor.startsWith('rgb')) {
          bgColor = rgbTextToVector(themeColor);
        }
      }

      var htmlPalette;
      if (both && htmlImage && htmlImageData) {
        if (!bgColor) {
          bgColor = colorThief.getColor(htmlImage);
        }
        var htmlVibrant = Vibrant.from(htmlImageData);
        htmlPalette = await htmlVibrant.getPalette();
      }
      var nativeVibrant = Vibrant.from(\`${nativeScreenshotUri}\`);
      nativeVibrant.getPalette(function(err, nativePalette) {
        if (err !== undefined && err !== null) {
          window.ReactNativeWebView.postMessage("Native Vibrant Error: " + err.message);
        } else {
          let textRgb;
          let textColor;
          let classNames = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span'];
          let attemptedColors = {};
          for (var i = 0; i < classNames.length; i++) {
            var name = classNames[i];
            var classElements = document.getElementsByTagName(name);
            var largestDelta = 0;
            for (var j = 0; j < classElements.length; j++) {
              let el = classElements[j];
              let elStyle = window.getComputedStyle(el);
              if (elStyle && elStyle.getPropertyValue('color') && attemptedColors[elStyle.getPropertyValue('color')] !== true) {
                let styleColor = elStyle.getPropertyValue('color');
                let rgb = styleColor.indexOf('rgb') === -1 ?
                  Vibrant.Util.hexToRgb(styleColor)
                  : rgbTextToVector(styleColor);
                if (bgColor) {
                  const delta = Vibrant.Util.rgbDiff(
                    rgb,
                    bgColor,
                  );
                  if (delta >= 40 && delta > largestDelta) {
                    textRgb = rgb;
                    textColor = 'rgba(' + Math.round(rgb[0]) + ',' + Math.round(rgb[1]) + ',' + Math.round(rgb[2]) + ',1.0)';
                  } else {
                    attemptedColors[styleColor] = true;
                  }
                } else if (!textColor) {
                  textRgb = rgb;
                  textColor = 'rgba(' + Math.round(rgb[0]) + ',' + Math.round(rgb[1]) + ',' + Math.round(rgb[2]) + ',1.0)';
                } else {
                  const r = rgb[0];
                  const g = rgb[1];
                  const b = rgb[2];
                  if (Math.abs(r - g) <= 10 && Math.abs(r - b) <= 10 && Math.abs(g - b) <= 10) {
                    textRgb = rgb;
                    textColor = 'rgba(' + Math.round(rgb[0]) + ',' + Math.round(rgb[1]) + ',' + Math.round(rgb[2]) + ',1.0)';
                    break;
                  }
                }
              }
            }
            if (textColor) {
              break;
            }
          }

          if (!both) {
            var setBgColor = !bgColor ? true : false;
            var highestPopulation = 0;
            var highestDelta = 0;
            for (var key in nativePalette) {
              const swatch = nativePalette[key];
              if (swatch !== undefined && swatch !== null) {
                if (textRgb) {
                  const swatchRgb = swatch.getRgb();
                  const textDelta = Vibrant.Util.rgbDiff(
                    swatchRgb,
                    textRgb,
                  );
                  if (textDelta > highestDelta) {
                    highestDelta = textDelta;
                    if (setBgColor) {
                      bgColor = swatchRgb;
                    }
                  }
                } else {
                  const population = swatch.getPopulation();
                  if (population > highestPopulation) {
                    highestPopulation = population;
                    if (setBgColor) {
                      bgColor = swatch.getRgb();
                    }
                  }
                }
              }
            }
            htmlPalette = nativePalette;
          }
          const _yiq = (bgColor[0] * 299 + bgColor[1] * 587 + bgColor[2] * 114) / 1000;

          if (textColor === undefined || textColor === null) {
            textColor = _yiq < 150 ? 'rgba(255,255,255,1.0)' : 'rgba(0,0,0,1.0)';
          }

          let highlightColor = _yiq < 150 ? 'rgba(255,255,255,1.0)' : 'rgba(0,0,0,1.0)';
          const htmlHlt = htmlPalette.Vibrant.getRgb();
          const nativeHlt = nativePalette.Vibrant.getRgb();
          const vibrantDelta = Vibrant.Util.rgbDiff(
            htmlHlt,
            nativeHlt,
          );
          if (vibrantDelta > 10) {
            // Different
            if (isValidHighlight(htmlHlt, bgColor)) {
              highlightColor = 'rgba(' + Math.round(htmlHlt[0]) + ',' + Math.round(htmlHlt[1]) + ',' + Math.round(htmlHlt[2]) + ',1.0)';
            } else if (isValidHighlight(nativeHlt, bgColor)) {
              highlightColor = 'rgba(' + Math.round(nativeHlt[0]) + ',' + Math.round(nativeHlt[1]) + ',' + Math.round(nativeHlt[2]) + ',1.0)';
            }
          } else if (isValidHighlight(htmlHlt, bgColor)) {
            // Similar and HTML passes
            highlightColor = 'rgba(' + Math.round(htmlHlt[0]) + ',' + Math.round(htmlHlt[1]) + ',' + Math.round(htmlHlt[2]) + ',1.0)';
          } else {
            var darkHlt = [0, 0, 0];
            var lightHlt = [0, 0, 0];
            const htmlDarkVibe = htmlPalette.DarkVibrant ? htmlPalette.DarkVibrant.getRgb() : [0, 0, 0];
            const htmlLightVibe = htmlPalette.LightVibrant ? htmlPalette.LightVibrant.getRgb() : [0, 0, 0];
            const nativeDarkVibe = nativePalette.DarkVibrant ? nativePalette.DarkVibrant.getRgb() : [0, 0, 0];
            const nativeLightVibe = nativePalette.LightVibrant ? nativePalette.LightVibrant.getRgb() : [0, 0, 0];
            const darkDelta = Vibrant.Util.rgbDiff(
              htmlDarkVibe,
              nativeDarkVibe,
            );
            if (darkDelta > 10) {
              if (isValidHighlight(nativeDarkVibe, bgColor)) {
                darkHlt = nativeDarkVibe;
              } else if (isValidHighlight(htmlDarkVibe, bgColor)) {
                darkHlt = htmlDarkVibe;
              }
            } else if (isValidHighlight(htmlDarkVibe, bgColor)) {
              darkHlt = htmlDarkVibe;
            }
            const lightDelta = Vibrant.Util.rgbDiff(
              htmlLightVibe,
              nativeLightVibe,
            );
            if (lightDelta > 10) {
              if (isValidHighlight(nativeLightVibe, bgColor)) {
                lightHlt = nativeLightVibe;
              } else if (isValidHighlight(htmlLightVibe, bgColor)) {
                lightHlt = htmlLightVibe;
              }
            } else if (isValidHighlight(htmlLightVibe, bgColor)) {
              lightHlt = htmlLightVibe;
            }
            const darkBgDelta = Vibrant.Util.rgbDiff(darkHlt, bgColor);
            const lightBgDelta = Vibrant.Util.rgbDiff(lightHlt, bgColor);
            if (darkBgDelta >= lightBgDelta && isValidHighlight(darkHlt, bgColor)) {
              highlightColor = 'rgba(' + Math.round(darkHlt[0]) + ',' + Math.round(darkHlt[1]) + ',' + Math.round(darkHlt[2]) + ',1.0)';
            } else if (isValidHighlight(lightHlt, bgColor)) {
              highlightColor = 'rgba(' + Math.round(lightHlt[0]) + ',' + Math.round(lightHlt[1]) + ',' + Math.round(lightHlt[2]) + ',1.0)';
            }
          }

          const themeResult = {
            theme: {
              backgroundColor: 'rgba(' + Math.round(bgColor[0]) + ',' + Math.round(bgColor[1]) + ',' + Math.round(bgColor[2]) + ',1.0)',
              highlightColor,
              textColor,
            },
          };
          window.ReactNativeWebView.postMessage(JSON.stringify(themeResult));

        }
      });
    }

    const html2canvas = makeHtml2Canvas();
    var ColorThief = makeColorThief();
    var colorThief = new ColorThief();
    html2canvas(document.body).then(canvas => {
      var htmlImage = new Image();
      var htmlImageData = canvas.toDataURL();
      htmlImage.src = htmlImageData;

      if (htmlImage.complete) {
        finish({both: true, htmlImage, htmlImageData});
      } else {
        var fallback = setTimeout(() => {
          finish({both: false, htmlImage, htmlImageData});
        }, 500);
        htmlImage.addEventListener('load', function() {
          clearInterval(fallback);
          finish({both: true, htmlImage, htmlImageData});
        });
      }

    }).catch(e => {
      window.ReactNativeWebView.postMessage("Html2canvas Error: " + e.message);
      finish({both: false});
    });
  })()
  true;`;
};
