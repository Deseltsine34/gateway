export const webViewInjectionRoot = `if (
  !window.bridgeCallbacks ||
  typeof window.bridgeCallbacks !== 'object'
) {
  window.bridgeCallbacks = {};
}
if (
  !window.iteratingBridgeCallbacks ||
  typeof window.iteratingBridgeCallbacks !== 'object'
) {
  window.iteratingBridgeCallbacks = {};
}
if (
  !window.bridgeEmitters ||
  typeof window.bridgeEmitters !== 'object'
) {
  window.bridgeEmitters = {};
}
window.modifiedUrls = [];

window.baseFsUri = '__BASE_FS_URI__';
window.fakeLocation = __FAKE_LOCATION__;

if (!window.cacheUri) {
  Object.defineProperty(window, 'cacheUri', {
    value: '__CACHE_URI__',
    writable: false,
    enumerable: false,
    configurable: false,
  });
}

function decodeArrayBuffer(input) {
  const _keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  function removePaddingChars(input) {
    var lkey = _keyStr.indexOf(input.charAt(input.length - 1));
    if (lkey == 64) {
      return input.substring(0, input.length - 1);
    }
    return input;
  }
  function decode(input, arrayBuffer) {
    //get last chars to see if are valid
    input = removePaddingChars(input);
    input = removePaddingChars(input);

    var bytes = parseInt((input.length / 4) * 3, 10);

    var uarray;
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
    var j = 0;

    if (arrayBuffer) {
      uarray = new Uint8Array(arrayBuffer);
    } else {
      uarray = new Uint8Array(bytes);
    }

    input = input.replace(/[^A-Za-z0-9\\+\\/\\=]/g, '');

    for (var i = 0; i < bytes; i += 3) {
      //get the 3 octects in 4 ascii chars
      enc1 = _keyStr.indexOf(input.charAt(j++));
      enc2 = _keyStr.indexOf(input.charAt(j++));
      enc3 = _keyStr.indexOf(input.charAt(j++));
      enc4 = _keyStr.indexOf(input.charAt(j++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      uarray[i] = chr1;
      if (enc3 != 64) {
        uarray[i + 1] = chr2;
      }
      if (enc4 != 64) {
        uarray[i + 2] = chr3;
      }
    }

    return uarray;
  }

  var bytes = (input.length / 4) * 3;
  var ab = new ArrayBuffer(bytes);
  decode(input, ab);
  return ab;
}

const makeDateProxy = (string) => {
  return new Date(Number(string));
};
const makeBufferProxy = (string) => {
  return decodeArrayBuffer(string);
};
const makeClassProxy = (string, handler) => {
  const proxyClass = class {
    constructor(...args) {
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'class_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
        bridgeId: string,
        requestId,
        action: {
          type: 'construct',
          args: [...args],
        },
      });
      waitForBridgeResponse(requestId, () => {
        this._bridgeId = requestId;
      });
    }
  }
  return new Proxy(proxyClass, handler);
};
// NOTE - As of now, there is no difference between makeFuncProxy, makePromiseProxy, makeAsyncFuncProxy. They both return promises
const makeFuncProxy = (string, handler) => {
  return new Proxy(function(...args) {
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId = 'func_' + new Date().getTime() + '_' + randomInt;
    sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
      bridgeId: string,
      requestId,
      action: {
        type: 'apply',
        args: [...args],
      },
    });
    return waitForBridgeResponse(requestId);
  }, handler);
};
const makeAsyncFuncProxy = (string, handler) => {
  return new Proxy(async function(...args) {
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId = 'asyncFunc_' + new Date().getTime() + '_' + randomInt;
    sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
      bridgeId: string,
      requestId,
      action: {
        type: 'apply',
        args: [...args],
      },
    });
    // return new Promise((resolve, reject) => {
    //   waitForBridgeResponse(requestId, res => {
    //     resolve(res)
    //   });
    // });
    return await waitForBridgeResponse(requestId);
  }, handler);
};
const makePromiseProxy = (string, handler) => {
  const promise = new Promise((resolve, reject) => {
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId = 'promise_' + new Date().getTime() + '_' + randomInt;
    sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
      bridgeId: string,
      requestId,
      action: {
        type: 'promise',
        args: [...args],
      },
    });
    waitForBridgeResponse(requestId, (response, type) => {
      if (type === 'rejection') {
        reject(response);
      } else {
        resolve(response)
      }
    });
  });
  return new Proxy(promise, handler);
};
const makeObjectProxy = (string, handler) => {
  return new Proxy({_bridgeId: string}, handler);
};
const makeEmitterProxy = (string, handler) => {
  class EventDispatcher {
    constructor() {
      this._listeners = [];
    }
    hasEventListener(type, listener) {
      return this._listeners.some(
        item => item.type === type && item.listener === listener,
      );
    }
    addListener(type, listener, options = {once: false}) {
      // Bridge
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'addListener_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
        bridgeId: string,
        requestId,
        action: {
          type: 'addListener',
          eventName: type,
        },
      });
      if (!bridgeEmitters[string]) {
        bridgeEmitters[string] = this;
      }
      // WebView
      if (!this.hasEventListener(type, listener)) {
        this._listeners.push({type, listener, options});
      }
      window.ReactNativeWebView.postMessage('Added event listener for: ' + type);
      return this;
    }
    addEventListener(type, listener, options = {once: false}) {
      // Bridge
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'addEventListener_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
        bridgeId: string,
        requestId,
        action: {
          type: 'addEventListener',
          eventName: type,
        },
      });
      if (!bridgeEmitters[string]) {
        bridgeEmitters[string] = this;
      }
      // WebView
      if (!this.hasEventListener(type, listener)) {
        this._listeners.push({type, listener, options});
      }
      window.ReactNativeWebView.postMessage('Added event listener for: ' + type);
      return this;
    }
    removeListener(type, listener) {
      // Bridge
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'removeListener_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
        bridgeId: string,
        requestId,
        action: {
          type: 'removeListener',
          eventName: type,
        },
      });
      if (!bridgeEmitters[string]) {
        bridgeEmitters[string] = this;
      }
      // WebView
      let index = this._listeners.findIndex(item => {
        return item.type === type && item.listener === listener;
      });
      if (index >= 0) {
        this._listeners.splice(index, 1);
      }
      return this;
    }
    removeEventListener(type, listener) {
      // Bridge
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'removeEventListener_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
        bridgeId: string,
        requestId,
        action: {
          type: 'removeEventListener',
          eventName: type,
        },
      });
      if (!bridgeEmitters[string]) {
        bridgeEmitters[string] = this;
      }
      // WebView
      let index = this._listeners.findIndex(item => {
        return item.type === type && item.listener === listener;
      });
      if (index >= 0) {
        this._listeners.splice(index, 1);
      }
      return this;
    }
    removeEventListeners() {
      this._listeners = [];
      return this;
    }
    dispatchEvent(eventName, ...args) {
      this._listeners
        .filter(item => item.type === eventName)
        .forEach(item => {
          const {
            type,
            listener,
            options: {once},
          } = item;
          listener.call(this, ...args);
          if (once === true) {
            this.removeEventListener(type, listener);
          }
        });
      return this;
    }
  }
  const target = new EventDispatcher();
  return new Proxy(target, handler);
};
// TODO H - Figure out how to bridge generators with React Native limitations
const makeGenProxy = (string, handler) => {
  return string;
};
const makeAsyncGenProxy = (string, handler) => {
  return string;
};

const handleProxyAction = (bridgeId, action) => {
  const randomInt = Math.floor(Math.random() * Math.floor(100000));
  const requestId = action.type + '_' + new Date().getTime() + '_' + randomInt;
  sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
    bridgeId,
    requestId,
    action,
  });
  return waitForBridgeResponse(requestId);
}

const emitterProxyGet = (bridgeId, target, key, receiver) => {
  if (
    key === 'dispatchEvent' ||
    key === 'hasEventListener' ||
    key === '_listeners' ||
    key === 'addListener' ||
    key === 'addEventListener' ||
    key === 'removeListener' ||
    key === 'removeEventListener'
  ) {
    return Reflect.get(target, key, receiver);
  }
  if (key === 'emit') {
    // TODO H - Send emit request to bridge
  }
  return handleProxyAction(bridgeId, {
    type: 'get',
    key,
  });
};
// TODO H - TRY: If you get a proxied object whose property needs to be retrieved,
//               keep getting a 'dummy' promise until the true value is available.
//               By knowing what the requested prop was, you can keep adding to the "chain queue" until the sequence is satisfied
const getProxyHandler = (string) => {
  const isEmitter = string.indexOf('[EMITTER]') === 0;
  const isPromise = string.indexOf('[PROMISE]') === 0;
  const bridgeId = string.substr(string.indexOf(']') + 1);
  return {
    get: function(target, key, receiver) {
      if (key === '_bridgeId' || (key === 'then' && !isPromise)) {
        return Reflect.get(...arguments);
      }
      if (isEmitter) {
        return emitterProxyGet(bridgeId, target, key, receiver);
      }
      return handleProxyAction(bridgeId, {
        type: 'get',
        key,
      });
    },
    defineProperty: function(target, key, descriptor) {
      if (key === '_bridgeId') {
        return Reflect.defineProperty(...arguments);
      }
      return handleProxyAction(bridgeId, {
        type: 'defineProperty',
        key,
        descriptor,
      });
    },
    deleteProperty: function(target, key) {
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'delete_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__NODE_REQUEST_LISTENER_ID__', requestId, {
        bridgeId,
        requestId,
        action: {
          type: 'deleteProperty',
          key,
        },
      });
    },
    getOwnPropertyDescriptor: function(target, key) {
      return handleProxyAction(bridgeId, {
        type: 'getOwnPropertyDescriptor',
        key,
      });
    },
    getPrototypeOf: function(target) {
      return handleProxyAction(bridgeId, {
        type: 'getPrototypeOf',
      });
    },
    has: function(target, key) {
      return handleProxyAction(bridgeId, {
        type: 'has',
        key,
      });
    },
    isExtensible: function(target) {
      return handleProxyAction(bridgeId, {
        type: 'isExtensible',
      });
    },
    preventExtensions: function(target) {
      return handleProxyAction(bridgeId, {
        type: 'preventExtensions',
      });
    },
    ownKeys: function(target) {
      return handleProxyAction(bridgeId, {
        type: 'ownKeys',
      });
    },
    setPrototypeOf: function(target, prototype) {
      return handleProxyAction(bridgeId, {
        type: 'setPrototypeOf',
        prototype,
      });
    },
    set: function(target, key, value) {
      if (key === '_bridgeId') {
        return Reflect.set(...arguments);
      }
      return handleProxyAction(bridgeId, {
        type: 'set',
        key,
        value,
      });
    },
  };
}

window.unBridgify = (result) => {
  if (typeof result === 'string') {
    const proxyHandler = getProxyHandler(result);
    if (result.indexOf('[CLASS]') === 0) {
      return makeClassProxy(result.replace('[CLASS]', ''), proxyHandler);
    }
    if (result.indexOf('[FUNCTION]') === 0) {
      return makeFuncProxy(result.replace('[FUNCTION]', ''), proxyHandler);
    }
    if (result.indexOf('[ASYNC_FUNCTION]') === 0) {
      return makeAsyncFuncProxy(result.replace('[ASYNC_FUNCTION]', ''), proxyHandler);
    }
    if (result.indexOf('[GENERATOR]') === 0) {
      return makeGenProxy(result.replace('[GENERATOR]', ''), proxyHandler);
    }
    if (result.indexOf('[ASYNC_GENERATOR]') === 0) {
      return makeAsyncGenProxy(result.replace('[ASYNC_GENERATOR]', ''), proxyHandler);
    }
    if (result.indexOf('[PROMISE]') === 0) {
      return makePromiseProxy(result.replace('[PROMISE]', ''), proxyHandler);
    }
    if (result.indexOf('[EMITTER]') === 0) {
      return makeEmitterProxy(result.replace('[EMITTER]', ''), proxyHandler);
    }
    if (result.indexOf('[BUFFER]') === 0) {
      return makeBufferProxy(result.replace('[BUFFER]', ''));
    }
    if (result.indexOf('[DATE]') === 0) {
      return makeDateProxy(result.replace('[DATE]', ''));
    }
    if (result.indexOf('[OBJECT]') === 0) {
      return makeObjectProxy(result.replace('[OBJECT]', ''), proxyHandler);
    }
  }
  if (Array.isArray(result)) {
    let arrayResult = result.map(item => {
      return unBridgify(item);
    });
    return arrayResult;
  }
  if (typeof result === 'object') {
    let objResult = {};
    for (const key in result) {
      objResult[key] = unBridgify(result[key]);
    }
    return objResult;
  }
  return result;
};

// NOTE - Pass array for all generic node requests so that they can be spread into arguments for corresponding functions
function sendBridgeRequest(channelId, requestId, args) {
  if (args === undefined || args === null) {
    args = requestId;
    requestId = channelId;
  }
  window.ReactNativeWebView.postMessage(JSON.stringify({
    channelId,
    requestId,
    args,
  }));
};
// NOTE - Always returns a promise
function waitForBridgeResponse(requestId, cb) {
  return new Promise((resolve, reject) => {
    var lastResponse = -1;
    (function waitForResults(){
      const requestResults = window.bridgeCallbacks[requestId];
      let iteratingRequestResults = window.iteratingBridgeCallbacks[requestId];
      if (requestResults === '__DO_NOT_RETURN__') {
        return resolve();
      }
      if (requestResults) {
        // TODO H - Errors
        const unBridgedResults = unBridgify(requestResults);
        if (cb) {
          cb(unBridgedResults);
        }
        return resolve(unBridgedResults);
      }
      if (iteratingRequestResults && iteratingRequestResults.length > lastResponse) {
        iteratingRequestResults = iteratingRequestResults.sort((a, b) => {
          if (b.end !== undefined && b.end !== null) {
            return -1;
          }
          if (a.end !== undefined && a.end !== null) {
            return 1;
          }
          return a.iteration - b.iteration;
        });
        for (var i = 0; i < iteratingRequestResults.length; i++) {
          let result = iteratingRequestResults[i];
          if (
            (result.iteration !== undefined &&
              result.iteration !== null &&
              lastResponse === result.iteration - 1) ||
            unBridgedResult.end === lastResponse
          ) {
            if (result.iteration !== undefined && result.iteration !== null) {
              lastResponse = result.iteration;
            }
            let unBridgedResult = unBridgify(result);
            if (cb) {
              cb(unBridgedResult);
            }
            if (unBridgedResult.end === lastResponse) {
              return resolve(iteratingRequestResults);
            }
          }
        }
      }
      setTimeout(waitForResults, 30);
    })();
  });
};
// NOTE - Alternative implementation that doesn't have a tiemout
//      - REASON IT CAN'T BE USED YET: Constant loop permanently freezes page
function waitForBridgeResponseSync(requestId, cb) {
  // This is the timeout
  let end = Date.now() + 5000;
  while (Date.now() < end) {
    if (window.bridgeCallbacks[requestId]) {
      end = Date.now() + 10000;
    }
    continue;
  }
  const requestResults = window.bridgeCallbacks[requestId];
  if (requestResults === '__DO_NOT_RETURN__') {
    return;
  }
  if (requestResults) {
    // TODO H - Errors
    const unBridgedResults = unBridgify(requestResults);
    if (cb) {
      cb(unBridgedResults);
      return;
    } else {
      return unBridgedResults;
    }
  }
};

function makeApiFunction(name, isAsync) {
  let nameArray = name.indexOf('.') === -1 ? [name] : name.split('.');
  if (isAsync) {
    return async function(...args) {
      const argsArray = [...args];
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'apiAsync_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__API_REQUEST_LISTENER_ID__', requestId, {
        requestId,
        apiName: nameArray,
        args: argsArray,
      });
      const result = await waitForBridgeResponse(requestId);
      return result;
    }
  }
  return function(...args) {
    const argsArray = [...args];
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId = 'api_sync_' + new Date().getTime() + '_' + randomInt;
    sendBridgeRequest('__API_REQUEST_LISTENER_ID__', requestId, {
      requestId,
      apiName: nameArray,
      args: argsArray,
    });
    return waitForBridgeResponse(requestId);
  }
}

// Fetch
const parseFetchMessage = (msg, resolve, reject) => {
  // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Started parseFetchMessage: ' + JSON.stringify(msg));
  if (Array.isArray(msg)) {
    // There are multiple results to listen for
    const res = msg.map((response, responseIndex) => {
      if (response.bodyId) {
        // response.bodyId is the unique ID for body listeners
        const subRequestId = response.bodyId;
        response.body = new Promise((subResolve, subReject) => {
          waitForBridgeResponse(subRequestId, subMsg => {
            parseFetchMessage(subMsg, subResolve, subReject);
          });
        });
        response.bodyId = undefined;
      }
      return response;
    });
    resolve(res);
    return;
  }
  if (msg.end !== undefined && msg.end !== null) {
    return;
  }
  if (msg.bodyId || (!msg.result && !msg.error)) {
    // This is the status result
    // Add body listener if needed, otherwise assume finished
    if (msg.bodyId) {
      // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Listening to response body: ' + JSON.stringify(msg));
      const subRequestId = msg.bodyId;
      msg.body = new Promise((subResolve, subReject) => {
        waitForBridgeResponse(subRequestId, subMsg => {
          // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Received body callback: ' + JSON.stringify(subMsg) + ' for subRequestId: ' + subRequestId);
          parseFetchMessage(subMsg, subResolve, subReject);
        });
      });
      msg.bodyId = undefined;
    }
    resolve(msg);
    return;
  }
  // This is the body result
  const {iteration, result, error, end} = msg;
  if (error) {
    window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Fetch error: ' + JSON.stringify(error));
    reject(error);
  }
  if (result) {
    // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Node Result Returned: ' + JSON.stringify(result));
    // TODO M - Async Generator support (Capable here!)
    resolve(result);
  }
};
const fetchHandler = {
  apply(target, thisArg, argumentsList) {
    // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Fetching... ' + JSON.stringify(argumentsList));
    let isSingleRequest = false;
    if (!Array.isArray(argumentsList[0])) {
      isSingleRequest = true;
      const url = argumentsList[0].url || argumentsList[0];
      if (url && url.search(/https?:\\/\\//i) !== -1) {
        // Return normal fetch, save precious resources
        return Reflect.apply(target, thisArg, argumentsList);
      }
      if (url && url.search(/file:\\/\\//i) !== -1) {
        // TODO M - Return fetch query/write on filesystem [GET = read (can set Content-Type to 'application/stat' or something, or have separate STAT method), PUT = write]
      }
    }
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId = 'fetch_' + new Date().getTime() + '_' + randomInt;
    // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Sending fetch to RN with id: ' + requestId + ' with arguments: ' + JSON.stringify(argumentsList));
    sendBridgeRequest('__FETCH_LISTENER_ID__', requestId, {
      requestId,
      argumentsList,
    });
    return new Promise((resolve, reject) => {
      waitForBridgeResponse(requestId, msg => {
        // window.ReactNativeWebView.postMessage('[WEBVIEW-LOG] Received message callback: ' + JSON.stringify(msg) + ' for requestId: ' + requestId);
        parseFetchMessage(msg, resolve, reject);
      });
    });
  },
};
window.basicFetch = window.fetch;
window.fetch = null;
window.fetch = new Proxy(window.basicFetch, fetchHandler);
// The query and write functions provide simpler, possibly more familiar, syntax
// for common fetching cases (GET and PUT, respectively).
const optionsToHeader = options => {
  let newOptions = {};
  for (const key in options) {
    let newKey = key.replace(/([a-z])([A-Z])/g, '$1-$2');
    newKey = capitalize(newKey);
    newOptions[newKey] = options[key];
  }
  return newOptions;
};
const queryOrWrite = async (method, ...params) => {
  let parameters = [...params];
  if (parameters.length === 0) {
    return;
  }
  let fetchMethod;
  if (parameters.length === 1) {
    const param = parameters[0];
    if (typeof param === 'string') {
      // Single Request
      fetchMethod = await window.fetch(param, {method});
    }
    if (typeof param === 'object') {
      // Single Request
      const newOptions = optionsToHeader(param.options);
      fetchMethod = await window.fetch(param.url, {...newOptions, method});
    } else {
      // Multiple Requests
      let newParam = param.map(p => {
        if (typeof p === 'object') {
          return optionsToHeader(p.options);
        } else {
          return optionsToHeader(p[1]);
        }
      });
      fetchMethod = await window.fetch(newParam);
    }
  } else {
    // Single Request
    const newOptions = optionsToHeader(parameters[1]);
    fetchMethod = await window.fetch(parameters[0], {...newOptions, method});
  }
  if (Array.isArray(fetchMethod)) {
    const fetchBodies = fetchMethod.map(res => res.body);
    return fetchBodies;
  }
  return fetchMethod.body;
};
window.query = (...params) => queryOrWrite('GET', ...params);
window.write = (...params) => queryOrWrite('PUT', ...params);

function getInfoKey(url) {
  const matches = url
    .replace('www.', '')
    .match(/[A-Za-z0-9-.]+:\\/\\/[A-Za-z0-9-.]+/i);
  if (matches === undefined || matches === null || matches.length === 0) {
    return undefined;
  }
  return matches[0];
}

// window.open [MIGHT DELETE]
// const windowOpenHandler = {
//   apply(target, thisArg, argumentsList) {
//     window.ReactNativeWebView.postMessage("OPENING:", JSON.stringify(argumentsList));
//     let url = argumentsList[0];
//     if (url.search(/file:\\/\\/|https?:\\/\\//i) === 0) {
//       return Reflect.apply(target, thisArg, argumentsList);
//     }
//
//     // HACK - MD Files
//     if (url.endsWith('-md.html')) {
//       url = url.replace('-md.html', '.md');
//     }
//
//     let requestOpts = {
//       method: 'LOAD',
//       headers: {
//         'Site-Info': getInfoKey(url),
//         Context: 'site',
//       },
//     };
//     let fetchResponse;
//     fetch(url, requestOpts).then(res => {
//       fetchResponse = res;
//     });
//     (function waitForFetch(){
//       if (fetchResponse) {
//         return;
//       }
//       setTimeout(waitForFetch, 30);
//     })();
//     let result;
//     fetchResponse.body.then(res => {
//       result = res;
//     });
//     (function waitForBody(){
//       if (result) {
//         return;
//       }
//       setTimeout(waitForBody, 30);
//     })();
//     if (
//       result &&
//       result.source &&
//       result.source.uri
//     ) {
//       // Render .md as .html
//       if (result.source.uri.endsWith('.md')) {
//         const htmlUri = result.source.uri.replace('.md', '-md.html');
//         result.source.uri = htmlUri;
//       }
//       let newArgs = argumentsList.map((el, index) => {
//         if (index === 0) {
//           return result.source.uri;
//         }
//         return el;
//       });
//       return Reflect.apply(target, thisArg, newArgs);
//     }
//     return Reflect.apply(target, thisArg, argumentsList);
//   },
// };
// window.basicOpen = window.open;
// window.open = null;
// window.open = new Proxy(window.basicOpen, windowOpenHandler);

window.missingElements = [];

const checkInvalidPropPath = (element, key, ignoreModified) => {
  const attr = element.getAttribute(key);
  const localName = element.getAttribute('localName');
  const uniqueId = attr + key + element.tagName + element.id + localName;
  return (
    attr &&
    (window.missingElements.indexOf(localName) === -1 ||
      customElements.get(localName) !== undefined) &&
    (ignoreModified ||
      modifiedUrls.indexOf(uniqueId) === -1) &&
    !/javascript:/.test(attr) &&
    attr.indexOf('#') === -1 &&
    // Handle Custom Protocol
    ((attr.search(/file:\\/\\/|https?:\\/\\//i) === -1 &&
      attr.search(/:\\/\\//i) !== -1) ||
      // Convert relative path to absolute
      (!window.location.protocol.startsWith('http') &&
        (attr.startsWith('file://') ||
          attr.search(/:\\/\\//i) === -1) &&
        !attr.replace('file://', '').startsWith(window.cacheUri)))
  )
};

// TODO H - BUG: Script replacement doesn't seem to work on consecutive local pages
//          KNOWN: - The paths that are being ignored specifically are Scripts that were defined in previous pages (i.e. <script src="/pauls-ui-kit/buttons.js" />)
//                 - Paths to already-replaced script files that should exist are returning a 'not found error' in node.js
//                 - customElements that are already defined can't be re-defined (throws error)
const loadNewElementContent = (element, key, url, loadUrl, setUrl, requestingLocation) => {
  // HACK - MD Files
  if (loadUrl.endsWith('-md.html')) {
    loadUrl = loadUrl.replace('-md.html', '.md');
  }

  let requestOpts = {
    method: 'LOAD',
    headers: {
      'Site-Info': getInfoKey(loadUrl),
      Context: 'site',
    },
  };
  fetch(loadUrl, requestOpts).then(fetchResponse => {
    fetchResponse.body.then(result => {
      if (result && result.source) {
        var newUrl = result.source.uri;
        if (!newUrl) {
          newUrl = setUrl
        }
        if (newUrl && requestingLocation === window.location.toString()) {
          // TODO H - BUG: Race condition causes occassional duplicate(s) and/or missing element(s)
          var newEl = element.cloneNode(true);
          newEl[key] = newUrl;
          const newElName = newEl.getAttribute('localName');
          if (customElements.get(newElName) !== undefined) {
            customElements.upgrade(newEl);
            window.ReactNativeWebView.postMessage("Upgraded new element: " + newElName);
          }
          var liveElements = document.getElementsByTagName(element.tagName);
          var lastAddition = -2;
          for (var k = 0; k < liveElements.length; k++) {
            if (requestingLocation === window.location.toString()) {
              let liveEl = liveElements[k];
              let liveAttr = liveEl.getAttribute(key);
              let liveTagName = liveEl.tagName;
              if (
                liveAttr !== undefined &&
                liveAttr !== null &&
                liveAttr.replace('file://', '') === url &&
                lastAddition < k - 1
              ) {
                lastAddition = k;
                // NOTE - DEBUG POINT
                if (liveTagName === 'SCRIPT' && key === 'src') {
                  var newScript = document.createElement('script');
                  newScript.type = liveEl.type ? liveEl.type : 'text/javascript';
                  newScript.src = newUrl;
                  newScript.async = false;
                  liveEl.parentNode.replaceChild(newScript, liveEl);
                } else {
                  liveEl.parentNode.replaceChild(newEl, liveEl);
                }
                window.ReactNativeWebView.postMessage("Replaced element path: " + newUrl + " in location: " + requestingLocation);
              }
            } else {
              break;
            }
          }
        }
      }
    }).catch(e => {
      window.ReactNativeWebView.postMessage("Element Replacement FETCH BODY Error: " + JSON.stringify(e) + " for url: " + loadUrl);
    });
  }).catch(e => {
    window.ReactNativeWebView.postMessage("Element Replacement FETCH Error: " + JSON.stringify(e) + " for url: " + loadUrl);
  });
};

// Fixing any elements calling for non-http or local urls
window.fixElementURLs = (elements, requestingLocation) => {
  window.ReactNativeWebView.postMessage("TRYING TO FIX ELEMENTS!");
  for (var i = 0; i < elements.length; i++) {
    let key;
    var element = elements[i];
    if (checkInvalidPropPath(element, 'href', true)) {
      key = 'href';
    } else if (checkInvalidPropPath(element, 'src', true)) {
      key = 'src';
    } else if (checkInvalidPropPath(element, 'path', true)) {
      key = 'path';
    }
    const elName = element.getAttribute('localName');
    if (key) {
      let url = element.getAttribute(key);
      let uniqueId = url + key + element.tagName + element.id + elName;
      if (
        window.missingElements.indexOf(elName) === -1 ||
        customElements.get(elName) !== undefined
      ) {
        if (modifiedUrls.indexOf(uniqueId) === -1) {
          modifiedUrls.push(uniqueId);
        }
        if (url.search(/file:\\/\\//i) === -1 && url.search(/:\\/\\//i) !== -1) {
          if (element.tagName && element.tagName !== 'A') {
            loadNewElementContent(element, key, url, url, undefined, requestingLocation);
          }
        } else {
          url = url.replace('file://', '');
          // Turn relative path into absolute
          const splitArray = url.split('/');
          var absoluteBase = window.baseFsUri;
          if (absoluteBase.search(/\\/\\.ui/i) !== -1) { // if (absoluteBase.endsWith('/.ui')) {
            // const exceptionIndex = absoluteBase.lastIndexOf('/.ui');
            // absoluteBase = absoluteBase.substr(0, exceptionIndex);
            absoluteBase = absoluteBase.substr(0, absoluteBase.search(/\\/\\.ui/i));
          }
          var absolutePath = absoluteBase;
          for (var j = 0; j < splitArray.length; j++) {
            const subPath = splitArray[j];
            if (j === 0 && subPath === '') {
              // '' at index 0 = Start at root folder
              absolutePath = absoluteBase
                .replace(new RegExp(window.fakeLocation.pathname + '$'), '');
            } else if (subPath === '..') {
              // '..' = Go back a folder
              absolutePath = absolutePath.substr(
                0,
                absolutePath.lastIndexOf('/'),
              );
            } else if (subPath !== '' && subPath !== '.') {
              absolutePath = absolutePath + '/' + subPath;
            } else if (subPath === '') {
              // Added to keep this consistent with original syntax
              absolutePath = absolutePath + '/';
            }
            // '.' = Current folder, so no action needed
          }
          let newUrlBase = window.fakeLocation.protocol + '//' + window.fakeLocation.hostname
          let newPathname = absolutePath.replace(window.cacheUri, '')
          if (newPathname === '') {
            newPathname = '/';
          }
          if (newPathname !== '/') {
            let splitNewPath = newPathname.split('/')
            if (splitNewPath[0] !== '') {
              splitNewPath.unshift('')
            }
            splitNewPath.splice(1, 2);
            newPathname = splitNewPath.join('/');
          }
          let newUrl = newUrlBase + newPathname;
          if (element.tagName && element.tagName === 'A') {
            var liveElements = document.getElementsByTagName(element.tagName);
            for (var k = 0; k < liveElements.length; k++) {
              if (requestingLocation === window.location.toString()) {
                let liveEl = liveElements[k];
                let liveAttr = liveEl.getAttribute(key);
                if (
                  liveAttr !== undefined &&
                  liveAttr !== null &&
                  liveAttr.replace('file://', '') === url
                ) {
                  liveEl[key] = newUrl;
                  window.ReactNativeWebView.postMessage("Replaced link ref: " + newUrl + ' in location: ' + requestingLocation);
                }
              } else {
                break;
              }
            }
          } else {
            loadNewElementContent(
              element,
              key,
              element.getAttribute(key),
              newUrl,
              absolutePath,
              requestingLocation,
            );
          }
        }
      }
    } else if (
      customElements.get(elName) !== undefined &&
      requestingLocation === window.location.toString()
    ) {
      var liveElements = document.getElementsByTagName(element.tagName);
      for (var k = 0; k < liveElements.length; k++) {
        if (requestingLocation === window.location.toString()) {
          let liveEl = liveElements[k];
          if (
            !checkInvalidPropPath(liveEl, 'href') &&
            !checkInvalidPropPath(liveEl, 'src') &&
            !checkInvalidPropPath(liveEl, 'path')
          ) {
            customElements.upgrade(liveEl);
            window.ReactNativeWebView.postMessage("Upgraded missing element in location: " + requestingLocation);
          }
        } else {
          break;
        }
      }
    }
  }
}

// parseURL
window.parseURL = function parseURL() {
  return new Promise((resolve, reject) => {
    const url = arguments[0];
    if (!url) {
      return reject('No URL to parse.');
    }
    const urlString = typeof url === 'string' ? url : url.href;
    if (
      urlString.search(/https?:\\/\\//i) === -1 &&
      urlString.search(/file:\\/\\//i) === -1
    ) {
      const randomInt = Math.floor(Math.random() * Math.floor(100000));
      const requestId = 'parseURL_' + new Date().getTime() + '_' + randomInt;
      sendBridgeRequest('__PARSE_URL_LISTENER_ID__', requestId, {
        requestId,
        url,
      });
      waitForBridgeResponse(requestId, nodeParsed => {
        if (nodeParsed.error) {
          reject(nodeParsed);
        } else {
          resolve(nodeParsed);
        }
      });
    } else if (typeof url === 'string') {
      let parsed = new URL(url);
      if (!parsed.pathname || parsed.pathname === '') {
        parsed.pathname = '/';
      }
      resolve(parsed);
    } else {
      resolve(url);
    }
  });
};

function isElement(obj) {
  try {
    //Using W3 DOM2 (works for FF, Opera and Chrome)
    return obj instanceof HTMLElement;
  }
  catch(e){
    //Browsers not supporting W3 DOM2 don't have HTMLElement and
    //an exception is thrown and we end up here. Testing some
    //properties that all elements have (works on IE7)
    return (typeof obj==="object") &&
      (obj.nodeType===1) && (typeof obj.style === "object") &&
      (typeof obj.ownerDocument ==="object");
  }
}

if (!window.refObserver) {
  window.refObserver = new MutationObserver((mutationList, observer) => {
    const requestingLocation = window.location.toString();
    var elementsToFix = [];
    mutationList.forEach((mutation) => {
      switch(mutation.type) {
        case 'childList':
          /* One or more children have been added to and/or removed
             from the tree; see mutation.addedNodes and
             mutation.removedNodes */
          if (mutation.addedNodes.length > 0) {
            [...mutation.addedNodes].map(obj => {
              if (
                isElement(obj) && (
                  checkInvalidPropPath(obj, 'href') ||
                  checkInvalidPropPath(obj, 'src') ||
                  checkInvalidPropPath(obj, 'path'))
              ) {
                elementsToFix.push(obj);
              } else if (
                isElement(obj) &&
                obj.innerHTML !== undefined &&
                obj.innerHTML !== null &&
                obj.innerHTML !== ''
              ) {
                const treeWalker = document.createTreeWalker(obj, NodeFilter.SHOW_ELEMENT);
                var walkNode = treeWalker.currentNode;
                while (walkNode !== undefined && walkNode !== null) {
                  if (
                    checkInvalidPropPath(walkNode, 'href', true) ||
                    checkInvalidPropPath(walkNode, 'src', true) ||
                    checkInvalidPropPath(walkNode, 'path', true)
                  ) {
                    elementsToFix.push(walkNode);
                  }
                  walkNode = treeWalker.nextNode();
                }
              }
            });
          }
          break;
        case 'attributes':
          /* An attribute value changed on the element in
             mutation.target; the attribute name is in
             mutation.attributeName and its previous value is in
             mutation.oldValue */
             if (
               isElement(mutation.target) &&
               (mutation.attributeName === 'href' ||
               mutation.attributeName === 'src' ||
               mutation.attributeName === 'path') &&
               checkInvalidPropPath(mutation.target, mutation.attributeName)
             ) {
               elementsToFix.push(mutation.target);
             }
        break;
      }
    });
    if (requestingLocation === window.location.toString() && elementsToFix.length > 0) {
      const undefinedElements = document.querySelectorAll(':not(:defined)');
      [...undefinedElements].map(missingEl => {
        if (window.missingElements.indexOf(missingEl.localName) === -1) {
          window.missingElements.push(missingEl.localName);
          var liveElements = document.getElementsByTagName(missingEl.tagName);
          var lastAddition = -2;
          for (var k = 0; k < liveElements.length; k++) {
            if (requestingLocation === window.location.toString()) {
              let liveEl = liveElements[k];
              customElements.upgrade(liveEl);
            } else {
              break;
            }
          }
          window.ReactNativeWebView.postMessage("Added Missing Element: " + missingEl.localName);
          if (customElements.get(missingEl.localName) !== undefined) {
            window.ReactNativeWebView.postMessage("Not yet defined");
          } else {
            window.ReactNativeWebView.postMessage("Already defined");
          }
          // NOTE - DEBUG POINT
          customElements.whenDefined(missingEl.localName).then(() => {
            if (requestingLocation === window.location.toString()) {
              window.ReactNativeWebView.postMessage("Missing Element Defined! " + missingEl.localName);
              const elQuery = document.querySelectorAll("*");
              const newElements = [...elQuery].filter(el => el.localName === missingEl.localName);
              window.fixElementURLs(newElements, requestingLocation);
            }
          }).catch(e => {
            window.ReactNativeWebView.postMessage("Custom Element whenDefined Error: " + JSON.stringify(e));
          });
        }
      });
      window.fixElementURLs(elementsToFix, requestingLocation);
    }
  });
}
window.refObserver.observe(document.documentElement, {
  attributes: true,
  attributeOldValue: true,
  attributeFilter: ['href', 'src', 'path'],
  characterData: true,
  characterDataOldValue: true,
  childList: true,
  subtree: true,
});

document.addEventListener('readystatechange', event => {
    // When HTML/DOM elements are ready:
    if (event.target.readyState === "interactive") {
      //does same as: ..addEventListener("DOMContentLoaded"..
    }
    // When window loaded (external resources are loaded too- css, src, etc...)
    if (event.target.readyState === "complete") {
      window.ReactNativeWebView.postMessage("Requesting Theme!");
    }
});

window.ReactNativeWebView.postMessage("SUCCESSFULLY INJECTED INTO WEBVIEW!");`;
