import nodejs from 'nodejs-mobile-react-native';
import {Platform} from 'react-native';
import Share from 'react-native-share';

export function nodeRequest(
  {module, requestId, uniqueNodeId, params},
  callback,
) {
  // NOTE: requestId = 'requestType*timeInSeconds*randomId';
  if (callback) {
    console.log('[DAT] Request Has Callback');
    nodejs.channel.addListener(requestId, callback);
  }
  console.log('[DAT] React Native Making Node Request', {
    module,
    requestId,
    uniqueNodeId,
    params,
  });
  nodejs.channel.post('web-api-bridge', {
    module,
    requestId,
    uniqueNodeId,
    params,
  });
}
