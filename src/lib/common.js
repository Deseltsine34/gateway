import {Alert} from 'react-native';
const parseURL = require('url-parse');
import nodejs from 'nodejs-mobile-react-native';

export function getImagePalette(params) {
  return new Promise((resolve, reject) => {
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId =
      'get-colors' + '*' + new Date().getTime() + '*' + randomInt;
    nodejs.channel.post('get-colors', {
      ...params,
      requestId,
    });
    nodejs.channel.addListener(requestId, ({error, result}) => {
      if (error) {
        reject(error);
      } else if (result) {
        resolve(result);
      }
    });
  });
}

export function decodeQRImage(params) {
  return new Promise((resolve, reject) => {
    const randomInt = Math.floor(Math.random() * Math.floor(100000));
    const requestId =
      'decode-qr' + '*' + new Date().getTime() + '*' + randomInt;
    nodejs.channel.post('decode-qr', {
      ...params,
      requestId,
    });
    nodejs.channel.addListener(requestId, ({error, result}) => {
      if (error) {
        reject(error);
      } else if (result) {
        resolve(result);
      }
    });
  });
}

export function getInfoKey(url) {
  const matches = url
    .replace('www.', '')
    .match(/[A-Za-z0-9-.]+:\/\/[A-Za-z0-9-.]+/i);
  if (matches === undefined || matches === null || matches.length === 0) {
    return undefined;
  }
  return matches[0];
}

export function formatError(error) {
  if (typeof error === 'string') {
    return {description: error};
  }
  return {
    code: error.code || error.statusCode || error.errorCode || error.status,
    domain: error.domain || error.name,
    description: error.description || error.message,
    category: error.category,
  };
}

export function maybeResetChameleonTheme({
  dispatch,
  getState,
  tabIndex,
  newUrl,
}) {
  // TODO H - Take host-specific and page-specific chameleonMode into account
  // TODO M - Reset to default theme if navigating back/forward across hostnames where the destination context isn't a site
  const _tabIndex = tabIndex || 0;
  const {currentIndex, history} = getState().tabHistory.get(_tabIndex);
  const source = history.get(currentIndex);
  if (source && getState().theme.get('chameleonMode')) {
    const sourceUrl = source.requestUrl || source.inputUrl || source.uri;
    const sourceHost = parseURL(sourceUrl).hostname;
    const newHost = parseURL(newUrl).hostname;
    console.log('Comparing sourceHost:', sourceHost, 'and newHost:', newHost);
    if (sourceHost !== newHost) {
      dispatch({
        type: 'RESET_CHAMELEON_THEME',
      });
    } else if (source.request) {
      const earlierSource = history.get(currentIndex + 1);
      if (earlierSource) {
        let earlierSourceUrl =
          earlierSource.requestUrl ||
          earlierSource.inputUrl ||
          earlierSource.uri;
        let earlierSourceHost = parseURL(earlierSourceUrl).hostname;
        if (earlierSourceHost !== newHost) {
          dispatch({
            type: 'RESET_CHAMELEON_THEME',
          });
        }
      }
    }
  } else if (!source) {
    dispatch({
      type: 'RESET_CHAMELEON_THEME',
    });
  }
}

export function dispatchBrowserError(
  {url, tabIndex, error: _error},
  dispatch,
  getState,
) {
  const error = formatError(_error);
  console.log('[ERROR] Tab Error', url, tabIndex, _error, error);
  const _tabIndex = tabIndex || 0;
  const {currentIndex, history} = getState().tabHistory.get(_tabIndex);
  const source = history.get(currentIndex);
  if (
    source &&
    url !== undefined &&
    url !== null &&
    tabIndex !== undefined &&
    tabIndex !== null &&
    !(url && (!source.inputUrl || source.inputUrl !== url))
  ) {
    const protocol = getProtocolSchema(url, getState().protocols);
    const protocolName = protocol.name || 'Request';
    if (url && !error.category) {
      error.category = `${protocolName} Error`;
    }
    maybeResetChameleonTheme({
      dispatch,
      getState,
      tabIndex,
      newUrl: url,
    });
    dispatch({
      type: 'SET_TAB_ERROR',
      tabIndex,
      error: {...error, source},
    });
  } else {
    let bodyText = '';
    if (typeof error === 'object') {
      for (let [key, value] of Object.entries(error)) {
        if (key !== 'stack' && typeof value !== 'object') {
          const newline = bodyText === '' ? '' : '\n';
          bodyText = bodyText + `${newline}${capitalize(key)}: ${value}`;
        }
      }
    } else {
      bodyText = `${error}`;
    }
    Alert.alert('Request Error', bodyText);
  }
}

export function getProtocolSchema(url, protocolState) {
  if (!protocolState) {
    return {};
  }
  const protocols = protocolState.toArray();
  // TODO M - Handle conflicting protocol RegExp's
  for (var i = 0; i < protocols.length; i++) {
    const protocol = protocols[i];
    if (typeof protocol === 'string') {
      // TODO H - Get JSON from reference
    }
    const protocolPrefix = protocol.prefix;
    const prefixRegex = new RegExp(protocolPrefix);
    if (prefixRegex.test(url)) {
      return protocol;
    }
  }
  return {};
}

export function parseSchemaIcon(icon) {
  if (!icon) {
    return {};
  }
  if (typeof icon === 'string') {
    return {asset: icon};
  }
  return icon;
}

// Format text to display in navigation input
export function displayUrl(text = '', removeSubs) {
  let lowerText = text.toLowerCase();
  let filteredText = lowerText;
  if (lowerText.search(/dat:\/\/|hyper:\/\/|gateway:\/\//i) === 0) {
    if (lowerText.search(/gateway:\/\//i) === 0) {
      lowerText = lowerText.replace(/gateway:\/\//i, 'dat://');
    }
    if (lowerText.length >= 70) {
      const shortText =
        filteredText.substr(0, 12) + '..' + filteredText.substr(68, 2);
      filteredText = shortText;
    }
  } else {
    filteredText = filteredText.replace(/https:\/\/|http:\/\/|www\./gi, '');
  }
  if (
    removeSubs &&
    text.search(/http/i) === 0 &&
    filteredText.indexOf('/') !== -1
  ) {
    const periodSubString = filteredText.substr(filteredText.indexOf('.'));
    const firstSlashSubString = periodSubString.substr(
      periodSubString.indexOf('/'),
    );
    filteredText = filteredText.replace(firstSlashSubString, '');
  }
  return filteredText;
}

export function getURLData(url, shortenHost) {
  let host = null;
  let version = null;
  let protocol = null;
  let pathname = null;

  if (url) {
    if (url.indexOf('://') === -1) {
      url = `dat://${url}`;
    }
    const parsed = parseURL(url);
    protocol =
      parsed.protocol.indexOf('undefined') === 0 ? 'hyper:' : parsed.protocol;
    pathname = parsed.pathname;
    if (parsed.hostname.indexOf('.') === -1) {
      const hostnameParts = parsed.hostname.split('+');
      host = hostnameParts[0];
      if (host.length >= 64 && shortenHost) {
        const shortHost =
          host.substr(0, 4) + '..' + host.substr(host.length - 2, 2);
        host = shortHost;
      }
      version = hostnameParts[1] || null;
      if (version === '') {
        version = null;
      }
      if (typeof version === 'string') {
        version = Number(version);
      }
    } else {
      host = parsed.hostname;
    }
  }

  return {
    host,
    version,
    protocol,
    pathname: pathname || '/',
  };
}

export function convertObjToString(obj) {
  let string = '{';
  const entries = Object.entries(obj);
  for (var i = 0; i < entries.length; i++) {
    const entry = entries[i];
    const endOfString = i === entries.length - 1 ? '' : ', ';
    const value =
      typeof entry[1] === 'object'
        ? convertObjToString(entry[1])
        : Array.isArray(entry[1])
        ? convertArrayToString(entry[1])
        : entry[1];
    const wrap = typeof entry[1] === 'string' ? '"' : '';
    const entryString = `${entry[0]}: ${wrap}${value}${wrap}${endOfString}`;
    string += entryString;
  }
  string += '}';
  return string;
}

export function convertArrayToString(array) {
  let string = '[';
  for (var i = 0; i < array.length; i++) {
    const entry = array[i];
    const endOfString = i === array.length - 1 ? '' : ', ';
    const value =
      typeof entry === 'object'
        ? convertObjToString(entry)
        : Array.isArray(entry)
        ? convertArrayToString(entry)
        : entry;
    const wrap = typeof entry === 'string' ? '"' : '';
    const entryString = `${wrap}${value}${wrap}${endOfString}`;
    string += entryString;
  }
  string += ']';
  return string;
}

export const GIGABYTE_IN_BYTES = 1073741824;
export const MEGABYTE_IN_BYTES = 1048576;
export const KILOBYTE_IN_BYTES = 1024;

export function formatSizeString(bytes) {
  if (!bytes) {
    return '';
  }
  let calculatedSize;
  let sizeUnit;
  if (bytes >= GIGABYTE_IN_BYTES) {
    // Gigabytes (GB)
    calculatedSize = bytes / GIGABYTE_IN_BYTES;
    sizeUnit = 'GB';
  } else if (bytes >= MEGABYTE_IN_BYTES) {
    // Megabytes (MB)
    calculatedSize = bytes / MEGABYTE_IN_BYTES;
    sizeUnit = 'MB';
  } else {
    //Kilobytes (KB)
    calculatedSize = bytes / KILOBYTE_IN_BYTES;
    sizeUnit = 'KB';
  }
  const roundedSizeUnit =
    Math.round((calculatedSize + Number.EPSILON) * 100) / 100;
  const sizeText = `${roundedSizeUnit} ${sizeUnit}`;
  return sizeText;
}

export function capitalize(string) {
  if (typeof string !== 'string') {
    return string;
  }
  const [firstLetter, ...rest] = string;
  return [firstLetter.toLocaleUpperCase(), ...rest].join('');
}

export function round(value, precision) {
  var multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}

// export function findStringOverlap(a, b) {
//   if (b.length === 0) {
//     return '';
//   }
//   if (a.endsWith(b)) {
//     return b;
//   }
//   if (a.indexOf(b) >= 0) {
//     return b;
//   }
//   return findStringOverlap(a, b.substring(0, b.length - 1));
// }

export function findStringOverlap(a, b, originalB, reverse) {
  if (!originalB) {
    originalB = b;
  }
  if (b.length === 0 && !reverse) {
    return findStringOverlap(a, originalB, originalB, true);
  }
  if (a.endsWith(b)) {
    return b;
  }
  if (a.indexOf(b) >= 0) {
    return b;
  }
  if (!reverse) {
    return findStringOverlap(a, b.substring(0, b.length - 1), originalB, false);
  } else {
    return findStringOverlap(a, b.substring(1, b.length), originalB, true);
  }
}
