/**
 * @format
 * @flow
 */

import React from 'react';
import {Platform} from 'react-native';

import PopupModalBackground from '../views/PopupModalBackground';
import TabHistoryPopup from '../views/TabHistoryPopup';

const TabHistoryPopupScreen = ({
  route: {
    params: {tabIndex, isBack},
  },
}: props) => {
  return (
    <PopupModalBackground
      button={isBack ? 'back' : Platform.OS === 'ios' ? 'fwd' : 'opt'}>
      <TabHistoryPopup tabIndex={tabIndex} isBack={isBack} />
    </PopupModalBackground>
  );
};

export default TabHistoryPopupScreen;
