/**
 * @format
 * @flow
 */

import React from 'react';

import PopupModalBackground from '../views/PopupModalBackground';
import OmniOptionsPopup from '../views/OmniOptionsPopup';

const OmniOptionsPopupScreen = ({
  route: {
    params: {tabIndex},
  },
}: props) => {
  return (
    <PopupModalBackground button="omni">
      <OmniOptionsPopup tabIndex={tabIndex} />
    </PopupModalBackground>
  );
};

export default OmniOptionsPopupScreen;
