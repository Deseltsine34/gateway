/**
 * @format
 * @flow
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ProtocolDescription from '../views/ProtocolDescription';
import BottomCardModal from '../views/BottomCardModal';
import SiteInfoHeader from '../views/SiteInfoHeader';
import {ActionCreators} from '../actions';

const ProtocolInfoScreen = ({
  route: {
    params: {tabIndex},
  },
}: props) => {
  // TODO H - Toggle Live Reloading
  // TODO H - View/Edit Permissions
  // TODO M - View/Create/Merge Forks, Revisions, Toggle Hosting
  return (
    <BottomCardModal>
      <SiteInfoHeader tabIndex={tabIndex} />
      <ProtocolDescription tabIndex={tabIndex} />
    </BottomCardModal>
  );
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProtocolInfoScreen);
