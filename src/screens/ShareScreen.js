/**
 * @format
 * @flow
 */

import React, {Fragment, useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  Alert,
  Platform,
  StatusBar,
  StyleSheet,
  PixelRatio,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Share from 'react-native-share';
import Clipboard from '@react-native-community/clipboard';
import ViewShot from 'react-native-view-shot';
import SvgRenderer from 'react-native-svg-renderer';
import ImageEditor from '@react-native-community/image-editor';
var RNFS = require('react-native-fs');
import {systemWeights} from 'react-native-typography';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

const QRCode = require('qrcode');

import BottomCardModal from '../views/BottomCardModal';
import ThemeView from '../views/ThemeView';
import ThemeText from '../views/ThemeText';
import ThemeSwitch from '../views/ThemeSwitch';
import ThemeSegment from '../views/ThemeSegment';
import {getInfoKey} from '../lib/common';
import NavigationService from '../lib/NavigationService';
import {ActionCreators} from '../actions';
import {squareQrSvg, verticalQrSvg, horizontalQrSvg} from '../svg';

function RGBAToHexA(rgba) {
  let sep = rgba.indexOf(',') > -1 ? ',' : ' ';
  rgba = rgba
    .substr(5)
    .split(')')[0]
    .split(sep);

  // strip the slash if using space-separated syntax
  if (rgba.indexOf('/') > -1) {
    rgba.splice(3, 1);
  }

  for (let R in rgba) {
    let r = rgba[R];
    if (r.indexOf('%') > -1) {
      let p = r.substr(0, r.length - 1) / 100;

      if (R < 3) {
        rgba[R] = Math.round(p * 255);
      } else {
        rgba[R] = p;
      }
    }
  }

  let r = (+rgba[0]).toString(16),
    g = (+rgba[1]).toString(16),
    b = (+rgba[2]).toString(16),
    a = Math.round(+rgba[3] * 255).toString(16);

  if (r.length === 1) {
    r = '0' + r;
  }
  if (g.length === 1) {
    g = '0' + g;
  }
  if (b.length === 1) {
    b = '0' + b;
  }
  if (a.length === 1) {
    a = '0' + a;
  }

  const hex = '#' + r + g + b + a;
  return hex;
}
const pixelRatio = PixelRatio.get();

const ShareScreen = ({
  route: {
    params: {tabIndex},
  },
  deviceDimensions,
  tabHistory,
  showToast,
  siteInfo,
  theme,
}: props) => {
  const viewShot = useRef(null);
  const statusBarHeight = StatusBar.statusBarHeight || 24;
  const screenWidth = deviceDimensions.get('width');
  const screenHeight = deviceDimensions.get('height');
  let trimmedScreenWidth = screenWidth - 20;
  const orientation = deviceDimensions.get('orientation');
  if (orientation === 'landscape') {
    trimmedScreenWidth = trimmedScreenWidth - statusBarHeight - 10;
  }
  const imageHeight = screenHeight / 4;
  const buttonContainerStyle =
    orientation === 'landscape'
      ? {
          flexDirection: 'row',
        }
      : {
          flexDirection: 'column',
        };
  const leftButtonMarginStyle =
    orientation === 'landscape' ? styles.smallRightMargin : null;
  let halfWidth = trimmedScreenWidth / 2 - 2.5;
  const {currentIndex, history} = tabHistory.get(tabIndex);
  const currentPage = history.get(currentIndex);
  const url = currentPage.requestUrl || currentPage.inputUrl || currentPage.uri;
  const isP2P = url.search(/https?:\/\/|file:\/\//i) !== 0;
  const [qrData, setQrData] = useState(undefined);
  const infoKey = getInfoKey(url);
  let info =
    infoKey && siteInfo.get(infoKey) ? siteInfo.get(infoKey).info : undefined;
  if (!info) {
    info = {};
  }
  const title = info.title;
  const description = info.description;
  const [hasQR, toggleQR] = useState(false);
  const [hasInvite, toggleInvite] = useState(false);
  const [shapeIndex, setShape] = useState(0);
  const [colorIndex, toggleColor] = useState(0);
  const colorOptions = ['Color', 'B&W'];
  const colorMode = colorOptions[colorIndex];
  const shapeOptions = hasInvite
    ? ['Horizontal', 'Vertical']
    : ['Square', 'Square'];
  const shape = shapeOptions[shapeIndex];
  useEffect(() => {
    const subState = theme.get('chameleonMode') ? 'chameleon' : 'default';
    const backgroundColor =
      colorMode === 'Color'
        ? theme.get(subState).get('backgroundColor')
        : 'rgba(255,255,255,1.0)';
    const highlightColor =
      colorMode === 'Color'
        ? theme.get(subState).get('highlightColor')
        : 'rgba(0,0,0,1.0)';
    const textColor =
      colorMode === 'Color'
        ? theme.get(subState).get('textColor')
        : 'rgba(0,0,0,1.0)';
    const bgHexA = RGBAToHexA(backgroundColor);
    const hlHexA = RGBAToHexA(highlightColor);
    let QR_SIZE =
      shape === 'Square'
        ? 1975
        : shape === 'Vertical'
        ? description
          ? 1203
          : 1252
        : 1342;
    let QR_X =
      shape === 'Square'
        ? 300
        : shape === 'Vertical'
        ? description
          ? 645
          : 625
        : 1692;
    let QR_Y =
      shape === 'Square'
        ? 300
        : shape === 'Vertical'
        ? description
          ? 1840
          : 1810
        : 600;
    let INVITE_QR_X = shape === 'Vertical' ? (description ? 645 : 625) : 300;
    let INVITE_QR_Y = shape === 'Vertical' ? (description ? 370 : 330) : 600;
    const genOptions = {
      type: 'svg',
      margin: 1,
      width: QR_SIZE,
      color: {
        light: bgHexA,
        dark: hlHexA,
      },
    };
    QRCode.toString(url, genOptions, async (error, qrSvg) => {
      const qrViewBoxSizeIndex = qrSvg.indexOf('viewBox="0 0 ');
      const qrViewBoxSize = qrSvg.substr(qrViewBoxSizeIndex + 13, 2);
      const QR_SCALE = QR_SIZE / Number(qrViewBoxSize);
      qrSvg = qrSvg
        .replace(
          '<svg xmlns="http://www.w3.org/2000/svg"',
          `<g id="qrcode" transform="translate(${QR_X}, ${QR_Y}) scale(${QR_SCALE}, ${QR_SCALE})"`,
        )
        .replace('</svg', '</g')
        .replace('\n', '');
      let inviteQrSvg = '';
      if (shape !== 'Square') {
        inviteQrSvg = await QRCode.toString(
          'https://GatewayBrowser.com/',
          genOptions,
        );
        const inviteViewBoxSizeIndex = inviteQrSvg.indexOf('viewBox="0 0 ');
        const inviteViewBoxSize = inviteQrSvg.substr(
          inviteViewBoxSizeIndex + 13,
          2,
        );
        const INVITE_SCALE = QR_SIZE / Number(inviteViewBoxSize);
        inviteQrSvg = inviteQrSvg
          .replace(
            '<svg xmlns="http://www.w3.org/2000/svg"',
            `<g id="qrcode" transform="translate(${INVITE_QR_X}, ${INVITE_QR_Y}) scale(${INVITE_SCALE}, ${INVITE_SCALE})"`,
          )
          .replace('</svg', '</g')
          .replace('\n', '');
      }
      const qrTitle = title || url;
      const qrDescription = description || '';
      let backgroundSvg =
        shape === 'Square'
          ? squareQrSvg
          : shape === 'Vertical'
          ? verticalQrSvg
          : horizontalQrSvg;
      if (!description && shape === 'Vertical') {
        backgroundSvg = backgroundSvg
          .replace(
            '<text id="Description" font-size="64" font-weight="normal" fill="TEXT_COLOR"><tspan x="38%" y="360" dominant-baseline="middle" text-anchor="middle">DESCRIPTION</tspan></text>\n',
            '',
          )
          .replace('y="310"', 'y="335"');
      } else if (!description && shape === 'Horizontal') {
        backgroundSvg = backgroundSvg
          .replace(
            '<text id="Description" font-size="64" font-weight="normal" fill="TEXT_COLOR"><tspan x="50%" y="480" dominant-baseline="middle" text-anchor="middle">DESCRIPTION</tspan></text>\n',
            '',
          )
          .replace('y="430"', 'y="455"');
      }
      let qrImageSvg = backgroundSvg
        .replace('<QRCODE/>', qrSvg)
        .replace('<INVITE_QRCODE/>', inviteQrSvg)
        .replace(
          /TEXT_COLOR/g,
          textColor.replace(', 1.0', '').replace('rgba', 'rgb'),
        )
        .replace(
          /BACKGROUND_COLOR/g,
          backgroundColor.replace(', 1.0', '').replace('rgba', 'rgb'),
        )
        .replace(
          /HIGHLIGHT_COLOR/g,
          highlightColor.replace(', 1.0', '').replace('rgba', 'rgb'),
        )
        .replace('TITLE', qrTitle)
        .replace('DESCRIPTION', qrDescription);
      setQrData(qrImageSvg);
    });
  }, [shape, colorMode, description, qrData, theme, title, url]);
  const titleInsert = title ? `${title}\n` : '';
  const descriptionInsert = description ? `${description}\n` : '';
  let shareText = `${titleInsert}${descriptionInsert}${url}`;
  if (hasInvite) {
    shareText = `Step 1. Download the Gateway mobile browser:\nhttps://gatewaybrowser.com/\nStep 2. Visit this P2P site:\n${titleInsert}${descriptionInsert}${url}`;
  }

  const copyToClipboard = () => {
    // TODO M - Copy images to clipboard when RNClipboard adds support
    Clipboard.setString(shareText);
    showToast('Copied to your clipboard!');
    NavigationService.goBack();
  };
  const nativeShare = async () => {
    if (hasQR && !qrData) {
      Alert.alert(
        'Generating QR Code',
        'You can share in a sec when your image is ready. 😊',
      );
    } else {
      try {
        let shareUrls = [];
        if (hasQR) {
          let png = await viewShot.current.capture();
          let cropped = png;
          // HACK - react-native-view-shot can't screenshot larger non-square images, so we need to crop and get base64 of that image
          //      - Issue Ref: https://github.com/gre/react-native-view-shot/issues/154
          const SHORTER_SIDE_SIZE = SCREENSHOT_IMAGE_SIZE * 0.77272727;
          const MARGIN_OFFSET = (SCREENSHOT_IMAGE_SIZE - SHORTER_SIDE_SIZE) / 2;
          if (shape === 'Vertical') {
            cropped = await ImageEditor.cropImage(png, {
              offset: {
                x: MARGIN_OFFSET * pixelRatio,
                y: 0,
              },
              size: {
                width: SHORTER_SIDE_SIZE * pixelRatio,
                height: SCREENSHOT_IMAGE_SIZE * pixelRatio,
              },
            });
          }
          if (shape === 'Horizontal') {
            cropped = await ImageEditor.cropImage(png, {
              offset: {
                x: 0,
                y: MARGIN_OFFSET * pixelRatio,
              },
              size: {
                width: SCREENSHOT_IMAGE_SIZE * pixelRatio,
                height: SHORTER_SIDE_SIZE * pixelRatio,
              },
            });
          }
          let base64 = await RNFS.readFile(cropped, 'base64');
          if (base64.indexOf('data:image/png;base64,') === -1) {
            base64 = 'data:image/png;base64,' + base64;
          }
          shareUrls = [base64];
        }
        if (shareText === url) {
          shareUrls.push(shareText);
        }
        let shareOptions = {};
        if (shareUrls.length === 1) {
          shareOptions.url = shareUrls[0];
        } else if (shareUrls.length > 1) {
          shareOptions.urls = shareUrls;
        }
        if (shareText !== url) {
          shareOptions.message = shareText;
        }
        await Share.open(shareOptions);
      } catch (shareError) {
        console.log('[BROWSER] Share error:', shareError);
      }
    }
  };
  // HACK - Android SVG Renderer causes app to crash if it's too big
  const SCREENSHOT_IMAGE_SIZE = Platform.OS === 'android' ? 1275 : 2550;
  let buttons = hasQR
    ? []
    : [
        <TouchableOpacity
          style={styles.smallBottomMargin}
          onPress={copyToClipboard}
          key="copyToClipboardButton">
          <ThemeView
            style={[
              styles.button,
              styles.center,
              {
                width:
                  orientation === 'landscape' ? halfWidth : trimmedScreenWidth,
              },
              leftButtonMarginStyle,
            ]}
            type="textColor"
            opacity={0.15}>
            <ThemeText style={[styles.text, styles.titleTextSize]}>
              Copy to Clipboard
            </ThemeText>
          </ThemeView>
        </TouchableOpacity>,
      ];
  buttons.push(
    <TouchableOpacity onPress={nativeShare} key="shareButton">
      <ThemeView
        style={[
          styles.button,
          styles.center,
          {
            width:
              orientation === 'landscape' && !hasQR
                ? halfWidth
                : trimmedScreenWidth,
          },
        ]}
        type="textColor"
        opacity={0.15}>
        <ThemeText style={[styles.text, styles.titleTextSize]}>
          Share...
        </ThemeText>
      </ThemeView>
    </TouchableOpacity>,
  );
  return (
    <Fragment>
      {qrData ? (
        <View
          style={[styles.svgCapturer, {left: screenWidth}]}
          collapsable={false}>
          <ViewShot
            ref={viewShot}
            options={{
              width: SCREENSHOT_IMAGE_SIZE,
              height: SCREENSHOT_IMAGE_SIZE,
            }}>
            <SvgRenderer
              width={`${SCREENSHOT_IMAGE_SIZE}`}
              height={`${SCREENSHOT_IMAGE_SIZE}`}
              svgXmlData={qrData}
            />
          </ViewShot>
        </View>
      ) : null}
      <BottomCardModal>
        <ThemeView style={styles.card}>
          {/* Title */}
          <Text
            style={[
              styles.titleTextSize,
              styles.bottomMargin,
              {width: trimmedScreenWidth},
            ]}
            numberOfLines={2}>
            <ThemeText style={styles.boldText} numberOfLines={1}>
              {'Share:\n'}
            </ThemeText>
            <ThemeText style={styles.text} numberOfLines={1}>
              {url}
            </ThemeText>
          </Text>
          {/* QR Display */}
          {hasQR && qrData ? (
            <ThemeView
              style={[
                styles.center,
                styles.bottomMargin,
                {
                  height:
                    orientation === 'landscape'
                      ? imageHeight
                      : trimmedScreenWidth,
                  width: trimmedScreenWidth,
                },
              ]}>
              <SvgRenderer
                height={
                  orientation === 'landscape'
                    ? `${imageHeight}`
                    : `${trimmedScreenWidth}`
                }
                width={
                  orientation === 'landscape'
                    ? `${imageHeight}`
                    : `${trimmedScreenWidth}`
                }
                style={styles.bottomMargin}
                svgXmlData={qrData}
              />
            </ThemeView>
          ) : null}
          {hasQR && !qrData ? (
            <ThemeView
              style={[
                styles.center,
                styles.bottomMargin,
                {
                  height:
                    orientation === 'landscape'
                      ? imageHeight
                      : trimmedScreenWidth,
                  width: trimmedScreenWidth,
                },
              ]}>
              <ThemeView
                style={[
                  styles.center,
                  {
                    height:
                      orientation === 'landscape'
                        ? imageHeight
                        : trimmedScreenWidth,
                    width: trimmedScreenWidth,
                  },
                ]}>
                <ActivityIndicator size="large" />
              </ThemeView>
            </ThemeView>
          ) : null}
          {/* Segmented Controls */}
          {hasQR ? (
            <ThemeView style={[styles.container, styles.bottomMargin]}>
              <ThemeSegment
                values={colorOptions}
                selectedIndex={colorIndex}
                onChange={event => {
                  toggleColor(event.nativeEvent.selectedSegmentIndex);
                }}
                style={[
                  styles.smallRightMargin,
                  {width: hasInvite ? halfWidth : trimmedScreenWidth},
                ]}
              />
              {hasInvite ? (
                <ThemeSegment
                  values={shapeOptions}
                  selectedIndex={shapeIndex}
                  onChange={event => {
                    setShape(event.nativeEvent.selectedSegmentIndex);
                  }}
                  style={{width: halfWidth}}
                />
              ) : null}
            </ThemeView>
          ) : null}
          {/* Switches */}
          <ThemeView style={styles.wrap}>
            <ThemeView
              style={[
                styles.container,
                styles.bottomMargin,
                styles.rightMargin,
              ]}>
              <ThemeSwitch
                onValueChange={() => {
                  toggleQR(previousState => !previousState);
                }}
                value={hasQR}
                style={styles.smallRightMargin}
              />
              <ThemeText style={[styles.text, styles.bodyTextSize]}>
                QR Code
              </ThemeText>
            </ThemeView>
            {isP2P ? (
              <ThemeView style={[styles.container, styles.bottomMargin]}>
                <ThemeSwitch
                  onValueChange={() => {
                    toggleInvite(previousState => !previousState);
                  }}
                  value={hasInvite}
                  style={styles.smallRightMargin}
                />
                <ThemeText style={[styles.text, styles.bodyTextSize]}>
                  Gateway Invite
                </ThemeText>
              </ThemeView>
            ) : null}
          </ThemeView>
          {/* Buttons */}
          <ThemeView style={buttonContainerStyle}>
            {buttons.map(button => button)}
          </ThemeView>
        </ThemeView>
      </BottomCardModal>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  svgCapturer: {
    position: 'absolute',
    top: 0,
  },
  webView: {
    position: 'absolute',
    flex: 0,
    height: 0,
    width: 0,
  },
  transparent: {
    backgroundColor: 'transparent',
  },
  center: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boldText: {
    ...systemWeights.semibold,
  },
  text: {
    ...systemWeights.regular,
  },
  titleTextSize: {
    fontSize: 22,
  },
  bodyTextSize: {
    fontSize: 18,
  },
  smallTextSize: {
    fontSize: 15,
  },
  bottomMargin: {
    marginBottom: 10,
  },
  smallBottomMargin: {
    marginBottom: 5,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  rightMargin: {
    marginRight: 10,
  },
  smallRightMargin: {
    marginRight: 5,
  },
  black: {
    color: 'black',
  },
  white: {
    backgroundColor: 'white',
  },
  button: {
    borderRadius: 15,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
    tabHistory: state.tabHistory,
    siteInfo: state.siteInfo,
    theme: state.theme,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShareScreen);
