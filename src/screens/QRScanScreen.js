/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import SvgRenderer from 'react-native-svg-renderer';
import {RNCamera} from 'react-native-camera';
import {systemWeights} from 'react-native-typography';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {ActionCreators} from '../actions';
import NavigationService from '../lib/NavigationService';
import CameraRollButton from '../views/CameraRollButton';
import {bigXSvg, flashOnSvg, flashOffSvg} from '../svg';

const QRScanScreen = ({
  route: {
    params: {tabIndex},
  },
  pushHistory,
  deviceDimensions,
}: props) => {
  const screenWidth = deviceDimensions.get('width');
  const textWidth = screenWidth - 50;
  const [flash, setFlash] = useState(RNCamera.Constants.FlashMode.off);
  let lastQR = '';
  // TODO L - Open scanned source in a new tab
  const scanQR = ({data}, force) => {
    if (typeof data === 'string' && (force === true || data !== lastQR)) {
      lastQR = data;
      console.log('[QR] Code Scanned', data);
      if (data.search(/https?:\/\/|file:\/\//i) !== 0) {
        // TODO L - Don't include tabIndex so that it creates a new tab
        pushHistory(tabIndex, {inputUrl: data, request: true});
      } else if (data.indexOf('.') !== -1) {
        if (data.search(/http/i) !== 0) {
          const httpsPrefix = 'https://';
          data = httpsPrefix.concat(data);
        }
        if (data.charAt(data.length - 1) !== '/') {
          data = data.concat('/');
        }
        pushHistory(tabIndex, {uri: data});
      }
      NavigationService.goBack();
    }
  };
  const toggleFlash = () => {
    if (flash === RNCamera.Constants.FlashMode.off) {
      setFlash(RNCamera.Constants.FlashMode.torch);
    } else {
      setFlash(RNCamera.Constants.FlashMode.off);
    }
  };
  return (
    <View style={styles.background}>
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={styles.camera}
        autoFocus={RNCamera.Constants.AutoFocus.on}
        type={RNCamera.Constants.Type.back}
        flashMode={flash}
        captureAudio={false}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message:
            'Gateway needs permission to use the camera if you want to scan QR codes',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        onBarCodeRead={scanQR}
      />
      <View style={[styles.header, styles.overlay, {screenWidth}]}>
        <SafeAreaView style={[styles.textContainer, {screenWidth}]}>
          <Text style={styles.text} numberOfLines={3} selectable={false}>
            Point camera at a QR code to instantly search or visit a URL
          </Text>
        </SafeAreaView>
      </View>
      <TouchableOpacity
        style={[styles.button, styles.overlay]}
        onPress={NavigationService.goBack}>
        <SvgRenderer height="12" width="12" fill="white" svgXmlData={bigXSvg} />
      </TouchableOpacity>
      <CameraRollButton scanQR={scanQR} />
      <TouchableOpacity
        style={[styles.button, styles.overlay]}
        onPress={toggleFlash}>
        <SvgRenderer
          height={flash === RNCamera.Constants.FlashMode.torch ? '16' : '20'}
          width={flash === RNCamera.Constants.FlashMode.torch ? '12.5' : '20'}
          fill="white"
          svgXmlData={
            flash === RNCamera.Constants.FlashMode.torch
              ? flashOnSvg
              : flashOffSvg
          }
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  camera: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  background: {
    flex: 1,
    backgroundColor: 'black',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  transparent: {
    backgroundColor: 'transparent',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
  },
  header: {
    position: 'absolute',
    alignItems: 'center',
    top: 0,
    left: 0,
    right: 0,
  },
  text: {
    ...systemWeights.regular,
    textAlign: 'center',
    color: 'white',
    marginTop: 10,
    fontSize: 20,
  },
  button: {
    width: 44,
    height: 44,
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    marginBottom: 30,
  },
  textContainer: {
    backgroundColor: 'transparent',
    margin: 25,
  },
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps(state) {
  return {
    deviceDimensions: state.deviceDimensions,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QRScanScreen);
