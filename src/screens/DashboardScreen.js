/**
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import NavigationService from '../lib/NavigationService';
import DashActionButton from '../views/DashActionButton';
import BottomCardModal from '../views/BottomCardModal';
// import SettingsButton from '../views/SettingsButton';
import GatewayLogo from '../views/GatewayLogo';
import {scanQRSvg, createNodeSvg, myNodesSvg} from '../svg';
import {ActionCreators} from '../actions';

// TODO M - SettingsButton

const DashboardScreen = ({
  route: {
    params: {tabIndex},
  },
}: props) => {
  const scanQR = () => {
    NavigationService.replace({
      name: 'QRScanner',
      params: {
        tabIndex,
      },
    });
  };

  const createNode = () => {
    // TODO M - Bring up screen for making a node
    Alert.alert('New Node', 'Coming Soon 😉');
  };

  const showMyNodes = () => {
    // TODO M - Navigate to "My Nodes" screen
    Alert.alert('My Nodes', 'Coming Soon 😉');
  };

  return (
    <BottomCardModal dashboard>
      <View style={[styles.horizontalContainer, styles.header]}>
        <GatewayLogo />
      </View>
      <View style={[styles.horizontalContainer, styles.footer]}>
        <DashActionButton
          svgXmlData={scanQRSvg}
          title="Scan QR"
          onPress={scanQR}
        />
        <DashActionButton
          svgXmlData={createNodeSvg}
          title="New Node"
          type="textColor"
          opacity={0.25}
          onPress={createNode}
        />
        <DashActionButton
          svgXmlData={myNodesSvg}
          title="My Nodes"
          type="textColor"
          opacity={0.25}
          onPress={showMyNodes}
        />
      </View>
    </BottomCardModal>
  );
};

const styles = StyleSheet.create({
  horizontalContainer: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingTop: 15,
  },
  footer: {
    justifyContent: 'space-around',
    paddingVertical: 15,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardScreen);
