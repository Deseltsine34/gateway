const rn_bridge = require('rn-bridge');
const fs = require('fs');

var Vibrant = require('node-vibrant');
var ColorThief = require('./color-thief');

rn_bridge.channel.on('get-colors', async msg => {
  // NOTE - params should always at least have a unique ID for a callback to RN
  console.log('[NODE] Started getting palette...');
  let {path, requestId} = msg;
  fs.readFile(path, (error, data) => {
    console.log('[NODE] Read palette image file...');
    if (error || data === null || data === undefined) {
      console.log('[NODE] Image info error:', error);
      rn_bridge.channel.post(requestId, {error});
    }
    if (data && data !== null && data !== undefined) {
      Vibrant.from(data).getPalette((vibrantError, palette) => {
        console.log('[NODE] Got palette!');
        if (vibrantError || palette === null || palette === undefined) {
          console.log('[NODE] Color palette error:', vibrantError);
          rn_bridge.channel.post(requestId, {error: vibrantError});
        }
        if (palette && palette !== null && palette !== undefined) {
          console.log('[NODE] Palette:', palette);
          let swatches = [];
          // Possible: Vibrant, Muted, DarkVibrant, DarkMuted, LightVibrant, LightMuted
          for (var key in palette) {
            const swatch = palette[key];
            if (swatch !== undefined && swatch !== null) {
              swatches.push({
                type: key,
                population: swatch.getPopulation(),
                rgb: swatch.getRgb(),
              });
            }
          }
          if (swatches.length === 0) {
            rn_bridge.channel.post(requestId, {error: 'No swatch results.'});
          } else {
            let result = {};
            ColorThief.getColor(data)
              .then(bgColor => {
                console.log('[NODE] Palette bgColor:', bgColor);
                result.backgroundColor = `rgba(${bgColor[0]},${bgColor[1]},${
                  bgColor[2]
                },1.0)`;
                swatches = swatches.sort((a, b) => {
                  return b.population - a.population;
                });
                const txt = swatches.shift();
                result.textColor = `rgba(${txt.rgb[0]},${txt.rgb[1]},${
                  txt.rgb[2]
                },1.0)`;
                console.log('[NODE] Palette textColor:', result.textColor);
                // result.textColor =
                //   palette[txt.type].getBodyTextColor() === '#000'
                //     ? 'rgba(0,0,0,1.0)'
                //     : 'rgba(255,255,255,1.0)';
                if (txt.type === 'Vibrant') {
                  console.log('[NODE] Palette bg is vibrant...');
                  // TODO - Get highlightColor based on the remaining deltas
                  const vibe = swatches.filter(
                    obj => obj.type === 'DarkVibrant',
                  )[0];
                  swatches = swatches.filter(obj => obj.type !== 'DarkVibrant');
                  result.highlightColor = `rgba(${vibe.rgb[0]},${vibe.rgb[1]},${
                    vibe.rgb[2]
                  },1.0)`;
                } else {
                  console.log('[NODE] Palette bg is NOT vibrant...');
                  const vibe = swatches.filter(
                    obj => obj.type === 'Vibrant',
                  )[0];
                  swatches = swatches.filter(obj => obj.type !== 'Vibrant');
                  result.highlightColor = `rgba(${vibe.rgb[0]},${vibe.rgb[1]},${
                    vibe.rgb[2]
                  },1.0)`;
                }
                rn_bridge.channel.post(requestId, {result});
              })
              .catch(thiefError => {
                rn_bridge.channel.post(requestId, {error: thiefError});
              });
          }
        }
      });
    }
  });
});
