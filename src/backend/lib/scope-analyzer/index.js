/* eslint-disable no-redeclare */
var assert = require('assert');
const acornWalk = require('acorn-walk');
var Symbol = require('es6-symbol');
var getAssignedIdentifiers = require('get-assigned-identifiers');
var isFunction = require('estree-is-function');
var Binding = require('scope-analyzer/binding');
var Scope = require('scope-analyzer/scope');

var kScope = Symbol('scope');

exports.createScope = createScope;
exports.visitScope = visitScope;
exports.visitBinding = visitBinding;
exports.crawl = crawl;
exports.analyze = crawl; // old name
exports.clear = clear;
exports.deleteScope = deleteScope;
exports.nearestScope = getNearestScope;
exports.scope = getScope;
exports.getBindings = getBindings;
exports.getBindingNames = getBindingNames;
exports.kScope = kScope;

exports.matchNodes = matchNodes;
exports.getParentAssignment = getParentAssignment;
exports.isReferenceNode = isReferenceNode;
exports.isAssignmentNode = isAssignmentNode;
exports.getAllAssignedValues = getAllAssignedValues;
exports.filterStringValues = filterStringValues;
exports.getAllStringValues = getAllStringValues;

// create a new scope at a node.
function createScope(node, bindings) {
  assert.ok(
    typeof node === 'object' && node && typeof node.type === 'string',
    'scope-analyzer: createScope: node must be an ast node',
  );
  if (!node[kScope]) {
    var parent = getParentScope(node);
    node[kScope] = new Scope(parent);
  }
  if (bindings) {
    for (var i = 0; i < bindings.length; i++) {
      node[kScope].define(new Binding(bindings[i]));
    }
  }
  return node[kScope];
}

// Separate scope and binding registration steps, for post-order tree walkers.
// Those will typically walk the scope-defining node _after_ the bindings that belong to that scope,
// so they need to do it in two steps in order to define scopes first.
function visitScope(node) {
  assert.ok(
    typeof node === 'object' && node && typeof node.type === 'string',
    'scope-analyzer: visitScope: node must be an ast node',
  );
  registerScopeBindings(node);
}
function visitBinding(node, path, ASTs) {
  assert.ok(
    typeof node === 'object' && node && typeof node.type === 'string',
    'scope-analyzer: visitBinding: node must be an ast node',
  );
  if (isVariable(node)) {
    registerReference(node, path, ASTs);
  }
}

function crawl(path, ASTs) {
  assert.ok(
    ASTs &&
      ASTs[path] &&
      ASTs[path].nodes &&
      typeof ASTs[path].nodes === 'object' &&
      typeof ASTs[path].nodes.type === 'string',
    'scope-analyzer: crawl: ast must be an ast node',
  );
  acornWalk.full(ASTs[path].nodes, function (node) {
    visitScope(node);
  });
  acornWalk.full(ASTs[path].nodes, function (node) {
    visitBinding(node, path, ASTs);
  });

  return ASTs[path];
}

function clear(ast) {
  assert.ok(
    typeof ast === 'object' && ast && typeof ast.type === 'string',
    'scope-analyzer: clear: ast must be an ast node',
  );
  acornWalk.full(ast, deleteScope);
}

function deleteScope(node) {
  if (node) {
    delete node[kScope];
  }
}

function getScope(node) {
  if (node && node[kScope]) {
    return node[kScope];
  }
  return null;
}

function getBindings(identifier, path, ASTs) {
  assert.strictEqual(
    typeof identifier,
    'object',
    'scope-analyzer: getBindings: identifier must be a node',
  );
  assert.ok(
    identifier.type === 'Identifier' || identifier.type === 'Literal',
    'scope-analyzer: getBindings: identifier must be an Identifier or Literal node',
  );

  var scopeNode = getDeclaredScope(identifier);
  if (!scopeNode) {
    return null;
  }
  var scope = getScope(scopeNode);
  if (!scope) {
    return null;
  }
  let names =
    identifier.bindingNames || getBindingNames(identifier, path, ASTs);
  let bindings = [];
  for (var i = 0; i < names.length; i++) {
    var name = names[0];
    var nameBindings = scope.getBindings(name) || [
      scope.undeclaredBindings.get(name),
    ];
    for (var j = 0; j < nameBindings.length; j++) {
      let binding = nameBindings[i];
      if (binding) {
        bindings.push(binding);
      }
    }
  }
  return bindings;
}

function registerScopeBindings(node) {
  if (node.type === 'Program') {
    createScope(node);
  }
  if (node.type === 'VariableDeclaration') {
    var scopeNode = getNearestScope(node, node.kind !== 'var');
    var scope = createScope(scopeNode);
    node.declarations.forEach(function (decl) {
      getAssignedIdentifiers(decl.id).forEach(function (id) {
        scope.define(new Binding(id.name, id));
      });
    });
  }
  if (node.type === 'ClassDeclaration') {
    var scopeNode = getNearestScope(node);
    var scope = createScope(scopeNode);
    if (node.id && node.id.type === 'Identifier') {
      scope.define(new Binding(node.id.name, node.id));
    }
  }
  if (node.type === 'FunctionDeclaration') {
    var scopeNode = getNearestScope(node, false);
    var scope = createScope(scopeNode);
    if (node.id && node.id.type === 'Identifier') {
      scope.define(new Binding(node.id.name, node.id));
    }
  }
  if (isFunction(node)) {
    var scope = createScope(node);
    node.params.forEach(function (param) {
      getAssignedIdentifiers(param).forEach(function (id) {
        scope.define(new Binding(id.name, id));
      });
    });
  }
  if (node.type === 'FunctionExpression' || node.type === 'ClassExpression') {
    var scope = createScope(node);
    if (node.id && node.id.type === 'Identifier') {
      scope.define(new Binding(node.id.name, node.id));
    }
  }
  if (node.type === 'ImportDeclaration') {
    var scopeNode = getNearestScope(node, false);
    var scope = createScope(scopeNode);
    getAssignedIdentifiers(node).forEach(function (id) {
      scope.define(new Binding(id.name, id));
    });
  }
  if (node.type === 'CatchClause') {
    var scope = createScope(node);
    if (node.param) {
      getAssignedIdentifiers(node.param).forEach(function (id) {
        scope.define(new Binding(id.name, id));
      });
    }
  }
}

function getParentScope(node) {
  var parent = node;
  while (parent.parent) {
    parent = parent.parent;
    if (getScope(parent)) {
      return getScope(parent);
    }
  }
}

// Get the scope that a declaration will be declared in
function getNearestScope(node, blockScope) {
  var parent = node;
  while (parent.parent) {
    parent = parent.parent;
    if (isFunction(parent)) {
      break;
    }
    if (blockScope && parent.type === 'BlockStatement') {
      break;
    }
    if (parent.type === 'Program') {
      break;
    }
  }
  return parent;
}

// Get the scope that this identifier has been declared in
function getDeclaredScope(id) {
  var parent = id;
  // Jump over one parent if this is a function's name--the variables
  // and parameters _inside_ the function are attached to the FunctionDeclaration
  // so if a variable inside the function has the same name as the function,
  // they will conflict.
  // Here we jump out of the FunctionDeclaration so we can start by looking at the
  // surrounding scope
  if (id.parent.type === 'FunctionDeclaration' && id.parent.id === id) {
    parent = id.parent;
  }
  while (parent.parent) {
    parent = parent.parent;
    if (parent[kScope] && parent[kScope].has(id.name)) {
      break;
    }
  }
  return parent;
}

const matchNodes = (node1, node2) => {
  if (
    node1.type === node2.type &&
    node1.start === node2.start &&
    node1.end === node2.end
  ) {
    return true;
  }
  return false;
};

const isAssignmentNode = (node) => {
  if (
    (node.type === 'AssignmentExpression' &&
      (node.operator === '=' ||
        node.operator === '||=' ||
        node.operator === '&&=' ||
        node.operator === '??=')) ||
    node.type === 'VariableDeclarator' ||
    (node.type === 'Property' && node.kind === 'init')
  ) {
    return true;
  }
  return false;
};

const getPropName = (node) => {
  let name = '';
  if (
    !node.parent ||
    (node.parent.type !== 'Property' &&
      node.parent.type !== 'ObjectExpression' &&
      node.parent.type !== 'ArrayExpression')
  ) {
    return name;
  }
  let previous = node;
  let remaining = [getPropName.parent];
  while (remaining.length) {
    let current = remaining.pop();
    if (current.type === 'Property') {
      let keyName = current.key.name || current.key.value;
      name = keyName + name;
    } else if (current.type === 'ArrayExpression') {
      let index = current.elements.findIndex((el) => {
        if (matchNodes(previous, el)) {
          return true;
        }
        return false;
      });
      if (index > -1) {
        name = index + name;
      }
    } else if (current.type !== 'ObjectExpression') {
      break;
    }
    remaining.push(current.parent);
    previous = current;
  }
  return name;
};

const findValuesFromPropName = (node, name) => {
  let values = [];
  let nameCheck = '';
  if (node.type !== 'ObjectPattern' || node.type !== 'ArrayPattern') {
    return values;
  }
  let remaining = [node];
  while (remaining.length) {
    let current = remaining.pop();
    if (nameCheck === name) {
      let identifiers = getAssignedIdentifiers(current);
      values.push(...identifiers);
    } else if (current.type === 'ObjectPattern') {
      let propIndex = current.properties.findIndex((prop) => {
        let keyName = prop.key.name || prop.key.value;
        let newName = nameCheck + keyName;
        if (name.indexOf(newName) === 0) {
          nameCheck = newName;
          return true;
        }
        return false;
      });
      if (propIndex === -1) {
        break;
      }
      let next = current.properties[propIndex].value;
      remaining.push(next);
    } else if (current.type === 'ArrayPattern') {
      let elIndex = current.elements.findIndex((el, index) => {
        let newName = nameCheck + index;
        if (name.indexOf(newName) === 0) {
          nameCheck = newName;
          return true;
        }
        return false;
      });
      if (elIndex === -1) {
        break;
      }
      let next = current.elements[elIndex];
      remaining.push(next);
    } else {
      break;
    }
  }
  return values;
};

const getParentAssignment = (node) => {
  var returnVal = {receivers: []};
  var remaining = [node];
  let rooted = false;
  let giverPropName = '';
  while (remaining.length) {
    let current = remaining.pop();
    if (
      !rooted &&
      current.parent &&
      (current.type === 'AwaitExpression' ||
        current.type === 'ExpressionStatement' ||
        current.type === 'ObjectExpression' ||
        current.type === 'ArrayExpression' ||
        current.type === 'ObjectPattern' ||
        current.type === 'ArrayPattern' ||
        current.type === 'Property' ||
        isReferenceNode(current) ||
        isAssignmentNode(current.parent))
    ) {
      remaining.push(current.parent);
    } else if (isAssignmentNode(current)) {
      if (!rooted) {
        rooted = true;
      }
      var receiver = current.left || current.id || current.key;
      var giver = current.right || current.init || current.value;
      returnVal.receivers.push(receiver);

      if (isAssignmentNode(giver)) {
        remaining.push(giver);
      } else {
        // End of Assignment Chain
        returnVal.giver = giver;
        giverPropName = getPropName(giver);
        if (matchNodes(node, giver) || giverPropName !== '') {
          returnVal.isGiver = true;
        }
      }
    }
  }
  if (returnVal.giver) {
    var finalReceivers = [];
    for (var i = 0; i < returnVal.receivers.length; i++) {
      let receiverNode = returnVal.receivers[i];
      if (receiverNode.type === 'Identifier') {
        var bindingNames = getBindingNames(receiverNode);
        bindingNames = bindingNames.map((name) => name + giverPropName);
        receiverNode.bindingNames = bindingNames;
        finalReceivers.push(receiverNode);
      } else {
        let relevantValues =
          giverPropName === ''
            ? getAssignedIdentifiers(receiverNode)
            : findValuesFromPropName(receiverNode, giverPropName);
        finalReceivers.push(...relevantValues);
      }
    }
    returnVal.receivers = finalReceivers;
  }
  // NOT NECESSARY: Solution should support 'var foo = bar = baz = API_CALL' where 'bar' is passed and it's understood that 'foo' and 'baz' are equivalent
  return undefined;
};

const isReferenceNode = (node) => {
  let current = node;
  while (current.parent) {
    if (
      current.type === 'AwaitExpression' ||
      current.type === 'ExpressionStatement'
    ) {
      current = current.parent;
    } else {
      break;
    }
  }
  if (
    current.type === 'Identifier' ||
    current.type === 'MemberExpression' ||
    current.type === 'CallExpression' ||
    current.type === 'ChainExpression'
  ) {
    return true;
  }
  return false;
};

// TODO H - Change to 'getRootBindings'
const getRootIdentifier = (node) => {
  let identifier;
  let remaining = [node];
  while (remaining.length) {
    let current = remaining.pop();
    if (
      current.type === 'VariableDeclarator' ||
      current.type === 'FunctionDeclaration'
    ) {
      remaining.push(current.id);
    } else if (current.type === 'Identifier') {
      identifier = current;
    } else if (current.type === 'AwaitExpression') {
      remaining.push(current.argument);
    } else if (
      current.type === 'ChainExpression' ||
      current.type === 'ExpressionStatement'
    ) {
      remaining.push(current.expression);
    } else if (current.type === 'CallExpression') {
      remaining.push(current.callee);
    } else if (current.type === 'MemberExpression') {
      if (isReferenceNode(current.object)) {
        remaining.push(current.object);
      } else {
        let assignedIds = getAssignedIdentifiers(current.object);
        if (assignedIds.length) {
          identifier = assignedIds;
        } else {
          remaining.push(current.property);
        }
      }
    } else {
      let assignedIds = getAssignedIdentifiers(current);
      if (assignedIds.length) {
        identifier = assignedIds;
      }
    }
  }
  return identifier;
};

// THINKING: So in any case that 'var foo = [API_NODE]' is declared, 'API_NODE' would already be caught.
//           When evaluated, 'getParentAssignment' would force the bindingName for 'foo' to be 'foo0' anyway, so all of those cases would be covered.
//  TODO H - ISSUE: But in the case of 'var bar = foo', it wouldn't come up unless 'getParentAssignment' also provided 'foo' with the caveat that it should only pass the special case for '0' to assigned variables like 'bar'
//           SOLUTION: When assignments are evaluated, search the binding for the root parent. So for 'foo.bar.baz.NODE', it would also search for the root binding of 'foo' just to find assignments where passing '0' is required
//                     Would 'var bop = foo.bar' be caught if you just searched for 'foo'? Yes! Because searching the 'foo' binding would find that assignment and tell it to modify any cases of 'bop.bar.baz.NODE'
//           IMPORTANT DETAIL: You could not ignore cases like 'var bop = foo.bar' after using it once because it's a vehicle for catching more potential cases like this.
// Would 'foo.bar.baz.NODE.other.prop' be covered? YES. Why:
// 'foo.bar.baz.NODE' would be caught, and then it would await the props in front of it in obvious cases.
// But for 'var bop = foo.bar.baz.NODE.other', it would say 'bop.bar.baz.NODE' is the binding of interest, which would catch any sub-props like '.other'

const getAllAssignedValues = (node, path, ASTs, deep) => {
  var values = [];
  var bindings = getBindings(node, path, ASTs);
  while (bindings.length) {
    let current = bindings.pop();
    if (current.getReferences) {
      bindings.push(...current.getReferences());
    } else {
      let assignment = getParentAssignment(current);
      if (assignment && !assignment.isGiver) {
        if (deep && isReferenceNode(assignment.giver)) {
          var newBindings = getBindings(assignment.giver, path, ASTs);
          if (newBindings.length) {
            bindings.push(...newBindings);
          } else {
            values.push(assignment.giver);
          }
        } else {
          values.push(assignment.giver);
        }
      }
    }
  }
  return values;
};

const filterStringValues = (values) => {
  // TODO M - See note for cases aside from regular Literal and reference to literal
  return values.filter((obj) => {
    if (obj.type === 'Literal') {
      return true;
    }
    return false;
  });
};

const getAllStringValues = (node, path, ASTs, deep) => {
  if (node.type === 'Literal') {
    return [node];
  }
  let values = getAllAssignedValues(node, path, ASTs, deep);
  return filterStringValues(values);
};

const getBindingNames = (node, path, ASTs) => {
  if (node.bindingNames) {
    return node.bindingNames;
  }

  // NOTE: Needed for 'getAllAssignedValues'
  // TODO H - Support ObjectExpression, Property, ArrayExpression
  //        - Properties should be recursively walked outward to also return the compound assignment-based bindingName (in addition to regular identifier)
  //        - (i.e. Passing 'baz' 'var foo = {bar: {baz: 0}}' should return the names ['foobarbaz'])
  //        - Literals that are in Property assignments should be treated like Identifiers
  // TODO H - Support ArrayExpression
  //        - Index of element should be found in its parent and a binding is returned only if it is assigned to anything
  // ^^^ These two may be something that can be done in a function shared by this and 'getParentAssignment'

  // NOTE: I don't think you need this here anymore
  // let node = _node;
  // while (node.parent) {
  //   if (isReferenceNode(node.parent)) {
  //     node = node.parent;
  //   } else {
  //     break;
  //   }
  // }

  let names = [''];
  let remaining = [node];
  // TODO H - Make sure bindings contained in computed MemberExpressions aren't going beyond their bounds
  //        - Example: [X] 'foo.bar[baz.bop].beep' with 'bop' being the initial target should produce 'bazbop'.
  //                   [X] Passing 'beep' should produce 'foobar___beep' where '___' is all possible values for 'baz.bop'.
  //                   [ ] If 'baz.bop' equals 'far', then 'foo.bar[baz.bop]' should appear in references to 'foobarfar'
  while (remaining.length) {
    let current = remaining.pop();
    if (current.type === 'AwaitExpression') {
      remaining.push(current.argument);
    } else if (
      current.type === 'ChainExpression' ||
      current.type === 'ExpressionStatement'
    ) {
      remaining.push(current.expression);
    } else if (current.type === 'CallExpression') {
      remaining.push(current.callee);
    } else if (current.type === 'MemberExpression') {
      if (current.property.value) {
        names = names.map((name) => current.property.value + name);
      } else if (!current.computed) {
        names = names.map((name) => current.property.name + name);
      } else {
        let keyVals = getAllStringValues(current.property, path, ASTs, true);
        let newNames = [];
        for (var i = 0; i < keyVals.length; i++) {
          let val = keyVals[i];
          if (val.value) {
            val = val.value;
          }
          let valNames = names.map((name) => val + name);
          newNames.push(...valNames);
        }
        if (newNames.length > names.length) {
          names = newNames;
        }
      }
      if (
        current.object.type === 'Identifier' ||
        current.object.type === 'AwaitExpression' ||
        current.object.type === 'ChainExpression' ||
        current.object.type === 'ExpressionStatement' ||
        current.object.type === 'CallExpression' ||
        current.object.type === 'MemberExpression'
      ) {
        remaining.push(current.object);
      } else {
        names = names.map((name) => current.object.type + name);
      }
    } else {
      // Identifiers and non-walkables oddities like 'ThisExpression'
      let currentName = current.name || current.value || current.type;
      names = names.map((name) => currentName + name);

      if (
        current.parent &&
        current.parent.type === 'MemberExpression' &&
        matchNodes(current, current.parent.property)
      ) {
        remaining.push(current.parent.object);
      }

      // NOTE: Probably not necessary because tracking assignment values happens elsewhere
      // let assignment = getParentAssignment(current);
      // if (assignment) {
      //   remaining.push(assignment.node);
      // }
    }
  }

  if (names.length === 1 && names[0] === '') {
    return [];
  }

  return names;
};

function registerReference(node, path, ASTs) {
  var scopeNode = getDeclaredScope(node);
  var scope = getScope(scopeNode);
  var names = getBindingNames(node, path, ASTs);
  for (var i = 0; i < names.length; i++) {
    var name = names[i];
    if (scope && scope.has(name)) {
      scope.add(name, node);
    }
    if (scope && !scope.has(name)) {
      scope.addUndeclared(name, node);
    }
  }
}

function isObjectKey(node) {
  return (
    node.type === 'Identifier' &&
    node.parent.type === 'Property' &&
    node.parent.key === node &&
    // a shorthand property may have the ===-same node as both the key and the value.
    // we should detect the value part.
    node.parent.value !== node
  );
}
function isMethodDefinition(node) {
  return (
    node.type === 'Identifier' &&
    node.parent.type === 'MethodDefinition' &&
    node.parent.key === node
  );
}
function isImportName(node) {
  return (
    node.parent.type === 'ImportSpecifier' && node.parent.imported === node
  );
}

function isVariable(node) {
  return (
    // !isObjectKey(node) &&
    !isMethodDefinition(node) &&
    (node.parent.type !== 'MemberExpression' ||
      node.parent.object === node ||
      node.parent.property === node) &&
    !isImportName(node)
  );
}
