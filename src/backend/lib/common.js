const {join} = require('path');
const stream = require('stream');
const assert = require('assert');
const fse = require('fs-extra');
const pump = require('pump');
const match = require('anymatch');
const callMeMaybe = require('call-me-maybe');
const pda2 = require('pauls-dat-api2');
const {DestDirectoryNotEmpty} = require('beaker-error-constants');
const util = require('util');

const {parseAndWriteJS} = require('./parser.js');

function formatError(error) {
  if (typeof error === 'string') {
    return {description: error};
  }
  return {
    code: error.code || error.statusCode || error.errorCode || error.status,
    domain: error.domain || error.name,
    description: error.description || error.message,
    category: error.category,
  };
}

const DEFAULT_IGNORE = ['.dat', '**/.dat', '.git', '**/.git'];
function maybe(cb, p) {
  if (typeof p === 'function') {
    p = p();
  }
  return callMeMaybe(cb, p);
}

var Transform = stream.Transform || require('readable-stream').Transform;

function trimRefPath(value) {
  if (value.indexOf('url') === 0) {
    return value.replace(/url[\n \s]*\([\n \s]*'?"?/, '').trim();
  }
  if (
    value.indexOf('href') === 0 ||
    value.indexOf('src') === 0 ||
    value.indexOf('path') === 0
  ) {
    return value
      .replace(/(href|src|path)[\n \s]*=[\n \s]*['"][\n \s]*/, '')
      .trim();
  }
  if (value.indexOf('](') === 0) {
    let isolatedValue = value.replace(/\][\n \s]*\([\n \s]*/, '');
    if (isolatedValue.search(/.?.?\//) !== 0) {
      isolatedValue = './' + isolatedValue;
    }
    return isolatedValue;
  }
  const jsQuoteMatch = value.match(/['"`]/);
  if (jsQuoteMatch !== null && jsQuoteMatch.length > 0) {
    const jsQuoteString = jsQuoteMatch[0];
    const firstQuoteIndex = value.indexOf(jsQuoteString);
    return value.substr(firstQuoteIndex + 1);
  }
  return value;
}

function RelativeToAbsolute(
  fileType,
  srcPath,
  fsPath,
  writeCb,
  baseFsPath,
  baseUrl,
  apis,
  options,
) {
  // allow use without new
  if (!(this instanceof RelativeToAbsolute)) {
    return new RelativeToAbsolute(options);
  }

  // init Transform
  Transform.call(this, options);

  this.fileType = fileType;
  this.srcPath = srcPath;
  this.fsPath = fsPath;
  this.writeCb = writeCb;
  this.baseFsPath = baseFsPath;
  this.baseUrl = baseUrl;
  this.apis = apis;
}
util.inherits(RelativeToAbsolute, Transform);

RelativeToAbsolute.prototype._transform = function(chunk, enc, cb) {
  const matchRegex = {
    html: /(?:(?:href|src|path)[\n \s]*=[\n \s]*['"][\n \s]*[^'"]+)|(?:url[\n \s]*\([\n \s]*'?"?[^'"\?\)]+)/g,
    css: /url[\n \s]*\([\n \s]*'?"?[^'"\?\)]+/g,
    js: /(?:require\(|from)[\n \s]*['"`]\.?\.?\/[^\n?:*<>'"`\\|]*/g,
    md: /\][\n \s]*\([\n \s]*[^)]+/g,
  }[this.fileType];
  let newChunk = typeof chunk === 'string' ? chunk : chunk.toString();
  let replacements = {};
  let refs = newChunk.match(matchRegex);
  if (refs === null) {
    refs = [];
  } else {
    refs = refs.filter(obj => {
      const rejection =
        /:\/\/|javascript:/.test(obj) ||
        obj.indexOf('#') !== -1 ||
        obj.indexOf('://') !== -1;
      return !rejection;
    });
    for (var i = 0; i < refs.length; i++) {
      let value = refs[i];
      if (
        this.fileType !== 'html' ||
        (this.fileType === 'html' && value.endsWith('.css'))
      ) {
        console.log('[NODE] FS Write pre-trim value:', value);
        if (replacements[value] === undefined) {
          console.log('[NODE] this.fsPath:', this.fsPath);
          console.log('[NODE] this.srcPath:', this.srcPath);
          let absolutePath = this.fsPath;
          let refPath = trimRefPath(value);
          const splitArray = refPath.split('/');
          for (var j = 0; j < splitArray.length; j++) {
            const subPath = splitArray[j];
            if (j === 0 && subPath === '') {
              // '' at index 0 = Start at root folder
              absolutePath = this.baseFsPath;
            } else if (subPath === '..') {
              // '..' = Go back a folder
              absolutePath = absolutePath.substr(
                0,
                absolutePath.lastIndexOf('/'),
              );
            } else if (subPath !== '' && subPath !== '.') {
              absolutePath = absolutePath + '/' + subPath;
            } else if (subPath === '') {
              // Added to keep this consistent with original syntax
              absolutePath = absolutePath + '/';
            }
            // '.' = Current folder, so no action needed
          }
          console.log('[NODE] absolutePath:', absolutePath);
          console.log('[NODE] this.baseFsPath:', this.baseFsPath);
          let relativePath = absolutePath.replace(this.baseFsPath, '');
          // Forces .md files to be replaced with html conversions
          // if (absolutePath.endsWith('.md')) {
          //   absolutePath = absolutePath.replace('.md', '-md.html');
          // }
          this.writeCb(relativePath, absolutePath);
          // Return new relative path (filter out this.fsPath) that can be found with hyper
          let replacementString = value.replace(refPath, absolutePath);
          // Replace
          newChunk = newChunk.split(value).join(replacementString);
          replacements[value] = replacementString;
        }
      }
    }
  }
  newChunk = newChunk.replace(
    /(\b(window)\b(\.|\[['`"])|([^\."`']))\b(location)\b/g,
    '$1fakeLocation',
  );
  // NOTE - Doing JS modification once all JS files have been mapped
  // if (this.fileType === 'js') {
  //   newChunk = modifyNewJsChunk(newChunk, this.apis);
  //   console.log('[NODE] newChunk:', newChunk);
  // }
  this.push(newChunk);
  cb();
};

const modifyCompleteChunk = function(
  chunk,
  fileType,
  srcPath,
  fsPath,
  writeCb,
  baseFsPath,
  baseUrl,
  apis,
) {
  const matchRegex = {
    html: /(?:(?:href|src|path)[\n \s]*=[\n \s]*['"][\n \s]*[^'"]+)|(?:url[\n \s]*\([\n \s]*'?"?[^'"\?\)]+)/g,
    css: /url[\n \s]*\([\n \s]*'?"?[^'"\?\)]+/g,
    js: /(?:require\(|from)[\n \s]*['"`]\.?\.?\/[^\n?:*<>'"`\\|]*/g,
    md: /\][\n \s]*\([\n \s]*[^)]+/g,
  }[fileType];
  let newChunk = typeof chunk === 'string' ? chunk : chunk.toString();
  let replacements = {};
  let refs = newChunk.match(matchRegex);
  if (refs === null) {
    refs = [];
  } else {
    refs = refs.filter(obj => {
      const rejection =
        /:\/\/|javascript:/.test(obj) ||
        obj.indexOf('#') !== -1 ||
        obj.indexOf('://') !== -1;
      return !rejection;
    });
    for (var i = 0; i < refs.length; i++) {
      let value = refs[i];
      if (
        fileType !== 'html' ||
        (fileType === 'html' && value.endsWith('.css'))
      ) {
        console.log('[NODE] FS Write pre-trim value:', value);
        if (replacements[value] === undefined) {
          console.log('[NODE] fsPath:', fsPath);
          console.log('[NODE] srcPath:', srcPath);
          let absolutePath = fsPath;
          let refPath = trimRefPath(value);
          const splitArray = refPath.split('/');
          for (var j = 0; j < splitArray.length; j++) {
            const subPath = splitArray[j];
            if (j === 0 && subPath === '') {
              // '' at index 0 = Start at root folder
              absolutePath = baseFsPath;
            } else if (subPath === '..') {
              // '..' = Go back a folder
              absolutePath = absolutePath.substr(
                0,
                absolutePath.lastIndexOf('/'),
              );
            } else if (subPath !== '' && subPath !== '.') {
              absolutePath = absolutePath + '/' + subPath;
            } else if (subPath === '') {
              // Added to keep this consistent with original syntax
              absolutePath = absolutePath + '/';
            }
            // '.' = Current folder, so no action needed
          }
          console.log('[NODE] absolutePath:', absolutePath);
          console.log('[NODE] baseFsPath:', baseFsPath);
          let relativePath = absolutePath.replace(baseFsPath, '');
          // Forces .md files to be replaced with html conversions
          // if (absolutePath.endsWith('.md')) {
          //   absolutePath = absolutePath.replace('.md', '-md.html');
          // }
          writeCb(relativePath, absolutePath);
          // Return new relative path (filter out fsPath) that can be found with hyper
          let replacementString = value.replace(refPath, absolutePath);
          // Replace
          newChunk = newChunk.split(value).join(replacementString);
          replacements[value] = replacementString;
        }
      }
    }
  }
  newChunk = newChunk.replace(
    /(\b(window)\b(\.|\[['`"])|([^\."`']))\b(location)\b/g,
    '$1fakeLocation',
  );
  // NOTE - Doing JS modification once all JS files have been mapped
  // if (fileType === 'js') {
  //   newChunk = modifyNewJsChunk(newChunk, apis);
  //   console.log('[NODE] newChunk:', newChunk);
  // }
  return newChunk;
};

// TODO M - Generalize this function to be potentially compatible with other libraries, maybe 'exportNodeToFilesystem'?
//        - POSSIBLE IMPLEMENTATIONS: Have this be defined in protocol registry, assume same api for node (i.e. Hyperdrive) as filesystem unless different API specified
//        - BETTER POSSIBILITY: Keep most of this the same, and just use generalized fetch calls for protocol-specific things like reading file/directory data
// TODO M - Write mounts to the base directory related to their own drive's key
//        - This would prevent writing the mount to fs for every site that uses it
const makeFsExport = (apis = {}) => {
  const exportToFilesystem = (opts, cb) => {
    return maybe(cb, async function () {
      assert(opts && typeof opts === 'object', 'opts object is required');

      // core arguments, dstPath and srcArchive
      var srcArchive = opts.srcArchive;
      var dstPath = opts.dstPath;

      assert(
        srcArchive && typeof srcArchive === 'object',
        'srcArchive is required',
      );
      assert(dstPath && typeof dstPath === 'string', 'dstPath is required');

      // parsing
      var modifyQueue = [];
      var jsContent = {};

      // options
      var srcPath = typeof opts.srcPath === 'string' ? opts.srcPath : '/';
      var overwriteExisting = opts.overwriteExisting === true;
      var skipUndownloadedFiles = opts.skipUndownloadedFiles === true;
      var ignore = Array.isArray(opts.ignore) ? opts.ignore : DEFAULT_IGNORE;
      var changes = opts.changes;
      var walkStart = opts.walkStart;
      var writtenDirectories = [];
      var writtenSrcNames = [];
      if (changes) {
        changes = changes
          .filter((change) => {
            if (change.type === 'del') {
              const path = join(dstPath, change.name);
              fse.removeSync(path);
              return false;
            }
            return true;
          })
          .map((change) => {
            if (change.name.indexOf('/') !== 0) {
              change.name = '/' + change.name;
            }
            return change;
          });
      }

      // abort if nonempty and not overwriting existing
      if (!overwriteExisting) {
        let files;
        try {
          files = await fse.readdir(dstPath);
        } catch (e) {
          // target probably doesnt exist, continue and let ensureDirectory handle it
        }
        if (files && files.length > 0) {
          throw new DestDirectoryNotEmpty();
        }
      }

      var seenDrives = new Set();
      const statThenExport = async function(srcPath, dstPath, skipFallback) {
        // apply ignore filter
        if (
          (ignore && match(ignore, srcPath)) ||
          writtenSrcNames.indexOf(srcPath) !== -1
        ) {
          return;
        }

        // Cut off before any insertions
        const srcInsertionIndex = srcPath.indexOf('${');
        if (srcInsertionIndex !== -1) {
          return;
          // srcPath = srcPath.substr(0, srcInsertionIndex);
        }
        const dstInsertionIndex = dstPath.indexOf('${');
        if (dstInsertionIndex !== -1) {
          return;
          // dstPath = dstPath.substr(0, dstInsertionIndex);
        }
        if (srcPath !== '/' && srcPath.endsWith('/')) {
          srcPath = srcPath.substr(0, srcPath.length - 1);
        }

        var srcStat;
        try {
          srcStat = await pda2.stat(srcArchive, srcPath);
        } catch (statError) {
          console.log('[NODE] First statError:', statError);
          // Fallback incase the file extension is just missing
          let srcPathParent = srcPath.substr(0, srcPath.lastIndexOf('/'));
          if (srcPathParent === '') {
            srcPathParent = '/';
          }
          let srcPathAsChild = srcPath.substr(srcPath.lastIndexOf('/') + 1);
          try {
            const parentContents = await pda2.readdir(
              srcArchive,
              srcPathParent,
            );
            console.log('[NODE] parentContents:', parentContents);
            console.log('[NODE] srcPathParent:', srcPathParent);
            console.log('[NODE] old srcPath:', srcPath, ':', srcPathAsChild);
            const foundElement = parentContents.find(element => {
              return (
                element.includes(srcPathAsChild) || element === srcPathAsChild
              );
            });
            console.log('[NODE] foundElement:', foundElement);
            if (foundElement === undefined) {
              console.log(
                '[NODE] Error: Child src with similar name not found.',
              );
              return;
            }
            if (writtenSrcNames.indexOf(srcPathParent) === -1) {
              writtenSrcNames.unshift(srcPathParent);
              writtenDirectories.unshift({
                name: srcPathParent,
                contents: parentContents,
                fs: dstPath.substr(0, dstPath.lastIndexOf('/')),
              });
            }
            srcPath = (srcPathParent + '/' + foundElement).replace('//', '/');
            if (writtenSrcNames.indexOf(srcPath) === -1) {
              console.log('[NODE] New srcPath:', srcPath);
              srcStat = await pda2.stat(srcArchive, srcPath);
            }
          } catch (secondAttemptError) {
            console.log(
              '[NODE] File export stat error:',
              secondAttemptError,
              skipFallback,
            );
            // let promises = [];
            // if (!skipFallback) {
            //   const filteredDirectories = writtenDirectories.filter(
            //     dir => dir.name !== srcPathParent,
            //   );
            //   for (var i = 0; i < filteredDirectories.length; i++) {
            //     const dirName = filteredDirectories[i].name;
            //     const dirContents = filteredDirectories[i].contents;
            //     const dirFsPath = filteredDirectories[i].fs;
            //     const foundElements = dirContents.filter(element =>
            //       element.includes(srcPathAsChild),
            //     );
            //     if (foundElements.length > 0) {
            //       foundElements.map(foundElement => {
            //         const potentialSrcPath = (
            //           dirName +
            //           '/' +
            //           foundElement
            //         ).replace('//', '/');
            //         if (writtenSrcNames.indexOf(potentialSrcPath) === -1) {
            //           writtenSrcNames.push(srcPath);
            //           const potentialDstPath = (
            //             dirFsPath +
            //             '/' +
            //             foundElement
            //           ).replace('//', '/');
            //           console.log(
            //             '[NODE] A potential src to write:',
            //             potentialSrcPath,
            //             'to path:',
            //             potentialDstPath,
            //           );
            //           promises.push(
            //             statThenExport(potentialSrcPath, potentialDstPath, true),
            //           );
            //         }
            //       });
            //     }
            //   }
            // }
            // // NOTE - We write all possible candidates because it's better to write a little too much than to have a missing asset
            // await Promise.allSettled(promises);
          }
        }

        if (!srcStat) {
          return;
        }

        if (srcStat.mount && srcStat.mount.key) {
          // abort mount cycles
          let key = srcStat.mount.key.toString('hex');
          if (seenDrives.has(key)) {
            return;
          }
          seenDrives.add(key);
        }

        // export by type
        if (srcStat.isFile()) {
          await exportFile(srcPath, srcStat, dstPath);
        } else if (srcStat.isDirectory()) {
          await exportDirectory(srcPath, dstPath);
        }
        return;
      };

      const exportFile = async function(srcPath, srcStat, dstPath) {
        // skip undownloaded files
        if (
          skipUndownloadedFiles &&
          srcStat.downloaded < srcStat.blocks &&
          writtenSrcNames.indexOf(srcPath) !== -1
        ) {
          return;
        }
        writtenSrcNames.push(srcPath);

        // HACK - MD Files
        if (srcPath.endsWith('.md') && dstPath.endsWith('-md.html')) {
          dstPath = dstPath.replace('-md.html', '.md');
        }

        // fetch dest stats
        var dstFileStats = null;
        try {
          dstFileStats = await fse.stat(dstPath);
        } catch (e) {}

        // track the stats
        stats.fileCount++;
        stats.totalSize += srcStat.size || 0;
        if (dstFileStats) {
          if (dstFileStats.isDirectory()) {
            // delete the directory-tree
            await fse.remove(dstPath);
            stats.addedFiles.push(dstPath);
          } else {
            stats.updatedFiles.push(dstPath);
          }
        } else {
          stats.addedFiles.push(dstPath);
        }

        // make sure the parent folder exists
        await fse.ensureDir(dstPath.substr(0, dstPath.lastIndexOf('/')));

        // write the file
        return new Promise((resolve, reject) => {
          if (
            changes &&
            changes.findIndex(element => element.name === srcPath) === -1
          ) {
            resolve();
          } else {
            var relativeToAbsolute;
            let refWrites = [];
            var writePath = join(opts.baseFsPath, srcPath);
            let fsParentPath = writePath.substr(0, writePath.lastIndexOf('/'));
            // if (opts.treatLikeRoot && fsParentPath.endsWith(opts.treatLikeRoot)) {
            //   const exceptionIndex = fsParentPath.lastIndexOf(opts.treatLikeRoot);
            //   fsParentPath = fsParentPath.substr(0, exceptionIndex);
            // }
            //
            // const transformOpts = [
            //   srcPath,
            //   fsParentPath,
            //   writeCb,
            //   opts.baseFsPath,
            //   opts.baseUrl,
            //   apis,
            // ];
            if (srcPath.endsWith('.html')) {
              relativeToAbsolute = 'html';
              // relativeToAbsolute = new RelativeToAbsolute(
              //   'html',
              //   ...transformOpts,
              // );
            }
            if (srcPath.endsWith('.css')) {
              relativeToAbsolute = 'css';
              // relativeToAbsolute = new RelativeToAbsolute(
              //   'css',
              //   ...transformOpts,
              // );
            }
            if (srcPath.endsWith('.js')) {
              relativeToAbsolute = 'js';
              // relativeToAbsolute = new RelativeToAbsolute(
              //   'js',
              //   ...transformOpts,
              // );
            }
            if (srcPath.endsWith('.md')) {
              relativeToAbsolute = 'md';
              // relativeToAbsolute = new RelativeToAbsolute(
              //   'md',
              //   ...transformOpts,
              // );
            }
            const writeCb = walkStart
              ? (srcRefPath, refDstPath) => {
                  if (!(ignore && match(ignore, srcRefPath))) {
                    if (
                      relativeToAbsolute === 'js' &&
                      opts.transformRelativePaths
                    ) {
                      modifyQueue.unshift(refDstPath);
                    }
                    if (writtenSrcNames.indexOf(srcRefPath) === -1) {
                      console.log('[NODE] Write Callback:', srcRefPath);
                      refWrites.push(statThenExport(srcRefPath, refDstPath));
                    }
                  }
                }
              : () => {};
            if (relativeToAbsolute && opts.transformRelativePaths) {
              if (relativeToAbsolute === 'js') {
                modifyQueue.unshift(dstPath);
              }
              console.log('[NODE] Transforming this file:', srcPath);
              const srcStream = srcArchive.createReadStream(srcPath);
              let fullChunk = '';
              srcStream.on('data', chunk => {
                let newChunk =
                  typeof chunk === 'string' ? chunk : chunk.toString();
                fullChunk += newChunk;
              });
              srcStream.on('end', () => {
                const absolutePathChunk = modifyCompleteChunk(
                  fullChunk,
                  relativeToAbsolute,
                  srcPath,
                  fsParentPath,
                  writeCb,
                  opts.baseFsPath,
                  opts.baseUrl,
                  apis,
                );
                jsContent[dstPath] = absolutePathChunk;
                if (relativeToAbsolute === 'js') {
                  resolve();
                } else {
                  var writeStream = fse.createWriteStream(dstPath);
                  writeStream.write(absolutePathChunk, 'utf8');
                  writeStream.on('finish', function() {
                    Promise.allSettled(refWrites).then(results => {
                      // HACK - MD Files
                      if (srcPath.endsWith('.md')) {
                        const mdString = fse.readFileSync(dstPath, 'utf8');
                        const htmlString = global.gateway.markdownToHtml(
                          mdString,
                        );
                        fse.writeFileSync(
                          dstPath.replace('.md', '-md.html'),
                          htmlString,
                        );
                      }
                      return resolve();
                    });
                  });
                  writeStream.on('error', function(err) {
                    console.log('[NODE] Transformative pump error:', err);
                    return resolve();
                  });
                  writeStream.end();
                }
              });
              // pump(
              //   srcArchive.createReadStream(srcPath),
              //   relativeToAbsolute,
              //   fse.createWriteStream(dstPath),
              //   err => {
              //     if (err) {
              //       console.log('[NODE] Transformative pump error:', err);
              //       return resolve();
              //     } else {
              //       Promise.allSettled(refWrites).then(results => {
              //         // HACK - MD Files
              //         if (srcPath.endsWith('.md')) {
              //           const mdString = fse.readFileSync(dstPath, 'utf8');
              //           const htmlString = global.gateway.markdownToHtml(
              //             mdString,
              //           );
              //           fse.writeFileSync(
              //             dstPath.replace('.md', '-md.html'),
              //             htmlString,
              //           );
              //         }
              //         resolve();
              //         return;
              //       });
              //     }
              //   },
              // );
            } else {
              console.log('[NODE] NOT transforming this file:', srcPath);
              pump(
                srcArchive.createReadStream(srcPath),
                fse.createWriteStream(dstPath),
                err => {
                  if (err) {
                    console.log('[NODE] Pump error:', err);
                  } else {
                    resolve();
                  }
                  return resolve();
                },
              );
            }
          }
        });
      };

      const exportDirectory = async function(srcPath, dstPath) {
        // make sure the destination folder exists
        await fse.ensureDir(dstPath);

        // list the directory
        var fileNames = await pda2.readdir(srcArchive, srcPath);
        writtenSrcNames.unshift(srcPath);
        writtenDirectories.unshift({
          name: srcPath,
          contents: fileNames,
          fs: dstPath,
        });

        // recurse into each
        var promises = [];
        fileNames.map(name => {
          if (name.endsWith('.js') || name.endsWith('.css')) {
            promises.push(
              statThenExport(join(srcPath, name), join(dstPath, name)),
            );
          }
        });
        return Promise.allSettled(promises);
      };

      // recursively export
      var stats = {
        addedFiles: [],
        updatedFiles: [],
        skipCount: 0,
        fileCount: 0,
        totalSize: 0,
      };
      // HACK - MD Files
      if (dstPath.endsWith('.md') && opts.transformRelativePaths) {
        dstPath = dstPath.replace('.md', '-md.html');
      }
      await statThenExport(srcPath, dstPath);
      if (modifyQueue.length > 0) {
        await parseAndWriteJS(apis, modifyQueue, jsContent);
      }
      return stats;
    });
  };
  return exportToFilesystem;
};

module.exports = {
  formatError,
  makeFsExport,
};
