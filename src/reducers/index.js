import {combineReducers} from 'redux';

import * as common from './common';
import * as browser from './browser';
import * as protocols from './protocols';

const reducer = combineReducers({
  ...common,
  ...browser,
  ...protocols,
});

export default reducer;
