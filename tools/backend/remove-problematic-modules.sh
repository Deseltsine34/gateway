#!/bin/bash

# Any copyright is dedicated to the Public Domain.
# http://creativecommons.org/publicdomain/zero/1.0/

set -eEu -o pipefail
shopt -s extdebug
IFS=$'\n\t'
trap 'onFailure $?' ERR

function onFailure() {
  echo "Unhandled script error $1 at ${BASH_SOURCE[0]}:${BASH_LINENO[0]}" >&2
  exit 1
}

# rm -rf ./nodejs-assets/nodejs-project/node_modules/sodium-native;
# rm -rf ./nodejs-assets/nodejs-project/node_modules/blake2b-universal/node_modules/sodium-native;
# rm -rf ./nodejs-assets/nodejs-project/node_modules/derive-key/node_modules/sodium-native;
# rm -rf ./nodejs-assets/nodejs-project/node_modules/hypercore-crypto/node_modules/sodium-native;
find ./nodejs-assets/nodejs-project/node_modules -type d -name "sodium-native" -exec rm -rf {} \;
