#!/bin/bash

# Any copyright is dedicated to the Public Domain.
# http://creativecommons.org/publicdomain/zero/1.0/

set -eEu -o pipefail
shopt -s extdebug
IFS=$'\n\t'
trap 'onFailure $?' ERR

function onFailure() {
  echo "Unhandled script error $1 at ${BASH_SOURCE[0]}:${BASH_LINENO[0]}" >&2
  exit 1
}

gawk -i inplace '/〇/{c++;if(c>=2 && c<5){gsub("〇","OO");}}1' nodejs-assets/nodejs-project/index.js
gawk -i inplace '/search: \/.+\/,/{gsub("search: /","search: new RegExp(\"");gsub("/,","\"),");}1' nodejs-assets/nodejs-project/index.js
