#!/usr/bin/env node
/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */
const fs = require('fs');
const path = require('path');

const basePath = path.resolve(
  './nodejs-assets/nodejs-project/documents_modules',
);
let regeneratorJsonPath = path.join(
  basePath,
  '/node_modules/regenerator/package.json',
);

function recursivelyGetFiles(dir, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
    if (err) {
      return done(err);
    }
    var pending = list.length;
    if (!pending) {
      return done(null, results);
    }
    list.forEach(function(file) {
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          recursivelyGetFiles(file, function(err, res) {
            const fullRes = res.map(resPath => {
              return path.resolve(file, resPath);
            });
            results = results.concat(fullRes);
            if (!--pending) {
              done(null, results);
            }
          });
        } else {
          results.push(file);
          if (!--pending) {
            done(null, results);
          }
        }
      });
    });
  });
}

async function transcribeDocModules() {
  return new Promise((resolve, reject) => {
    let outputString = `const path = require('path');
    const fs = require('fs');
    const fse = require('fs-extra');

    // This is a super dirty hack because nodejs-mobile can't detect the babel plugins
    // and we can't run npm install during runtime because of iOS constraints.

    const docModules = [\n`;

    const regeneratorDependencies = JSON.parse(
      fs.readFileSync(regeneratorJsonPath, {
        encoding: 'utf8',
      }),
    ).dependencies;

    recursivelyGetFiles(basePath, (error, results) => {
      if (results) {
        results.map((filePath, fileIndex) => {
          if (filePath.endsWith('.json') || filePath.endsWith('.js')) {
            const data = fs
              .readFileSync(filePath, {
                encoding: 'utf8',
              })
              .replace(/\\/g, '\\\\')
              .replace(/`/g, '\\`')
              .replace(/\$\{/g, '\\${');
            let key = filePath.replace(basePath, '');
            outputString =
              outputString + `{ '${key}': ` + `\`${data}\` }` + ',\n';

            // HACK - Regenerator needs dependencies to be in its directory to work in nodejs-mobile
            if (
              regeneratorDependencies[
                key.replace('/node_modules/', '').split('/')[0]
              ]
            ) {
              const subKey = '/node_modules/regenerator' + key;
              outputString =
                outputString + `{ '${subKey}': ` + `\`${data}\` }` + ',\n';
            }
          }
        });
        outputString =
          outputString +
          '];\n\n' +
          `const writeDocumentsModules = async () => {
          return await Promise.all(
            docModules.map(async (docModule, moduleIndex) => {
              const entry = Object.entries(docModule);
              let filePath = entry[0];
              let data = entry[1];

              if (Array.isArray(filePath)) {
                let fileEntry = filePath;
                filePath = fileEntry[0];
                data = fileEntry[1];
              }

              // Ensure directory exists
              const filePathArray = filePath.split('/');
              if (filePathArray.filter(subPath => subPath !== '').length > 1) {
                const lastSlashIndex = filePath.lastIndexOf('/');
                const parentPath = filePath.substring(0, lastSlashIndex);
                const fullParentPath = path.resolve(global.dataDir + parentPath);
                fs.mkdirSync(fullParentPath, {recursive: true});
              }

              // Write file
              const fullPath = path.resolve(global.dataDir + filePath);
              return fs.writeFileSync(fullPath, data);
            }),
          );
        };
        module.exports = writeDocumentsModules;`;
        fs.writeFileSync(
          path.resolve(
            './nodejs-assets/nodejs-project/lib/modules-to-documents.js',
          ),
          outputString,
        );
        resolve({
          stdout: 'Transcribed ' + results.length + ' files for Documents!',
        });
      } else if (error) {
        reject({
          stdout: 'Error transcribing document modules: ' + error,
        });
      }
    });
  });
}

module.exports = transcribeDocModules;
